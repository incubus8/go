package jsonsecrets

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
)

var (
	errWrongType = errors.New("wrong type")
	errNotFound  = errors.New("not found")
)

// New will create a config wrapper based on a JSON file
func New(file string) (*Wrapper, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("failed to open JSON secrets file with err: %s", err)
	}

	blob, err := io.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("failed to read JSON secrets file with err: %s", err)
	}

	return NewFromBytes(blob)
}

// NewFromBytes will create a config wrapper based on the supplied JSON string
func NewFromBytes(payload []byte) (*Wrapper, error) {
	var config map[string]interface{}

	err := json.Unmarshal(payload, &config)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal JSON secrets with err: %s", err)
	}

	return &Wrapper{
		config: config,
	}, nil
}

// Wrapper is the JSON implementation of cfgsdk.SecretWrapper
type Wrapper struct {
	config map[string]interface{}
}

// GetSecret implements cfgsdk.StorageWrapper
func (w *Wrapper) GetSecret(key string) (string, error) {
	val, ok := w.config[key]
	if !ok {
		return "", fmt.Errorf("key: %s - %w", key, errNotFound)
	}

	valAsString, ok := val.(string)
	if !ok {
		return "", fmt.Errorf("key: %s, val: %v, type: %T - %w", key, val, val, errWrongType)
	}

	return valAsString, nil
}
