package jsonsecrets

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWrapperSecret(t *testing.T) {
	cfgWrapper, err := New("./testdata/test.json")
	assert.Nil(t, err)

	tests := []struct {
		desc        string
		key         string
		expectedVal string
		expectedErr error
	}{
		{
			desc:        "string feature happy path",
			key:         "string_feature",
			expectedErr: nil,
			expectedVal: "string value",
		},
		{
			desc:        "string feature not found",
			key:         "string_feature_404",
			expectedErr: errNotFound,
			expectedVal: "",
		},
	}

	for _, test := range tests {
		val, err := cfgWrapper.GetSecret(test.key)

		assert.Equal(t, test.expectedVal, val, test.desc)
		assert.True(t, errors.Is(err, test.expectedErr), test.desc)
	}
}
