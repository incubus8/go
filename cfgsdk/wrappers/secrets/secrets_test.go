package secrets

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetSecrets(t *testing.T) {
	os.Setenv("SECRET_STRING", "this_is_a_secret")

	var wrapper EnvSecret

	tests := []struct {
		desc        string
		key         string
		expectedVal string
		expectErr   bool
	}{
		{
			desc:        "secret in env happy path",
			key:         "SECRET_STRING",
			expectedVal: "this_is_a_secret",
			expectErr:   false,
		},
		{
			desc:        "secret not in env",
			key:         "SECRET_STRING_UNSET",
			expectedVal: "",
			expectErr:   true,
		},
	}

	for _, test := range tests {
		feature, err := wrapper.GetSecret(test.key)

		assert.Equal(t, test.expectedVal, feature, test.desc)
		assert.Equal(t, test.expectErr, err != nil, feature, test.desc)
	}
}
