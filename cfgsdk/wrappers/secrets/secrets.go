package secrets

import (
	"errors"
	"fmt"
	"os"
)

var errVarUnset = errors.New("variable not set")

// EnvSecret is the implementation of cfgsdk.SecretWrapper
type EnvSecret struct{}

// GetSecret implements cfgsdk.SecretWrapper
func (s *EnvSecret) GetSecret(key string) (string, error) {
	val, ok := os.LookupEnv(key)
	if !ok {
		return "", fmt.Errorf("secret not found. key: %s err: %w", key, errVarUnset)
	}

	return val, nil
}
