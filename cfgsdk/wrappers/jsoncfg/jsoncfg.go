package jsoncfg

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"

	"gitlab.com/incubus8/go/cfgsdk"
	"gitlab.com/incubus8/go/cfgsdk/attribute"
)

var (
	errWrongType = errors.New("wrong type")
	errNotFound  = errors.New("not found")
)

// WrapperOption is used to pass additional options to the wrapper
type WrapperOption func(*Wrapper)

// New will create a config wrapper based on a JSON file
func New(file string, opts ...WrapperOption) (*Wrapper, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("failed to open JSON config file with err: %s", err)
	}

	blob, err := io.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("failed to read JSON config file with err: %s", err)
	}

	cfg := &Wrapper{
		rawConfig: blob,
	}

	for _, opt := range opts {
		opt(cfg)
	}

	cfg, err = NewFromBytes(cfg.rawConfig)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}

// NewFromBytes will create a config wrapper based on the supplied JSON	string
func NewFromBytes(payload []byte) (*Wrapper, error) {
	var config map[string]interface{}

	err := json.Unmarshal(payload, &config)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal JSON config with err: %s", err)
	}

	return &Wrapper{
		jsonConfig: config,
	}, nil
}

// WithSub substitutes a template string such as %%ADDRESS%% with a string
func WithSub(tpl, val string) WrapperOption {
	return func(w *Wrapper) {
		w.rawConfig = bytes.ReplaceAll(w.rawConfig, []byte(tpl), []byte(val))
	}
}

// Wrapper is the JSON implementation of cfgsdk.StorageWrapper
type Wrapper struct {
	rawConfig  []byte
	jsonConfig map[string]interface{}
}

// GetInt implements cfgsdk.StorageWrapper
func (w *Wrapper) GetInt(key string, _ []attribute.Attribute) (int, error) {
	val, ok := w.jsonConfig[key]
	if !ok {
		return 0, fmt.Errorf("key: %s - %w", key, errNotFound)
	}

	valAsFloat, ok := val.(float64)
	if !ok {
		return 0, fmt.Errorf("key: %s, val: %v, type: %T - %w", key, val, val, errWrongType)
	}

	return int(valAsFloat), nil
}

// GetString implements cfgsdk.StorageWrapper
func (w *Wrapper) GetString(key string, _ []attribute.Attribute) (string, error) {
	val, ok := w.jsonConfig[key]
	if !ok {
		return "", fmt.Errorf("key: %s - %w", key, errNotFound)
	}

	valAsString, ok := val.(string)
	if !ok {
		return "", fmt.Errorf("key: %s, val: %v, type: %T - %w", key, val, val, errWrongType)
	}

	return valAsString, nil
}

// GetFloat implements cfgsdk.StorageWrapper
func (w *Wrapper) GetFloat(key string, _ []attribute.Attribute) (float64, error) {
	val, ok := w.jsonConfig[key]
	if !ok {
		return 0, fmt.Errorf("key: %s - %w", key, errNotFound)
	}

	valAsFloat, ok := val.(float64)
	if !ok {
		return 0, fmt.Errorf("key: %s, val: %v, type: %T - %w", key, val, val, errWrongType)
	}

	return valAsFloat, nil
}

// GetBool implements cfgsdk.StorageWrapper
func (w *Wrapper) GetBool(key string, _ []attribute.Attribute) (bool, error) {
	val, ok := w.jsonConfig[key]
	if !ok {
		return false, fmt.Errorf("key: %s - %w", key, errNotFound)
	}

	valAsBool, ok := val.(bool)
	if !ok {
		return false, fmt.Errorf("key: %s, val: %v, type: %T - %w", key, val, val, errWrongType)
	}

	return valAsBool, nil
}

// GetJSON implements cfgsdk.StorageWrapper
func (w *Wrapper) GetJSON(key string, destination interface{}, _ []attribute.Attribute) error {
	val, ok := w.jsonConfig[key]
	if !ok {
		return fmt.Errorf("key: %s - %w", key, errNotFound)
	}

	// val is map type
	payload, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("key: %s, val: %v, type: %T - %w - err: %s", key, val, val, errWrongType, err)
	}

	err = json.Unmarshal(payload, destination)
	if err != nil {
		return fmt.Errorf("key: %s, val: %v, type: %T - %w - err: %s", key, val, val, errWrongType, err)
	}

	return nil
}

// GetIntWatcher implements cfgsdk.StorageWrapper
func (w *Wrapper) GetIntWatcher(_ string, _ []attribute.Attribute) cfgsdk.Watcher {
	return &cfgsdk.NOOPWatcher{}
}

// GetStringWatcher implements cfgsdk.StorageWrapper
func (w *Wrapper) GetStringWatcher(_ string, _ []attribute.Attribute) cfgsdk.Watcher {
	return &cfgsdk.NOOPWatcher{}
}

// GetFloatWatcher implements cfgsdk.StorageWrapper
func (w *Wrapper) GetFloatWatcher(_ string, _ []attribute.Attribute) cfgsdk.Watcher {
	return &cfgsdk.NOOPWatcher{}
}

// GetBoolWatcher implements cfgsdk.StorageWrapper
func (w *Wrapper) GetBoolWatcher(_ string, _ []attribute.Attribute) cfgsdk.Watcher {
	return &cfgsdk.NOOPWatcher{}
}

// GetJSONWatcher implements cfgsdk.StorageWrapper
func (w *Wrapper) GetJSONWatcher(_ string, _ []attribute.Attribute) cfgsdk.Watcher {
	return &cfgsdk.NOOPWatcher{}
}
