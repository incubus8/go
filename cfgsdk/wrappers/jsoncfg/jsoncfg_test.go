package jsoncfg

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWrapperInt(t *testing.T) {
	cfgWrapper, err := New("./testdata/test.json")
	assert.Nil(t, err)

	tests := []struct {
		desc        string
		key         string
		expectedVal int
		expectedErr error
	}{
		{
			desc:        "int feature happy path",
			key:         "int_feature",
			expectedErr: nil,
			expectedVal: 5,
		},
		{
			desc:        "int feature not found",
			key:         "int_feature_404",
			expectedErr: errNotFound,
			expectedVal: 0,
		},
	}

	for _, test := range tests {
		val, err := cfgWrapper.GetInt(test.key, nil)

		assert.Equal(t, test.expectedVal, val, test.desc)
		assert.True(t, errors.Is(err, test.expectedErr), test.desc)
	}
}

func TestWrapperFloat(t *testing.T) {
	cfgWrapper, err := New("./testdata/test.json")
	assert.Nil(t, err)

	tests := []struct {
		desc        string
		key         string
		expectedVal float64
		expectedErr error
	}{
		{
			desc:        "float feature happy path",
			key:         "float_feature",
			expectedErr: nil,
			expectedVal: 2.3,
		},
		{
			desc:        "float feature not found",
			key:         "float_feature_404",
			expectedErr: errNotFound,
			expectedVal: 0,
		},
	}

	for _, test := range tests {
		val, err := cfgWrapper.GetFloat(test.key, nil)

		assert.Equal(t, test.expectedVal, val, test.desc)
		assert.True(t, errors.Is(err, test.expectedErr), test.desc)
	}
}

func TestWrapperBool(t *testing.T) {
	cfgWrapper, err := New("./testdata/test.json")
	assert.Nil(t, err)

	tests := []struct {
		desc        string
		key         string
		expectedVal bool
		expectedErr error
	}{
		{
			desc:        "bool feature happy path",
			key:         "bool_feature",
			expectedErr: nil,
			expectedVal: true,
		},
		{
			desc:        "bool feature not found",
			key:         "bool_feature_404",
			expectedErr: errNotFound,
			expectedVal: false,
		},
	}

	for _, test := range tests {
		val, err := cfgWrapper.GetBool(test.key, nil)

		assert.Equal(t, test.expectedVal, val, test.desc)
		assert.True(t, errors.Is(err, test.expectedErr), test.desc)
	}
}

func TestWrapperString(t *testing.T) {
	cfgWrapper, err := New("./testdata/test.json")
	assert.Nil(t, err)

	tests := []struct {
		desc        string
		key         string
		expectedVal string
		expectedErr error
	}{
		{
			desc:        "string feature happy path",
			key:         "string_feature",
			expectedErr: nil,
			expectedVal: "string value",
		},
		{
			desc:        "string feature not found",
			key:         "string_feature_404",
			expectedErr: errNotFound,
			expectedVal: "",
		},
	}

	for _, test := range tests {
		val, err := cfgWrapper.GetString(test.key, nil)

		assert.Equal(t, test.expectedVal, val, test.desc)
		assert.True(t, errors.Is(err, test.expectedErr), test.desc)
	}
}

func TestWrapperJSON(t *testing.T) {
	cfgWrapper, err := New("./testdata/test.json")
	assert.Nil(t, err)

	tests := []struct {
		desc        string
		key         string
		expectedVal *jsonObject
		expectedErr error
	}{
		{
			desc:        "json feature happy path",
			key:         "json_feature",
			expectedErr: nil,
			expectedVal: &jsonObject{Name: "fu", Age: 666},
		},
		{
			desc:        "json feature not found",
			key:         "json_feature_404",
			expectedErr: errNotFound,
			expectedVal: &jsonObject{Name: "", Age: 0},
		},
	}

	for _, test := range tests {
		var val jsonObject
		err := cfgWrapper.GetJSON(test.key, &val, nil)

		assert.Equal(t, test.expectedVal, &val, test.desc)
		assert.True(t, errors.Is(err, test.expectedErr), test.desc)
	}
}

func TestWrapperJSONWithSubs(t *testing.T) {
	cfgWrapper, err := New("./testdata/test.json",
		WithSub("%%TEST%%", "substitute"),
		WithSub("<ANOTHER_TEST>", "another_substitute"))
	assert.Nil(t, err)

	tests := []struct {
		desc        string
		key         string
		expectedVal *jsonObject
		expectedErr error
	}{
		{
			desc:        "sub feature happy path with %",
			key:         "substitute_feature",
			expectedErr: nil,
			expectedVal: &jsonObject{Name: "substitute", Age: 999},
		},
		{
			desc:        "sub feature happy path with <>",
			key:         "another_substitute_feature",
			expectedErr: nil,
			expectedVal: &jsonObject{Name: "another_substitute", Age: 50},
		},
	}

	for _, test := range tests {
		var val jsonObject
		err := cfgWrapper.GetJSON(test.key, &val, nil)

		assert.Equal(t, test.expectedVal, &val, test.desc)
		assert.True(t, errors.Is(err, test.expectedErr), test.desc)
	}
}

type jsonObject struct {
	Name string
	Age  int
}
