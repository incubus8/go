package cfgsdk

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"gitlab.com/incubus8/go/cfgsdk/attribute"
)

// New creates and initialized the client based on the supplied config
func New(cfg Config, storage StorageWrapper, secrets SecretWrapper) *Client {
	cfg.Logger().ClientEvents("client created with %T and %T wrappers", storage, secrets)

	return &Client{
		cfg:            cfg,
		storage:        storage,
		secrets:        secrets,
		keysMutex:      &sync.RWMutex{},
		keyToListeners: map[string][]chan<- struct{}{},
		watchers:       map[string]Watcher{},
		shutdownSignal: make(chan struct{}),
	}
}

// Client is the implementation of the config client.
// NOTE: if you want to mock this client, please define an interface in your code and use that to generate mocks
type Client struct {
	cfg Config

	storage StorageWrapper
	secrets SecretWrapper

	keysMutex      *sync.RWMutex
	keyToListeners map[string][]chan<- struct{}
	watchers       map[string]Watcher

	// this channel will be closed when the client is shutdown
	shutdownSignal chan struct{}

	attributes []attribute.Attribute

	ticker *time.Ticker
}

// Start starts a goroutine to monitor for config changes.
// This method must be called for Watch() to work
func (c *Client) Start() {
	c.cfg.Logger().ClientEvents("config watcher started")

	c.ticker = time.NewTicker(c.cfg.ConfigScanInterval())

	go func() {
		for {
			select {
			case <-c.ticker.C:
				c.scanConfig()

			case <-c.shutdownSignal:
				c.ticker.Stop()

				c.cfg.Logger().ClientEvents("config watcher stopped")

				return
			}
		}
	}()
}

// Shutdown returns a channel that will be closed when the client has complete graceful shutdown.
// This method should be called when the application is shutting down.
func (c *Client) Shutdown() chan struct{} {
	stopCh := make(chan struct{})

	// async shutdown and clean up
	go func() {
		defer close(stopCh)

		c.cfg.Logger().ClientEvents("config shutdown")

		close(c.shutdownSignal)
	}()

	return stopCh
}

// Watch creates an observer to the supplied config key.  Any changes to the config key will result in an event being to the supplied
// channel.
// Users are expected to use the "pull to pull" pattern whereby notifications on the channel trigger the user to reload the latest config.
//
// Typical use-cases for this include: dynamic configuration of a goroutine worker pool or dependency URL.
// Cases where explicit clean up and reinitialization is needed.
//
// NOTE: when events are not read from the channel and the channel becomes full then subsequent events will be dropped
// NOTE: users must supply their own notification channel so that they can control the buffer side and share notification channels
// between watchers (if it makes sense to do so)
func (c *Client) Watch(key string, notices chan<- struct{}) error {
	if key == "" {
		return errors.New("key cannot be empty")
	}

	if notices == nil {
		return errors.New("notices cannot be nil")
	}

	c.keysMutex.Lock()
	defer c.keysMutex.Unlock()

	_, exists := c.keyToListeners[key]
	if !exists {
		watcher, err := c.getWatcher(key)
		if err != nil {
			return err
		}

		c.keyToListeners[key] = []chan<- struct{}{}

		c.watchers[key] = watcher
	}

	c.keyToListeners[key] = append(c.keyToListeners[key], notices)

	return nil
}

// GetWatcher returns a watcher for the supplied key
func (c *Client) getWatcher(key string) (Watcher, error) {
	watcher := c.storage.GetStringWatcher(key, c.attributes)
	if watcher != nil {
		return watcher, nil
	}

	watcher = c.storage.GetJSONWatcher(key, c.attributes)
	if watcher != nil {
		return watcher, nil
	}

	watcher = c.storage.GetIntWatcher(key, c.attributes)
	if watcher != nil {
		return watcher, nil
	}

	watcher = c.storage.GetBoolWatcher(key, c.attributes)
	if watcher != nil {
		return watcher, nil
	}

	watcher = c.storage.GetFloatWatcher(key, c.attributes)
	if watcher != nil {
		return watcher, nil
	}

	return nil, fmt.Errorf("unable to determine watcher for supplied key: %s", key)
}

// Unwatch is the opposite of `Watch()`.
// Note: Multiple and erroneous calls to Unwatch will be silently ignored
func (c *Client) Unwatch(key string, notices chan struct{}) {
	if key == "" {
		return
	}

	if notices == nil {
		return
	}

	c.keysMutex.Lock()
	defer c.keysMutex.Unlock()

	listeners, exists := c.keyToListeners[key]
	if !exists {
		return
	}

	afterRemoval := make([]chan<- struct{}, 0, len(listeners))

	for _, listener := range listeners {
		if listener != notices {
			afterRemoval = append(afterRemoval, listener)
		}
	}

	if len(afterRemoval) == len(listeners) {
		return
	}

	c.keyToListeners[key] = afterRemoval

	if len(listeners) == 0 {
		delete(c.keyToListeners, key)
		delete(c.watchers, key)
	}
}

// GetInt returns a integer configuration value.
//
// Typical use-cases for this include: circuit breaker settings, retry settings and tweaking configuration that can be different for every
// request, like enabling or disabling new features.
//
// Note: If callers require additional validation, like ensuring the value is positive then they should perform that on the returned value.
// Note: The default value is returned when the value loaded is of a type that cannot be converted to the requested type
// Note: The default value is returned when the value does not exist in config storage
// Note: The default value is NOT returned when value in config storage is exists but is a zero value (e.g 0 or "")
func (c *Client) GetInt(key string, defaultValue int, attributes ...attribute.Attribute) int {
	c.cfg.Stats().GetUsage(key, statsKeyGetInt)

	storageAttributes := c.getMergedAttributes(attributes)

	result, err := c.storage.GetInt(key, storageAttributes)
	if err != nil {
		c.reportDefaultValue(key, statsKeyGetInt, err)

		return defaultValue
	}

	return result
}

// GetString returns a string configuration value.
//
// For more info, see `GetInt()`
func (c *Client) GetString(key, defaultValue string, attributes ...attribute.Attribute) string {
	c.cfg.Stats().GetUsage(key, statsKeyGetString)

	storageAttributes := c.getMergedAttributes(attributes)

	result, err := c.storage.GetString(key, storageAttributes)
	if err != nil {
		c.reportDefaultValue(key, statsKeyGetString, err)

		return defaultValue
	}

	return result
}

// GetFloat returns a float64 configuration value.
//
// For more info, see `GetInt()`
func (c *Client) GetFloat(key string, defaultValue float64, attributes ...attribute.Attribute) float64 {
	c.cfg.Stats().GetUsage(key, statsKeyGetFloat)

	storageAttributes := c.getMergedAttributes(attributes)

	result, err := c.storage.GetFloat(key, storageAttributes)
	if err != nil {
		c.reportDefaultValue(key, statsKeyGetFloat, err)

		return defaultValue
	}

	return result
}

// GetBool returns a boolean configuration value.
//
// For more info, see `GetInt()`
func (c *Client) GetBool(key string, defaultValue bool, attributes ...attribute.Attribute) bool {
	c.cfg.Stats().GetUsage(key, statsKeyGetBool)

	storageAttributes := c.getMergedAttributes(attributes)

	result, err := c.storage.GetBool(key, storageAttributes)
	if err != nil {
		c.reportDefaultValue(key, statsKeyGetBool, err)

		return defaultValue
	}

	return result
}

// GetJSON attempts to populate `destination` with the value from config storage
//
// For more info, see `GetInt()`
//
// NOTE: `destination` also serves as the default value.  Issues like failure to parse will leave the destination unchanged
func (c *Client) GetJSON(key string, destination interface{}, attributes ...attribute.Attribute) {
	c.cfg.Stats().GetUsage(key, statsKeyGetJSON)

	storageAttributes := c.getMergedAttributes(attributes)

	err := c.storage.GetJSON(key, destination, storageAttributes)
	if err != nil {
		c.reportDefaultValue(key, statsKeyGetJSON, err)
	}
}

// GetSecret returns a secret configuration value.
//
// For more info, see `GetInt()`
func (c *Client) GetSecret(key, defaultValue string) string {
	c.cfg.Stats().GetUsage(key, statsKeyGetSecret)

	result, err := c.secrets.GetSecret(key)
	if err != nil {
		c.reportDefaultValue(key, statsKeyGetSecret, err)

		return defaultValue
	}

	return result
}

func (c *Client) scanConfig() {
	c.keysMutex.RLock()
	defer c.keysMutex.RUnlock()

	var key string
	var watcher Watcher

	for key = range c.watchers {
		watcher = c.watchers[key]

		if !watcher.HasChanged() {
			continue
		}

		c.cfg.Logger().VariableChanged(key)

		for _, listener := range c.keyToListeners[key] {
			select {
			case listener <- struct{}{}:
			// happy path

			default: // drop
			}
		}
	}
}

func (c *Client) getMergedAttributes(funcAttributes []attribute.Attribute) []attribute.Attribute {
	return append(c.attributes, funcAttributes...)
}

func (c *Client) reportDefaultValue(key, tag string, err error) {
	c.cfg.Stats().DefaultValue(key, tag)
	c.cfg.Logger().DefaultValue(key, "[%s] default value returned. cause: %s", tag, err)
}

// Config is the configuration for the Client
type Config interface {
	// Logger returns the logger instance for the config client
	Logger() Logger

	// Stats returns the instrumentation client for the config client
	Stats() Stats

	// ConfigScanInterval is the duration between beginning config changed checks
	ConfigScanInterval() time.Duration
}

// Logger is logging for the Client
//
//go:generate mockery --name=Logger --case=underscore --testonly --inpackage
type Logger interface {
	// VariableChanged records changes in variable state.  This should be used for DEBUG only
	VariableChanged(key string)

	// ClientEvents records client events like start up, shutdown and watch subscriptions and unsubscriptions
	ClientEvents(message string, args ...interface{})

	// DefaultValue records when the default value has been returned and why
	DefaultValue(key, message string, args ...interface{})
}
