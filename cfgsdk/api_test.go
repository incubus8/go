package cfgsdk

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestClient_startStop(t *testing.T) {
	client := New(&testConfig{}, nil, nil)

	stopCh := client.Shutdown()

	select {
	case <-stopCh:
		// happy path
		assert.True(t, true)

	case <-time.After(1 * time.Second):
		assert.Fail(t, "timeout waiting for shutdown")
	}
}

func TestClient_WatchUnWatch_happyPath(t *testing.T) {
	// inputs
	testKey := "test-key"
	updatedValue := "updated"
	notices := make(chan struct{}, 1)

	// mocks
	mockWatcher := &MockWatcher{}
	mockWatcher.On("HasChanged").Return(true)

	storage := &MockStorageWrapper{}
	storage.On("GetStringWatcher", testKey, mock.Anything).Return(mockWatcher, nil).Once()
	storage.On("GetString", testKey, mock.Anything).Return(updatedValue, nil).Once()

	// use client
	client := New(&testConfig{}, storage, nil)

	client.Start()
	defer client.Shutdown()

	require.NoError(t, client.Watch(testKey, notices))
	defer client.Unwatch(testKey, notices)

	// wait for update notification
	select {
	case <-notices:
		assert.Equal(t, updatedValue, client.GetString(testKey, ""))

	case <-time.After(1 * time.Second):
		assert.Fail(t, "timeout")
	}
}

func TestClient_GetInt(t *testing.T) {
	testKey := "test-key"
	happyValue := 1234
	defaultValue := 6666

	scenarios := []struct {
		desc          string
		inConfigMock  func(*MockStorageWrapper)
		expectedValue int
	}{
		{
			desc: "happy path",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetInt", testKey, mock.Anything).Return(happyValue, nil).Once()
			},
			expectedValue: happyValue,
		},
		{
			desc: "error path - unknown key",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetInt", testKey, mock.Anything).Return(0, errors.New("unknown key")).Once()
			},
			expectedValue: defaultValue,
		},
		{
			desc: "error path - bad value",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetInt", testKey, mock.Anything).Return(0, errors.New("bad value")).Once()
			},
			expectedValue: defaultValue,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			// config mocks
			storage := &MockStorageWrapper{}
			scenario.inConfigMock(storage)

			client := New(&testConfig{}, storage, nil)

			result := client.GetInt(testKey, defaultValue)
			assert.Equal(t, scenario.expectedValue, result, scenario.desc)
		})
	}
}

func TestClient_GetString(t *testing.T) {
	testKey := "test-key"
	happyValue := "fu"
	defaultValue := "bar"

	scenarios := []struct {
		desc          string
		inConfigMock  func(*MockStorageWrapper)
		expectedValue string
	}{
		{
			desc: "happy path",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetString", testKey, mock.Anything).Return(happyValue, nil).Once()
			},
			expectedValue: happyValue,
		},
		{
			desc: "error path - unknown key",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetString", testKey, mock.Anything).Return("", errors.New("unknown key")).Once()
			},
			expectedValue: defaultValue,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			// config mocks
			storage := &MockStorageWrapper{}
			scenario.inConfigMock(storage)

			client := New(&testConfig{}, storage, nil)

			result := client.GetString(testKey, defaultValue)
			assert.Equal(t, scenario.expectedValue, result, scenario.desc)
		})
	}
}

func TestClient_GetFloat(t *testing.T) {
	testKey := "test-key"
	happyValue := 12.34
	defaultValue := 66.66

	scenarios := []struct {
		desc          string
		inConfigMock  func(*MockStorageWrapper)
		expectedValue float64
	}{
		{
			desc: "happy path",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetFloat", testKey, mock.Anything).Return(happyValue, nil).Once()
			},
			expectedValue: happyValue,
		},
		{
			desc: "error path - unknown key",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetFloat", testKey, mock.Anything).Return(0.0, errors.New("unknown key")).Once()
			},
			expectedValue: defaultValue,
		},
		{
			desc: "error path - bad value",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetFloat", testKey, mock.Anything).Return(0.0, errors.New("bad value")).Once()
			},
			expectedValue: defaultValue,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			// config mocks
			storage := &MockStorageWrapper{}
			scenario.inConfigMock(storage)

			client := New(&testConfig{}, storage, nil)

			result := client.GetFloat(testKey, defaultValue)
			assert.Equal(t, scenario.expectedValue, result, scenario.desc)
		})
	}
}

func TestClient_GetBool(t *testing.T) {
	testKey := "test-key"
	happyValue := true
	defaultValue := false

	scenarios := []struct {
		desc          string
		inConfigMock  func(*MockStorageWrapper)
		expectedValue bool
	}{
		{
			desc: "happy path",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetBool", testKey, mock.Anything).Return(happyValue, nil).Once()
			},
			expectedValue: happyValue,
		},
		{
			desc: "error path - unknown key",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetBool", testKey, mock.Anything).Return(false, errors.New("unknown key")).Once()
			},
			expectedValue: defaultValue,
		},
		{
			desc: "error path - bad value",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetBool", testKey, mock.Anything).Return(false, errors.New("bad value")).Once()
			},
			expectedValue: defaultValue,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			// config mocks
			storage := &MockStorageWrapper{}
			scenario.inConfigMock(storage)

			client := New(&testConfig{}, storage, nil)

			result := client.GetBool(testKey, defaultValue)
			assert.Equal(t, scenario.expectedValue, result, scenario.desc)
		})
	}
}

func TestClient_GetJSON(t *testing.T) {
	testKey := "test-key"
	happyValue := fakeAppConfig{
		Host: "happy",
		Port: 666,
	}
	defaultValue := fakeAppConfig{
		Host: "default",
		Port: 888,
	}

	scenarios := []struct {
		desc          string
		inConfigMock  func(*MockStorageWrapper)
		expectedValue *fakeAppConfig
	}{
		{
			desc: "happy path",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetJSON", testKey, mock.Anything, mock.Anything).Return(nil).
					Run(func(args mock.Arguments) {
						destination := args.Get(1).(*fakeAppConfig)
						*destination = happyValue
					}).Once()
			},
			expectedValue: &happyValue,
		},
		{
			desc: "error path - unknown key",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetJSON", testKey, mock.Anything, mock.Anything).Return(errors.New("unknown key")).Once()
			},
			expectedValue: &defaultValue,
		},
		{
			desc: "error path - bad value",
			inConfigMock: func(wrapper *MockStorageWrapper) {
				wrapper.On("GetJSON", testKey, mock.Anything, mock.Anything).Return(errors.New("bad value")).Once()
			},
			expectedValue: &defaultValue,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			// config mocks
			storage := &MockStorageWrapper{}
			scenario.inConfigMock(storage)

			client := New(&testConfig{}, storage, nil)

			result := &defaultValue

			client.GetJSON(testKey, result)
			assert.Equal(t, scenario.expectedValue, result, scenario.desc)
		})
	}
}

func TestClient_GetSecret(t *testing.T) {
	testKey := "test-key"
	happyValue := "fu"
	defaultValue := "bar"

	scenarios := []struct {
		desc          string
		inConfigMock  func(secrets *MockSecretWrapper)
		expectedValue string
	}{
		{
			desc: "happy path",
			inConfigMock: func(secrets *MockSecretWrapper) {
				secrets.On("GetSecret", testKey).Return(happyValue, nil).Once()
			},
			expectedValue: happyValue,
		},
		{
			desc: "error path - unknown key",
			inConfigMock: func(secrets *MockSecretWrapper) {
				secrets.On("GetSecret", testKey).Return("", errors.New("unknown key")).Once()
			},
			expectedValue: defaultValue,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			// config mocks
			secrets := &MockSecretWrapper{}

			scenario.inConfigMock(secrets)

			client := New(&testConfig{}, nil, secrets)

			result := client.GetSecret(testKey, defaultValue)
			assert.Equal(t, scenario.expectedValue, result, scenario.desc)
		})
	}
}

func TestClient_instrumentation(t *testing.T) {
	testKey := "foo"

	scenarios := []struct {
		desc          string
		configureMock func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger)
		useClient     func(client *Client)
	}{
		{
			desc: "happy path - GetInt()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetInt", testKey, mock.Anything).Return(123, nil).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetInt)
			},
			useClient: func(client *Client) {
				_ = client.GetInt(testKey, 666)
			},
		},
		{
			desc: "sad path - GetInt()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetInt", testKey, mock.Anything).Return(0, errors.New("something failed")).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetInt)
				mockStats.On("DefaultValue", testKey, statsKeyGetInt)
				mockLogger.On("DefaultValue", testKey, statsKeyGetInt, mock.Anything)
			},
			useClient: func(client *Client) {
				_ = client.GetInt(testKey, 666)
			},
		},
		{
			desc: "happy path - GetString()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetString", testKey, mock.Anything).Return("aaa", nil).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetString)
			},
			useClient: func(client *Client) {
				_ = client.GetString(testKey, "bbb")
			},
		},
		{
			desc: "sad path - GetString()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetString", testKey, mock.Anything).Return("", errors.New("something failed")).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetString)
				mockStats.On("DefaultValue", testKey, statsKeyGetString)
				mockLogger.On("DefaultValue", testKey, statsKeyGetInt, mock.Anything)
			},
			useClient: func(client *Client) {
				_ = client.GetString(testKey, "bbb")
			},
		},
		{
			desc: "happy path - GetFloat()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetFloat", testKey, mock.Anything).Return(12.34, nil).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetFloat)
			},
			useClient: func(client *Client) {
				_ = client.GetFloat(testKey, 56.78)
			},
		},
		{
			desc: "sad path - GetFloat()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetFloat", testKey, mock.Anything).Return(0.0, errors.New("something failed")).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetFloat)
				mockStats.On("DefaultValue", testKey, statsKeyGetFloat)
				mockLogger.On("DefaultValue", testKey, statsKeyGetInt, mock.Anything)
			},
			useClient: func(client *Client) {
				_ = client.GetFloat(testKey, 56.78)
			},
		},
		{
			desc: "happy path - GetBool()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetBool", testKey, mock.Anything).Return(false, nil).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetBool)
			},
			useClient: func(client *Client) {
				_ = client.GetBool(testKey, true)
			},
		},
		{
			desc: "sad path - GetBool()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetBool", testKey, mock.Anything).Return(false, errors.New("something failed")).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetBool)
				mockStats.On("DefaultValue", testKey, statsKeyGetBool)
				mockLogger.On("DefaultValue", testKey, statsKeyGetInt, mock.Anything)
			},
			useClient: func(client *Client) {
				_ = client.GetBool(testKey, true)
			},
		},
		{
			desc: "happy path - GetJSON()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetJSON", testKey, mock.Anything, mock.Anything).Return(nil).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetJSON)
			},
			useClient: func(client *Client) {
				client.GetJSON(testKey, &fakeAppConfig{})
			},
		},
		{
			desc: "sad path - GetJSON()",
			configureMock: func(mockStorage *MockStorageWrapper, mockStats *MockStats, mockLogger *MockLogger) {
				mockStorage.On("GetJSON", testKey, mock.Anything, mock.Anything).Return(errors.New("something failed")).Once()

				mockStats.On("GetUsage", testKey, statsKeyGetJSON)
				mockStats.On("DefaultValue", testKey, statsKeyGetJSON)
				mockLogger.On("DefaultValue", testKey, statsKeyGetInt, mock.Anything)
			},
			useClient: func(client *Client) {
				client.GetJSON(testKey, &fakeAppConfig{})
			},
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			mockStats := &MockStats{}
			mockStorage := &MockStorageWrapper{}
			mockLogger := &MockLogger{}

			scenario.configureMock(mockStorage, mockStats, mockLogger)

			client := New(&testConfig{stats: mockStats}, mockStorage, nil)
			scenario.useClient(client)
		})
	}
}

type fakeAppConfig struct {
	Host string
	Port int
}
