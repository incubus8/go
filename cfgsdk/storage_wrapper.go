package cfgsdk

import (
	"gitlab.com/incubus8/go/cfgsdk/attribute"
)

// StorageWrapper defines the methods a storage wrapper must provide.
//
//go:generate mockery --name=StorageWrapper --case=underscore --testonly --inpackage
type StorageWrapper interface {
	// GetInt should return the requested config as an int or an error.
	GetInt(key string, attributes []attribute.Attribute) (int, error)

	// GetIntWatcher returns a watcher for a int key (returns nil if the supplied key does not exist or is not the correct type)
	GetIntWatcher(key string, attributes []attribute.Attribute) Watcher

	// GetString should return the requested config as an string or an error.
	GetString(key string, attributes []attribute.Attribute) (string, error)

	// GetStringWatcher returns a watcher for a string key (returns nil if the supplied key does not exist or is not the correct type)
	GetStringWatcher(key string, attributes []attribute.Attribute) Watcher

	// GetFloat should return the requested config as an float or an error.
	GetFloat(key string, attributes []attribute.Attribute) (float64, error)

	// GetFloatWatcher returns a watcher for a float key (returns nil if the supplied key does not exist or is not the correct type)
	GetFloatWatcher(key string, attributes []attribute.Attribute) Watcher

	// GetBool should return the requested config as an bool or an error.
	GetBool(key string, attributes []attribute.Attribute) (bool, error)

	// GetBoolWatcher returns a watcher for a bool key (returns nil if the supplied key does not exist or is not the correct type)
	GetBoolWatcher(key string, attributes []attribute.Attribute) Watcher

	// GetJSON should return the requested config as an object or an error.
	GetJSON(key string, destination interface{}, attributes []attribute.Attribute) error

	// GetJSONWatcher returns a watcher for a JSON key (returns nil if the supplied key does not exist or is not the correct type)
	GetJSONWatcher(key string, attributes []attribute.Attribute) Watcher
}
