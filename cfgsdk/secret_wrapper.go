package cfgsdk

// SecretWrapper defines the methods a secret storage must provide.
//
//go:generate mockery --name=SecretWrapper --case=underscore --testonly --inpackage
type SecretWrapper interface {
	// GetSecret should return the requested secret as a string or an error.
	GetSecret(key string) (string, error)
}
