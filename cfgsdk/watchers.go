package cfgsdk

import (
	"encoding/json"
	"reflect"
)

// Watcher provides a simple way to monitor changes in a key
//
//go:generate mockery --name=Watcher --case=underscore --testonly --inpackage
type Watcher interface {
	HasChanged() bool
}

// IntWatcher watches int values
type IntWatcher struct {
	Getter   func() (int, error)
	Previous int
}

// HasChanged implements wrappers.Watcher
func (i *IntWatcher) HasChanged() bool {
	current, err := i.Getter()
	if err != nil {
		return false
	}

	result := i.Previous != current

	i.Previous = current

	return result
}

// StringWatcher watches string values
type StringWatcher struct {
	Getter   func() (string, error)
	Previous string
}

// HasChanged implements wrappers.Watcher
func (s *StringWatcher) HasChanged() bool {
	current, err := s.Getter()
	if err != nil {
		return false
	}

	result := s.Previous != current

	s.Previous = current

	return result
}

// FloatWatcher watches float values
type FloatWatcher struct {
	Getter   func() (float64, error)
	Previous float64
}

// HasChanged implements wrappers.Watcher
func (f *FloatWatcher) HasChanged() bool {
	current, err := f.Getter()
	if err != nil {
		return false
	}

	result := f.Previous != current

	f.Previous = current

	return result
}

// BoolWatcher watches bool values
type BoolWatcher struct {
	Getter   func() (bool, error)
	Previous bool
}

// HasChanged implements wrappers.Watcher
func (b *BoolWatcher) HasChanged() bool {
	current, err := b.Getter()
	if err != nil {
		return false
	}

	result := b.Previous != current

	b.Previous = current

	return result
}

// JSONWatcher watches JSON config
type JSONWatcher struct {
	Getter            func() (json.RawMessage, error)
	Previous          json.RawMessage
	PreviousUnmarshal interface{}
}

// HasChanged implements wrappers.Watcher
func (s *JSONWatcher) HasChanged() bool {
	current, err := s.Getter()
	if err != nil {
		return false
	}

	var uCurrent interface{}

	err = json.Unmarshal(current, &uCurrent)
	if err != nil {
		return false
	}

	result := !reflect.DeepEqual(s.PreviousUnmarshal, uCurrent)

	s.Previous = current
	s.PreviousUnmarshal = uCurrent

	return result
}

// NOOPWatcher implements a Watcher for config that cannot change
type NOOPWatcher struct{}

// HasChanged implements wrappers.Watcher
func (*NOOPWatcher) HasChanged() bool {
	return false
}
