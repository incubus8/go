package attribute

const (
	instanceKey = "Instance"
	personalKey = "Personal"
)

// Attribute is a value/properties given to identify a certain config request/client
type Attribute interface {
	Key() string
	Value() Value

	// IsIdentityKey set an attribute as an identity key,
	// an identity allow for a attribute to have changes in other attributes, yet can still be identified as the same attribute.
	// This is useful for targeting behavior that involves percentage against the common population, ex :  Percentage rollout, A/B test
	IsIdentityKey() bool

	// IsMasked masked mask an attribute
	IsMasked() bool
}

func Custom(key string, val Value, masked bool) Attribute {
	return &attributeImpl{key: key, value: val, identity: false, masked: masked}
}

func CustomIdentity(key string, val Value, masked bool) Attribute {
	return &attributeImpl{key: key, value: val, identity: true, masked: masked}
}

func Instance(val string) Attribute {
	return &attributeImpl{
		key: instanceKey,
		value: ValueString(val),
		identity: false,
		masked: false,
	}
}

func Personal(val string) Attribute {
	return &attributeImpl{
		key: personalKey,
		value: ValueString(val),
		identity: true,
		masked: false,
	}
}


type attributeImpl struct {
	key   string
	value Value

	identity bool
	masked   bool
}

func (a *attributeImpl) IsIdentityKey() bool {
	return a.identity
}

func (a *attributeImpl) IsMasked() bool {
	return a.masked
}

func (a *attributeImpl) Key() string {
	return a.key
}

func (a *attributeImpl) Value() Value {
	return a.value
}
