package attribute

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValueBool(t *testing.T) {
	trueValue := ValueBool(true)

	assert.True(t, trueValue.BoolValue())
	assert.Equal(t, BoolType, trueValue.Type())

	trueValueStr, err := trueValue.ToString()

	assert.Equal(t, "true", trueValueStr)
	assert.Nil(t, err)

	falseValue := ValueBool(false)

	assert.False(t, falseValue.BoolValue())
	assert.Equal(t, BoolType, falseValue.Type())

	falseValueStr, err := falseValue.ToString()

	assert.Equal(t, "false", falseValueStr)
	assert.Nil(t, err)
}

func TestValueString(t *testing.T) {
	testValue := ValueString("test-value")

	assert.Equal(t, "test-value", testValue.StringValue())
	assert.Equal(t, StringType, testValue.Type())

	testValueStr, err := testValue.ToString()

	assert.Equal(t, "test-value", testValueStr)
	assert.Nil(t, err)
}

func TestValueInt(t *testing.T) {
	test := ValueInt(10)

	assert.Equal(t, 10, test.IntValue())
	assert.Equal(t, IntType, test.Type())

	testStr, err := test.ToString()

	assert.Equal(t, "10", testStr)
	assert.Nil(t, err)
}
