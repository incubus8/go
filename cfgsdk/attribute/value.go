package attribute

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"strconv"
)

const (
	BoolType   Type = 0
	IntType    Type = 1
	StringType Type = 2
)

type Type int

type Value interface {
	Type() Type
	StringValue() string
	IntValue() int
	BoolValue() bool

	ToString() (string, error)
	Hash() (string, error)
}

type valueImpl struct {
	attributeType Type

	stringValue string
	boolValue   bool
	intValue    int
}

func (a *valueImpl) StringValue() string {
	return a.stringValue
}

func (a *valueImpl) BoolValue() bool {
	return a.boolValue
}

func (a *valueImpl) IntValue() int {
	return a.intValue
}

func (a *valueImpl) Type() Type {
	return a.attributeType
}

func (a *valueImpl) Hash() (string, error) {
	valueStr, err := a.ToString()
	if err != nil {
		return "", err
	}

	valueWithType := strconv.Itoa(int(a.attributeType)) + valueStr

	sha := sha256.New()
	sha.Write([]byte(valueWithType))

	shaRes := sha.Sum(nil)

	return hex.EncodeToString(shaRes), nil
}

func (a *valueImpl) ToString() (string, error) {
	switch a.attributeType {
	case BoolType:
		return strconv.FormatBool(a.boolValue), nil

	case IntType:
		return strconv.Itoa(a.intValue), nil

	case StringType:
		return a.stringValue, nil

	default:
		return "", errors.New("invalid to string type")
	}
}

func ValueString(value string) Value {
	return &valueImpl{
		attributeType: StringType,
		stringValue:   value,
	}
}

func ValueInt(intVal int) Value {
	return &valueImpl{
		attributeType: IntType,
		intValue:      intVal,
	}
}

func ValueBool(boolVal bool) Value {
	return &valueImpl{
		attributeType: BoolType,
		boolValue:     boolVal,
	}
}
