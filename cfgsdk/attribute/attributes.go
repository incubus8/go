package attribute

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"sort"
)

type Attributes []Attribute

func (b Attributes) Len() int           { return len(b) }
func (b Attributes) Less(i, j int) bool { return b[i].Key() < b[j].Key() }
func (b Attributes) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }

// Identity returns either identifying attribute or a key generated from all attributes
func (b Attributes) Identity() (derived bool, result string) {
	for _, attribute := range b {
		if !attribute.IsIdentityKey() {
			continue
		}

		var identityKey string
		var err error

		switch {
		case attribute.IsMasked():
			identityKey, err = attribute.Value().Hash()

		default:
			identityKey, err = attribute.Value().ToString()
		}

		if err != nil {
			continue
		}

		return false, identityKey
	}

	return true, b.Hash()
}

func (b Attributes) Hash() string {
	sort.Sort(b)

	var buffer bytes.Buffer

	for _, att := range b {
		valueString, _ := att.Value().Hash()
		buffer.WriteString(att.Key() + ":" + valueString)
	}

	sha := sha256.New()
	sha.Write(buffer.Bytes())

	shaRes := sha.Sum(nil)

	return hex.EncodeToString(shaRes)
}
