package attribute

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAttribute(t *testing.T) {
	scenarios := []struct {
		desc             string
		inAttribute      Attribute
		expectKey        string
		expectVal        Value
		expectIsIdentity bool
		expectIsMasked   bool
	}{
		{
			desc:             "Instance",
			inAttribute:      Instance("test-instance"),
			expectKey:        instanceKey,
			expectVal:        ValueString("test-instance"),
			expectIsIdentity: false,
			expectIsMasked:   false,
		},
		{
			desc: "Custom - String Value - not masked",
			inAttribute: Custom("test-custom", ValueString("a"),
				false),
			expectKey:        "test-custom",
			expectVal:        ValueString("a"),
			expectIsIdentity: false,
			expectIsMasked:   false,
		},
		{
			desc: "Custom - String Value - masked",
			inAttribute: Custom("test-custom", ValueString("a"),
				true),
			expectKey:        "test-custom",
			expectVal:        ValueString("a"),
			expectIsIdentity: false,
			expectIsMasked:   true,
		},
		{
			desc: "CustomIdentity - String Value - not masked",
			inAttribute: CustomIdentity("test-custom", ValueString("a"),
				false),
			expectKey:        "test-custom",
			expectVal:        ValueString("a"),
			expectIsIdentity: true,
			expectIsMasked:   false,
		},
		{
			desc: "CustomIdentity - String Value - masked",
			inAttribute: CustomIdentity("test-custom", ValueString("a"),
				true),
			expectKey:        "test-custom",
			expectVal:        ValueString("a"),
			expectIsIdentity: true,
			expectIsMasked:   true,
		},
	}
	for _, s := range scenarios {
		scenario := s

		t.Run(scenario.desc, func(t *testing.T) {
			inAttribute := scenario.inAttribute

			assert.Equal(t, scenario.expectKey, inAttribute.Key())
			assert.Equal(t, scenario.expectVal, inAttribute.Value())
			assert.Equal(t, scenario.expectIsIdentity, inAttribute.IsIdentityKey())
			assert.Equal(t, scenario.expectIsMasked, inAttribute.IsMasked())
		})
	}
}
