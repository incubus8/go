package attribute

import (
	"testing"
)

func TestAttributes_Identity(t *testing.T) {
	scenarios := []struct {
		desc          string
		inAttributes  []Attribute
		expectDerived bool
		expectResult  string
	}{
		{
			desc: "With not masked identity attribute",
			inAttributes: []Attribute{
				Personal("personalid"),
				Custom("aaa", ValueString("value"), false),
			},
			expectDerived: false,
			expectResult:  "personalid",
		},
		{
			desc: "With masked identity attribute",
			inAttributes: []Attribute{
				CustomIdentity("masked id", ValueString("masked id"), true),
				Custom("aaa", ValueString("value"), false),
			},
			expectDerived: false,
			expectResult:  "f707f483be1e6f928847802f5bb9cb10888e2cc969018234cf72fc47adff0714",
		},
		{
			desc: "Without identity attibute - Order A",
			inAttributes: []Attribute{
				Custom("aaa", ValueString("value"), false),
				Custom("ccc", ValueBool(false), false),
				Custom("bbb", ValueString("value B"), false),
			},
			expectDerived: true,
			expectResult:  "be5b9cea36763b864d9d0489387269b5649d5157913e2a9177ecf6f618052201",
		},
		{
			desc: "Without identity attibute - Order B",
			inAttributes: []Attribute{
				Custom("bbb", ValueString("value B"), false),
				Custom("ccc", ValueBool(false), false),
				Custom("aaa", ValueString("value"), false),
			},
			expectDerived: true,
			expectResult:  "be5b9cea36763b864d9d0489387269b5649d5157913e2a9177ecf6f618052201",
		},
		{
			desc: "Without identity attibute - Different Attributes",
			inAttributes: []Attribute{
				Custom("bbb", ValueString("value B"), false),
				Custom("ccc", ValueString("false"), false),
				Custom("aaa", ValueString("value"), false),
			},
			expectDerived: true,
			expectResult:  "44c6c110feaa63034451f7404ef2e9358b9055d81d17386558a2848391e9fbbd",
		},
	}
	for _, s := range scenarios {
		scenario := s

		t.Run(scenario.desc, func(t *testing.T) {
			gotDerived, gotResult := Attributes(scenario.inAttributes).Identity()

			if gotDerived != scenario.expectDerived {
				t.Errorf("Identity() gotDerived = %v, want %v", gotDerived, scenario.expectDerived)
			}
			if gotResult != scenario.expectResult {
				t.Errorf("Identity() gotResult = %v, want %v", gotResult, scenario.expectResult)
			}
		})
	}
}
