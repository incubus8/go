package cfgsdk

const (
	// stats keys for tracking usage
	statsKeyGetInt    = "get.int"
	statsKeyGetString = "get.string"
	statsKeyGetFloat  = "get.float"
	statsKeyGetBool   = "get.bool"
	statsKeyGetJSON   = "get.json"
	statsKeyGetSecret = "get.secret"
)

// Stats is the instrumentation for the Client
//
//go:generate mockery --name=Stats --case=underscore --testonly --inpackage
type Stats interface {
	// GetUsage records usage of the `GetXxx()` functions.  Warning: this data should be sampled as it will be frequently called
	GetUsage(key, typeFunc string)

	// DefaultValue records usage of default values.  This should often be used for create alerts
	DefaultValue(key, typeFunc string)
}
