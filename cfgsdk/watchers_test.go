package cfgsdk

import (
	"encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestIntWatcher(t *testing.T) {
	initialValue := 1

	scenarios := []struct {
		desc     string
		inGetter func() (int, error)
		expected bool
	}{
		{
			desc: "happy path - unchanged",
			inGetter: func() (int, error) {
				return initialValue, nil
			},
			expected: false,
		},
		{
			desc: "happy path - changed",
			inGetter: func() (int, error) {
				return 666, nil
			},
			expected: true,
		},
		{
			desc: "sad path - error",
			inGetter: func() (int, error) {
				return 0, errors.New("something failed")
			},
			expected: false,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			watcher := &IntWatcher{
				Getter:   scenario.inGetter,
				Previous: initialValue,
			}

			result := watcher.HasChanged()
			assert.Equal(t, scenario.expected, result)
		})
	}
}

func TestStringWatcher(t *testing.T) {
	initialValue := "foo"

	scenarios := []struct {
		desc     string
		inGetter func() (string, error)
		expected bool
	}{
		{
			desc: "happy path - unchanged",
			inGetter: func() (string, error) {
				return initialValue, nil
			},
			expected: false,
		},
		{
			desc: "happy path - changed",
			inGetter: func() (string, error) {
				return "bar", nil
			},
			expected: true,
		},
		{
			desc: "sad path - error",
			inGetter: func() (string, error) {
				return "", errors.New("something failed")
			},
			expected: false,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			watcher := &StringWatcher{
				Getter:   scenario.inGetter,
				Previous: initialValue,
			}

			result := watcher.HasChanged()
			assert.Equal(t, scenario.expected, result)
		})
	}
}

func TestFloatWatcher(t *testing.T) {
	initialValue := 12.34

	scenarios := []struct {
		desc     string
		inGetter func() (float64, error)
		expected bool
	}{
		{
			desc: "happy path - unchanged",
			inGetter: func() (float64, error) {
				return initialValue, nil
			},
			expected: false,
		},
		{
			desc: "happy path - changed",
			inGetter: func() (float64, error) {
				return 66.66, nil
			},
			expected: true,
		},
		{
			desc: "sad path - error",
			inGetter: func() (float64, error) {
				return 0, errors.New("something failed")
			},
			expected: false,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			watcher := &FloatWatcher{
				Getter:   scenario.inGetter,
				Previous: initialValue,
			}

			result := watcher.HasChanged()
			assert.Equal(t, scenario.expected, result)
		})
	}
}

func TestBoolWatcher(t *testing.T) {
	initialValue := true

	scenarios := []struct {
		desc     string
		inGetter func() (bool, error)
		expected bool
	}{
		{
			desc: "happy path - unchanged",
			inGetter: func() (bool, error) {
				return initialValue, nil
			},
			expected: false,
		},
		{
			desc: "happy path - changed",
			inGetter: func() (bool, error) {
				return false, nil
			},
			expected: true,
		},
		{
			desc: "sad path - error",
			inGetter: func() (bool, error) {
				return false, errors.New("something failed")
			},
			expected: false,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.desc, func(t *testing.T) {
			watcher := &BoolWatcher{
				Getter:   scenario.inGetter,
				Previous: initialValue,
			}

			result := watcher.HasChanged()
			assert.Equal(t, scenario.expected, result)
		})
	}
}

func TestJSONWatcher(t *testing.T) {
	initialValue := json.RawMessage(`{"foo":"bar", "foo2":"bar2"}`)

	scenarios := []struct {
		desc     string
		inGetter func() (json.RawMessage, error)
		expected bool
	}{
		{
			desc: "happy path - unchanged",
			inGetter: func() (json.RawMessage, error) {
				return initialValue, nil
			},
			expected: false,
		},
		{
			desc: "happy path - unchanged - different order",
			inGetter: func() (json.RawMessage, error) {
				return json.RawMessage(`{"foo2":"bar2","foo":"bar"}`), nil
			},
			expected: false,
		},
		{
			desc: "happy path - changed",
			inGetter: func() (json.RawMessage, error) {
				return json.RawMessage(`{"foo":"apples"}`), nil
			},
			expected: true,
		},
		{
			desc: "sad path - error",
			inGetter: func() (json.RawMessage, error) {
				return nil, errors.New("something failed")
			},
			expected: false,
		},
		{
			desc: "sad path - bad new json",
			inGetter: func() (json.RawMessage, error) {
				return json.RawMessage(`-`), nil
			},
			expected: false,
		},
	}

	for _, s := range scenarios {
		scenario := s

		var previousUnmarshal interface{}

		err := json.Unmarshal(initialValue, &previousUnmarshal)
		require.Nil(t, err)

		t.Run(scenario.desc, func(t *testing.T) {
			watcher := &JSONWatcher{
				Getter:            scenario.inGetter,
				Previous:          initialValue,
				PreviousUnmarshal: previousUnmarshal,
			}

			result := watcher.HasChanged()
			assert.Equal(t, scenario.expected, result)
		})
	}
}
