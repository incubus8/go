package kafka

import (
	"time"

	kafkago "github.com/segmentio/kafka-go"
)

// Instrumentation defines the instrumentation (stats and logging) interface used by this package
//
//go:generate mockery -name=Instrumentation -case underscore -testonly -inpkg
type Instrumentation interface {
	// InitializedProducer logs producer is initialized successfully
	InitializedProducer()
	// ProducerStats logs producer stats
	ProducerStats(stats *kafkago.WriterStats)
	// ProducerDuration records duration on writing message to brokers
	ProducerDuration(start time.Time)

	// InitializedConsumer logs consumer is initialized successfully
	InitializedConsumer()
	// ConsumerStats logs consumer stats
	ConsumerStats(stats *kafkago.ReaderStats)
	// ClosedConsumer logs consumer is closed successfully
	ClosedConsumer()
	// StartConsumerReadLoop logs consumer's readLoop is started
	StartConsumerReadLoop()
	// StopConsumerReadLoop logs consumer's readLoop is stopped
	StopConsumerReadLoop()
	// ReadConsumerError logs error on reading messages from broker
	ReadConsumerError(err error)
	// ConsumerCommitFailed logs and adds stats for committing messages
	ConsumerCommitFailed(err error)

	// This method is called to log any errors relating to Kafka
	Printf(msg string, args ...interface{})
}
