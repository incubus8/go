package kafka

import (
	"errors"
	"io"
	"testing"
	"time"

	kafkago "github.com/segmentio/kafka-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestNewBytesConsumer(t *testing.T) {
	type args struct {
		config ConsumerConfig
	}

	scenarios := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "happy path",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: false,
		},
		{
			name: "invalid config - missing brokers",
			args: args{
				config: &consumerConfigTest{
					brokers: []string{},
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid config - missing groupID",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: "",
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid config - missing topic",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   "",
				},
			},
			wantErr: true,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.name, func(t *testing.T) {
			_, err := NewBytesConsumer(scenario.args.config)

			assert.Equal(t, scenario.wantErr, err != nil)
		})
	}
}

func TestBytesConsumer_Start(t *testing.T) {
	var (
		config = &consumerConfigTest{
			brokers: brokersForTest,
			groupID: groupIDForTest,
			topic:   topicIDForTest,
		}
		value = []byte("value-test")
	)

	scenarios := []struct {
		name                       string
		config                     ConsumerConfig
		configureKafkagoReaderMock func(reader *mockKafkagoReader)
		wantValue                  []byte
		wantErr                    bool
		wantConsumerClosed         bool
	}{
		{
			name:   "happy path",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{Value: value}, nil)
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(nil)
			},
			wantValue:          value,
			wantErr:            false,
			wantConsumerClosed: false,
		},
		{
			name:   "happy path - read message failure",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{}, errors.New("failed to read message"))
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(nil)
			},
			wantValue:          nil,
			wantErr:            true,
			wantConsumerClosed: false,
		},
		{
			name:   "happy path - eof",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{}, io.EOF)
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(nil)
			},
			wantValue:          nil,
			wantErr:            true,
			wantConsumerClosed: false,
		},
		{
			name:   "failed to commit messages to kafka - unknown error",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{Value: value}, nil)
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(errors.New("unknown error"))
			},
			wantValue:          value,
			wantErr:            false,
			wantConsumerClosed: false,
		},
		{
			name:   "failed to commit messages to kafka - closed pipe",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{Value: value}, nil)
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(io.ErrClosedPipe)
			},
			wantValue:          value,
			wantErr:            false,
			wantConsumerClosed: true,
		},
	}

	for _, s := range scenarios {
		scenario := s

		t.Run(scenario.name, func(t *testing.T) {
			kafkagoReaderMock := &mockKafkagoReader{}
			scenario.configureKafkagoReaderMock(kafkagoReaderMock)

			c := newBytesConsumer(scenario.config, kafkagoReaderMock)

			msgChan, err := c.Start()
			require.NoError(t, err)

			if scenario.wantErr {
				select {
				case <-msgChan:
					t.Errorf("Start(), there should be no messages in original channel")

				default:
					// happy path
				}
			} else {
				select {
				case msg := <-msgChan:
					assert.Equal(t, scenario.wantValue, msg, scenario.name)

				case <-time.After(1 * time.Second):
					assert.Fail(t, "timeout reading from channel")
				}
			}

			err = c.Close()
			assert.NoError(t, err, scenario.name)
		})
	}
}
