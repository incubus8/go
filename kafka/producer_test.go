package kafka

import (
	"context"
	"crypto/tls"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"
)

type producerConfigTest struct {
	brokers []string
	topic   string
	tls     *tls.Config
	sasl    *SaslConfig
}

func (p producerConfigTest) Instrumentation() Instrumentation {
	return &NoopInstrumentation{}
}

func (p producerConfigTest) Brokers() []string {
	return p.brokers
}

func (p producerConfigTest) Topic() string {
	return p.topic
}

func (p producerConfigTest) MaxRetry() int {
	return 3
}

func (p producerConfigTest) CompressionCodec() string {
	return ""
}

func (p producerConfigTest) IsAsync() bool {
	return false
}

func (p producerConfigTest) Acks() AckType {
	return AckLeader
}

func (p producerConfigTest) TLS() *tls.Config {
	return p.tls
}

func (p producerConfigTest) Sasl() *SaslConfig {
	return p.sasl
}

func TestValidateProducerConfig(t *testing.T) {
	type args struct {
		config ProducerConfig
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "happy path - no sasl",
			args: args{config: producerConfigTest{
				brokers: brokersForTest,
				topic:   topicIDForTest,
			}},
			wantErr: false,
		},
		{
			name: "happy path - with sasl, plaintext",
			args: args{config: producerConfigTest{
				brokers: brokersForTest,
				topic:   topicIDForTest,
				tls:     tlsConfigForTest,
				sasl:    saslConfigForTest,
			}},
			wantErr: false,
		},
		{
			name: "missing brokers",
			args: args{config: producerConfigTest{
				brokers: []string{},
				topic:   topicIDForTest,
			}},
			wantErr: true,
		},
		{
			name: "missing topic",
			args: args{config: producerConfigTest{
				brokers: brokersForTest,
				topic:   "",
			}},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if err := validateProducerConfig(tt.args.config); (err != nil) != tt.wantErr {
				t.Errorf("validateProducerConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewProducer(t *testing.T) {
	type args struct {
		config ProducerConfig
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "happy path",
			args: args{config: producerConfigTest{
				brokers: brokersForTest,
				topic:   topicIDForTest,
			}},
			wantErr: false,
		},
		{
			name: "invalid config - missing brokers",
			args: args{
				config: producerConfigTest{
					brokers: []string{},
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid config - missing topic",
			args: args{
				config: producerConfigTest{
					brokers: brokersForTest,
					topic:   "",
				},
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewProducer(tt.args.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewProducer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_producerImplV1_Write(t *testing.T) {
	var (
		config = &producerConfigTest{brokers: brokersForTest, topic: topicIDForTest}
		key    = []byte("key-test")
		value  = []byte("value-test")
	)

	type args struct {
		ctx   context.Context
		key   []byte
		value []byte
	}

	tests := []struct {
		name                   string
		config                 ProducerConfig
		configureKafkagoWriter func(writer *mockKafkagoWriter)
		args                   args
		wantErr                bool
	}{
		{
			name: "happy path",
			args: args{
				ctx:   context.Background(),
				key:   key,
				value: value,
			},
			config: config,
			configureKafkagoWriter: func(writer *mockKafkagoWriter) {
				writer.On("WriteMessages", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			},
			wantErr: false,
		},
		{
			name: "write failure",
			args: args{
				ctx:   context.Background(),
				key:   key,
				value: value,
			},
			config: config,
			configureKafkagoWriter: func(writer *mockKafkagoWriter) {
				writer.On("WriteMessages", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("failed to write to brokers"))
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			p := &Producer{
				config: config,
			}
			kafkagoWriterMock := &mockKafkagoWriter{}
			tt.configureKafkagoWriter(kafkagoWriterMock)
			p.producer = kafkagoWriterMock

			if err := p.Write(tt.args.ctx, tt.args.key, tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Write() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_producerImplV1_WriteValue(t *testing.T) {
	var (
		config = &producerConfigTest{brokers: brokersForTest, topic: topicIDForTest}
		value  = []byte("value-test")
	)

	type args struct {
		ctx   context.Context
		value []byte
	}

	tests := []struct {
		name    string
		config  ProducerConfig
		args    args
		wantErr bool
	}{
		{
			name: "happy path",
			args: args{
				ctx:   context.Background(),
				value: value,
			},
			config:  config,
			wantErr: false,
		},
		{
			name: "write failure",
			args: args{
				ctx:   context.Background(),
				value: value,
			},
			config:  config,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			p := &Producer{
				config: config,
			}
			kafkagoWriterMock := &mockKafkagoWriter{}
			var writeErrMock error
			if tt.wantErr {
				writeErrMock = errors.New("failed to write to brokers")
			}
			kafkagoWriterMock.On("WriteMessages", mock.Anything, mock.Anything, mock.Anything).Return(writeErrMock)
			p.producer = kafkagoWriterMock

			if err := p.WriteValue(tt.args.ctx, tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("WriteValue() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_producerImplV1_Close(t *testing.T) {
	config := &producerConfigTest{brokers: brokersForTest, topic: topicIDForTest}

	tests := []struct {
		name    string
		config  ProducerConfig
		wantErr bool
	}{
		{
			name:    "happy path",
			config:  config,
			wantErr: false,
		},
		{
			name:    "close failure",
			config:  config,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			p := &Producer{
				config: config,
			}
			kafkagoWriterMock := &mockKafkagoWriter{}
			var writeErrMock error
			if tt.wantErr {
				writeErrMock = errors.New("failed to write to brokers")
			}

			kafkagoWriterMock.On("Close", mock.Anything).Return(writeErrMock)
			p.producer = kafkagoWriterMock

			if err := p.Close(); (err != nil) != tt.wantErr {
				t.Errorf("Close() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
