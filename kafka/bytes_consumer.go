package kafka

import (
	"io"
	"sync"
	"time"

	kafkago "github.com/segmentio/kafka-go"
)

// NewBytesConsumer is used to create consumer instance
func NewBytesConsumer(config ConsumerConfig) (*BytesConsumer, error) {
	if err := validateConsumerConfig(config); err != nil {
		return nil, err
	}

	return newBytesConsumer(config, kafkago.NewReader(getKafkaGoConfig(config))), nil
}

func newBytesConsumer(config ConsumerConfig, kafkaReader kafkagoReader) *BytesConsumer {
	bytesConsumer := &BytesConsumer{
		consumer:  newConsumer(config, kafkaReader),
		messageCh: make(chan []byte),
	}

	bytesConsumer.config.Instrumentation().InitializedConsumer()

	return bytesConsumer
}

type BytesConsumer struct {
	*consumer

	messageCh chan []byte
	initOnce  sync.Once
}

// Start returns a channel of message payload sent by producer.
func (b *BytesConsumer) Start() (<-chan []byte, error) {
	b.initOnce.Do(func() {
		b.waitGroup.Add(1)
		b.config.Instrumentation().StartConsumerReadLoop()

		go b.readMessages()

		go func() {
			ticker := time.NewTicker(statsReportInterval)

			for range ticker.C {
				stats := b.kafkaReader.Stats()
				b.config.Instrumentation().ConsumerStats(&stats)
			}
		}()
	})

	return b.messageCh, nil
}

//nolint:funlen
func (b *BytesConsumer) readMessages() {
	defer func() {
		close(b.messageCh)

		b.stopReading()
	}()

	for {
		msg, err := b.consumer.kafkaReader.FetchMessage(b.readerContext)
		if err != nil {
			// m.consumer is closed
			if err == io.EOF {
				b.config.Instrumentation().StopConsumerReadLoop()
				return
			}

			b.config.Instrumentation().ReadConsumerError(err)

			select {
			case <-b.closeChan:
				b.config.Instrumentation().StopConsumerReadLoop()
				return

			default:
				continue
			}
		}

		select {
		case <-b.closeChan:
			b.config.Instrumentation().StopConsumerReadLoop()
			return

		case b.messageCh <- msg.Value:
			err := b.consumer.kafkaReader.CommitMessages(b.readerContext, msg)
			if err != nil {
				b.config.Instrumentation().ConsumerCommitFailed(err)

				if err == io.ErrClosedPipe {
					// end the reading loop due to closed consumer
					return
				}
			}
		}
	}
}
