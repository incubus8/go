package kafka

import (
	"context"
	"testing"
	"time"

	kafkago "github.com/segmentio/kafka-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestNewChannelBasedConsumer(t *testing.T) {
	type args struct {
		config ConsumerConfig
	}

	scenarios := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "happy path",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: false,
		},
		{
			name: "invalid config - missing brokers",
			args: args{
				config: &consumerConfigTest{
					brokers: []string{},
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid config - missing groupID",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: "",
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid config - missing topic",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   "",
				},
			},
			wantErr: true,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.name, func(t *testing.T) {
			_, err := NewChannelBasedConsumer(scenario.args.config)
			assert.Equal(t, scenario.wantErr, err != nil)
		})
	}
}

func TestChannelBasedConsumer_happyPath(t *testing.T) {
	// inputs
	config := &consumerConfigTest{
		brokers: brokersForTest,
		groupID: groupIDForTest,
		topic:   topicIDForTest,
	}
	key := []byte("key-test")
	value := []byte("value-test")

	kafkaMsg := kafkago.Message{Key: key, Value: value}

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	// mocks
	reader := &mockKafkagoReader{}
	reader.On("Close", mock.Anything).Return(nil)
	reader.On("FetchMessage", mock.Anything).Return(kafkaMsg, nil)

	ackDone := make(chan struct{})
	ackFunc := func() {
		close(ackDone)
	}

	// object under test
	consumer, err := newChannelBasedConsumer(config, reader, ackFunc)
	require.NoError(t, err)

	msgCh := make(chan *Message)
	ackCh := make(chan *Message)

	consumer.Start(msgCh, ackCh)

	// read a message
	var msg *Message
	select {
	case msg = <-msgCh:
		assert.NotNil(t, msg)

	case <-ctx.Done():
		assert.Fail(t, "timeout reading from channel")
	}

	// commit the message
	reader.On("CommitMessages", mock.Anything, kafkaMsg).Return(nil)

	select {
	case ackCh <- msg:
		// happy path

	case <-ctx.Done():
		assert.Fail(t, "timeout waiting for commit from channel")
	}

	// wait for ack done
	select {
	case <-ackDone:
		// happy path

	case <-ctx.Done():
		assert.Fail(t, "timeout waiting for commit from channel")
	}

	// clean up
	err = consumer.Close()
	assert.NoError(t, err)

	// validate reader usage
	assert.True(t, reader.AssertExpectations(t))
}
