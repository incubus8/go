# Kafka commons library

An opinionated kafka wrapper based on [segmentio/kafka-go](https://github.com/segmentio/kafka-go).

## Usage

See the [examples directory](./examples) to see how to use the library.
