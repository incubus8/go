package kafka

import (
	"time"
)

const (
	// Time interval for reporting stats
	statsReportInterval = 10 * time.Second
)
