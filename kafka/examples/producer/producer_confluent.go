package main

import (
	"context"
	"crypto/tls"
	"log"
	"os"

	"gitlab.com/incubus8/go/kafka"
)

// Implement ProducerConfig
type ccloudProducerConfig struct {
	brokers []string
	topic   string
	tls     *tls.Config
	sasl    *kafka.SaslConfig
}

func (p ccloudProducerConfig) Instrumentation() kafka.Instrumentation {
	return nil
}

func (p ccloudProducerConfig) Brokers() []string {
	return p.brokers
}

func (p ccloudProducerConfig) Topic() string {
	return p.topic
}

func (p ccloudProducerConfig) MaxRetry() int {
	return 3
}

func (p ccloudProducerConfig) CompressionCodec() string {
	return ""
}

func (p ccloudProducerConfig) IsAsync() bool {
	return false
}

func (p ccloudProducerConfig) Acks() kafka.AckType {
	return kafka.AckLeader
}

func (p ccloudProducerConfig) TLS() *tls.Config {
	return p.tls
}

func (p ccloudProducerConfig) Sasl() *kafka.SaslConfig {
	return p.sasl
}

func Example() {
	brokers := []string{os.Getenv("CONFLUENT_BROKERS")}
	topic := os.Getenv("CONFLUENT_TOPIC")
	ccloudUsername := os.Getenv("CONFLUENT_USERNAME")
	ccloudPassword := os.Getenv("CONFLUENT_PASSWORD")

	// Sample message
	messages := [][]byte{
		[]byte("this"),
		[]byte("is"),
		[]byte("kafkaaaaaaa"),
	}

	// Build the config
	config := ccloudProducerConfig{
		brokers: brokers,
		topic:   topic,
		tls:     &tls.Config{}, // Use the default tls config
		sasl: &kafka.SaslConfig{
			Username: ccloudUsername,
			Password: ccloudPassword,
		},
	}

	// Initialize the producer
	p, err := kafka.NewProducer(config)
	if err != nil {
		log.Fatal(err)
	}

	// Write the messages to kafka using round robin (no key defined)
	for _, msg := range messages {
		p.WriteValue(context.Background(), msg)
		if err != nil {
			log.Fatal(err)
		}
	}

	err = p.Close()
	if err != nil {
		log.Fatal(err)
	}
}
