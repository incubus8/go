package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/incubus8/go/kafka"
)

// Implement ConsumerConfig for use in our use case
type ccloudConsumerConfig struct {
	brokers []string
	groupID string
	topic   string
	tls     *tls.Config
	sasl    *kafka.SaslConfig
}

// DebugLogger implements ConsumerConfig
func (c *ccloudConsumerConfig) DebugLogger() kafka.KLogger {
	return nil
}

// ReadFromFirstOffset implements ConsumerConfig
func (c *ccloudConsumerConfig) ReadFromFirstOffset() bool {
	return false
}

// Instrumentation implements ConsumerConfig
func (c *ccloudConsumerConfig) Instrumentation() kafka.Instrumentation {
	return nil
}

// Brokers implements ConsumerConfig
func (c *ccloudConsumerConfig) Brokers() []string {
	return c.brokers
}

// GroupID implements ConsumerConfig
func (c *ccloudConsumerConfig) GroupID() string {
	return c.groupID
}

// Topic implements ConsumerConfig
func (c *ccloudConsumerConfig) Topic() string {
	return c.topic
}

// TLS implements ConsumerConfig
func (c *ccloudConsumerConfig) TLS() *tls.Config {
	return c.tls
}

// Sasl implements ConsumerConfig
func (c *ccloudConsumerConfig) Sasl() *kafka.SaslConfig {
	return c.sasl
}

func (c *ccloudConsumerConfig) MinBytes() int {
	return 0 // use default setting
}

func (c *ccloudConsumerConfig) MaxBytes() int {
	return 0 // use default setting
}

func (c *ccloudConsumerConfig) MaxWait() time.Duration {
	return 0 // use default setting
}

func (c *ccloudConsumerConfig) CommitInterval() time.Duration {
	return 0 // use default setting
}

func (c *ccloudConsumerConfig) RebalanceTimeout() time.Duration {
	return 0 // use default setting
}

func Example() {
	brokers := []string{os.Getenv("CONFLUENT_BROKERS")}
	groupID := os.Getenv("CONFLUENT_GROUP_ID")
	topic := os.Getenv("CONFLUENT_TOPIC")
	ccloudUsername := os.Getenv("CONFLUENT_USERNAME")
	ccloudPassword := os.Getenv("CONFLUENT_PASSWORD")

	// Build the config
	config := &ccloudConsumerConfig{
		brokers: brokers,
		groupID: groupID,
		topic:   topic,
		tls:     &tls.Config{}, // Use the default tls config
		sasl: &kafka.SaslConfig{
			Username: ccloudUsername,
			Password: ccloudPassword,
		},
	}

	// Initialize a bytes consumer
	consumer, err := kafka.NewBytesConsumer(config)
	if err != nil {
		log.Fatal(err)
	}

	// Start the consumer
	msgChan, err := consumer.Start()
	if err != nil {
		log.Fatal(err)
	}

	// Read messages
	for msg := range msgChan {
		fmt.Println(string(msg))
	}

	// Close the consumer
	err = consumer.Close()
	if err != nil {
		log.Fatal(err)
	}
}
