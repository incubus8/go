package kafka

import (
	"crypto/tls"
	"fmt"
	"testing"
	"time"

	kafkago "github.com/segmentio/kafka-go"
)

var (
	brokersForTest   = []string{"broker1"}
	groupIDForTest   = "groupID-test"
	topicIDForTest   = "topic-test"
	tlsConfigForTest = &tls.Config{
		ClientAuth: 0,
	}
	saslConfigForTest = &SaslConfig{
		Username: "test-username",
		Password: "test-password",
	}
)

type consumerConfigTest struct {
	brokers    []string
	groupID    string
	topic      string
	tls        *tls.Config
	saslConfig *SaslConfig
}

func (c *consumerConfigTest) MinBytes() int {
	return 0
}

func (c *consumerConfigTest) MaxBytes() int {
	return 0
}

func (c *consumerConfigTest) MaxWait() time.Duration {
	return 0
}

func (c *consumerConfigTest) CommitInterval() time.Duration {
	return 0
}

func (c *consumerConfigTest) RebalanceTimeout() time.Duration {
	return 0
}

func (c *consumerConfigTest) DebugLogger() KLogger {
	return nil
}

func (c *consumerConfigTest) ReadFromFirstOffset() bool {
	return false
}

func (c *consumerConfigTest) Instrumentation() Instrumentation {
	return &NoopInstrumentation{}
}

func (c *consumerConfigTest) Brokers() []string {
	return c.brokers
}

func (c *consumerConfigTest) GroupID() string {
	return c.groupID
}

func (c *consumerConfigTest) Topic() string {
	return c.topic
}

func (c *consumerConfigTest) TLS() *tls.Config {
	return c.tls
}

func (c *consumerConfigTest) Sasl() *SaslConfig {
	return c.saslConfig
}

func TestValidateConsumerConfig(t *testing.T) {
	type args struct {
		config ConsumerConfig
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "happy path - no sasl",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: false,
		},
		{
			name: "happy path - with sasl, plaintext",
			args: args{
				config: &consumerConfigTest{
					brokers:    brokersForTest,
					groupID:    groupIDForTest,
					topic:      topicIDForTest,
					tls:        tlsConfigForTest,
					saslConfig: saslConfigForTest,
				},
			},
			wantErr: false,
		},
		{
			name: "missing brokers",
			args: args{
				config: &consumerConfigTest{
					brokers: []string{},
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},

		{
			name: "missing groupID",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: "",
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "missing topic",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   "",
				},
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if err := validateConsumerConfig(tt.args.config); (err != nil) != tt.wantErr {
				t.Errorf("ValidateConsumer() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// NoopInstrumentation implements Instrumentation
type NoopInstrumentation struct{}

func (n NoopInstrumentation) Printf(msg string, args ...interface{}) {
	fmt.Printf(msg, args...)
}

func (n NoopInstrumentation) ConsumerStats(stats *kafkago.ReaderStats) {
}

func (n NoopInstrumentation) ConsumerCommitFailed(_ error) {
}

// InitializedProducer implements Instrumentation
func (n NoopInstrumentation) InitializedProducer() {
}

// ProducerStats implements Instrumentation
func (n NoopInstrumentation) ProducerStats(stats *kafkago.WriterStats) {
}

// ProducerDuration implements Instrumentation
func (n NoopInstrumentation) ProducerDuration(start time.Time) {
}

// InitializedConsumer implements Instrumentation
func (n NoopInstrumentation) InitializedConsumer() {
}

// ClosedConsumer implements Instrumentation
func (n NoopInstrumentation) ClosedConsumer() {
}

// StartConsumerReadLoop implements Instrumentation
func (n NoopInstrumentation) StartConsumerReadLoop() {
}

// StopConsumerReadLoop implements Instrumentation
func (n NoopInstrumentation) StopConsumerReadLoop() {
}

// ReadConsumerError implements Instrumentation
func (n NoopInstrumentation) ReadConsumerError(err error) {
}
