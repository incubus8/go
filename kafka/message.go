package kafka

import (
	"time"

	kafkago "github.com/segmentio/kafka-go"
)

func NewMessage(topic string, partition int, offset int64, key, value []byte, createdAt time.Time) *Message {
	return &Message{
		original: &kafkago.Message{
			Topic:     topic,
			Partition: partition,
			Offset:    offset,
			Key:       key,
			Value:     value,
			Time:      createdAt,
		},
	}
}

// Message defines the response from kafka
type Message struct {
	original *kafkago.Message
}

func (m *Message) Topic() string {
	return m.original.Topic
}

func (m *Message) Partition() int {
	return m.original.Partition
}

func (m *Message) Offset() int64 {
	return m.original.Offset
}

func (m *Message) Key() []byte {
	return m.original.Key
}

func (m *Message) Value() []byte {
	return m.original.Value
}

func (m *Message) CreatedAt() time.Time {
	return m.original.Time
}
