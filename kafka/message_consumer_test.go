package kafka

import (
	"errors"
	"io"
	"testing"
	"time"

	kafkago "github.com/segmentio/kafka-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestNewMessageConsumer(t *testing.T) {
	type args struct {
		config ConsumerConfig
	}

	scenarios := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "happy path",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: false,
		},
		{
			name: "invalid config - missing brokers",
			args: args{
				config: &consumerConfigTest{
					brokers: []string{},
					groupID: groupIDForTest,
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid config - missing groupID",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: "",
					topic:   topicIDForTest,
				},
			},
			wantErr: true,
		},
		{
			name: "invalid config - missing topic",
			args: args{
				config: &consumerConfigTest{
					brokers: brokersForTest,
					groupID: groupIDForTest,
					topic:   "",
				},
			},
			wantErr: true,
		},
	}

	for _, s := range scenarios {
		scenario := s
		t.Run(scenario.name, func(t *testing.T) {
			_, err := NewMessageConsumer(scenario.args.config)
			assert.Equal(t, scenario.wantErr, err != nil)
		})
	}
}

func TestMessageConsumer_Start(t *testing.T) {
	var (
		config = &consumerConfigTest{
			brokers: brokersForTest,
			groupID: groupIDForTest,
			topic:   topicIDForTest,
		}
		key   = []byte("key-test")
		value = []byte("value-test")
	)

	scenarios := []struct {
		name                       string
		config                     ConsumerConfig
		configureKafkagoReaderMock func(reader *mockKafkagoReader)
		wantMessage                *Message
		wantErr                    bool
		wantConsumerClosed         bool
	}{
		{
			name:   "happy path",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{Key: key, Value: value}, nil)
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(nil)
			},
			wantMessage:        newMessage(key, value),
			wantErr:            false,
			wantConsumerClosed: false,
		},
		{
			name:   "read message failure",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{Key: key, Value: value},
					errors.New("failed to read message"))
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(nil)
			},
			wantMessage:        nil,
			wantErr:            true,
			wantConsumerClosed: false,
		},
		{
			name:   "failed to commit messages to kafka - unknown error",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{Key: key, Value: value}, nil)
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(errors.New("unknown error"))
			},
			wantMessage:        newMessage(key, value),
			wantErr:            false,
			wantConsumerClosed: false,
		},
		{
			name:   "failed to commit messages to kafka - closed pipe",
			config: config,
			configureKafkagoReaderMock: func(reader *mockKafkagoReader) {
				reader.On("Close", mock.Anything).Return(nil)
				reader.On("FetchMessage", mock.Anything).Return(kafkago.Message{Key: key, Value: value}, nil)
				reader.On("CommitMessages", mock.Anything, mock.Anything).Return(io.ErrClosedPipe)
			},
			wantMessage:        newMessage(key, value),
			wantErr:            false,
			wantConsumerClosed: true,
		},
	}

	for _, s := range scenarios {
		scenario := s

		t.Run(scenario.name, func(t *testing.T) {
			kafkagoReaderMock := &mockKafkagoReader{}
			scenario.configureKafkagoReaderMock(kafkagoReaderMock)

			c := newMessageConsumer(scenario.config, kafkagoReaderMock)

			msgChan, _ := c.Start()

			if scenario.wantErr {
				select {
				case <-msgChan:
					assert.Fail(t, "StartReadMessages(), there should be no messages in message channel")

				default:
					// happy path
				}
			} else {
				select {
				case msg := <-msgChan:
					assert.Equal(t, msg, scenario.wantMessage, scenario.name)

				case <-time.After(1 * time.Second):
					assert.Fail(t, "timeout reading from channel")
				}
			}

			err := c.Close()
			assert.NoError(t, err, scenario.name)
		})
	}
}

func newMessage(key, value []byte) *Message {
	return &Message{
		original: &kafkago.Message{
			Key:   key,
			Value: value,
		},
	}
}
