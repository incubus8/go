package kafka

import (
	"context"
	"io"
	"sync"

	kafkago "github.com/segmentio/kafka-go"
)

// NewMessageConsumer is used to create consumer instance
func NewMessageConsumer(config ConsumerConfig) (*MessageConsumer, error) {
	if err := validateConsumerConfig(config); err != nil {
		return nil, err
	}

	return newMessageConsumer(config, kafkago.NewReader(getKafkaGoConfig(config))), nil
}

func newMessageConsumer(config ConsumerConfig, reader kafkagoReader) *MessageConsumer {
	config.Instrumentation().InitializedConsumer()

	return &MessageConsumer{
		consumer:  newConsumer(config, reader),
		messageCh: make(chan *Message),
	}
}

type MessageConsumer struct {
	*consumer

	messageCh chan *Message
	initOnce  sync.Once
}

// Start returns a channel of message payload sent by producer.
func (m *MessageConsumer) Start() (<-chan *Message, error) {
	m.initOnce.Do(func() {
		m.waitGroup.Add(1)

		m.config.Instrumentation().StartConsumerReadLoop()

		go m.readMessages()
	})

	return m.messageCh, nil
}

//nolint:funlen
func (m *MessageConsumer) readMessages() {
	defer func() {
		close(m.messageCh)

		m.stopReading()
	}()

	for {
		select {
		case <-m.closeChan:
			m.config.Instrumentation().StopConsumerReadLoop()
			return

		default:
			// happy path
		}

		msg, err := m.consumer.kafkaReader.FetchMessage(context.Background())
		if err != nil {
			// m.consumer is closed
			if err == io.EOF {
				m.config.Instrumentation().StopConsumerReadLoop()
				return
			}

			m.config.Instrumentation().ReadConsumerError(err)

			continue
		}

		select {
		case <-m.closeChan:
			m.config.Instrumentation().StopConsumerReadLoop()
			return

		case m.messageCh <- &Message{original: &msg}:
			err := m.consumer.kafkaReader.CommitMessages(context.Background(), msg)
			if err != nil {
				m.config.Instrumentation().ConsumerCommitFailed(err)

				if err == io.ErrClosedPipe {
					// end the reading loop due to closed consumer
					return
				}
			}
		}
	}
}
