package kafka

import (
	"context"
	"crypto/tls"
	"sync"
	"time"

	kafkago "github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/plain"
)

const (
	defaultCommitInterval   = 1 * time.Second
	defaultMaxWait          = 500 * time.Millisecond
	defaultRebalanceTimeout = 30 * time.Second

	defaultMinBytes = 1    // 1 Byte
	defaultMaxBytes = 10e6 // 10MB
)

// ConsumerConfig includes settings for creating a kafka consumer
type ConsumerConfig interface {
	Instrumentation() Instrumentation

	// Brokers specifies addresses of brokers
	Brokers() []string

	// GroupID specifies consumer group ID
	GroupID() string

	// Topic specifies kafka topic
	Topic() string

	// It(optional) specifies TSL settings, nil value means TLS is disabled. Required if using confluent
	// cloud clusters.
	TLS() *tls.Config

	// It(optional) specifies SASL related settings, nil value means consumer will connect to brokers
	// without auth info. Required if using confluent cloud clusters.
	Sasl() *SaslConfig

	// (optional) Logging for debugging Kafka events
	DebugLogger() KLogger

	// This method should return false (read from last offset) for most cases.
	// Offset determines from whence the consumer group should begin
	// consuming when it finds a partition without a committed offset.
	ReadFromFirstOffset() bool

	// Min and Max bytes per read request (defaults to min: 10KB and max: 10MB)
	MinBytes() int
	MaxBytes() int

	// (optional) Maximum wait for messages to be read. Default: 1 second
	MaxWait() time.Duration

	// (optional) Interval between read commits Default: 1 second
	CommitInterval() time.Duration

	// (optional) Timeout for rebalance events. Default: 30 seconds
	RebalanceTimeout() time.Duration
}

// validateConsumerConfig checks whether ConsumerConfig is valid or not
func validateConsumerConfig(config ConsumerConfig) error {
	if config == nil {
		return ErrConfigNil
	}

	if len(config.Brokers()) < 1 {
		return ErrConfigEmptyBrokers
	}

	if config.Topic() == "" {
		return ErrConfigEmptyTopic
	}

	if config.GroupID() == "" {
		return ErrConfigEmptyGroupID
	}

	return nil
}

func newConsumer(config ConsumerConfig, reader kafkagoReader) *consumer {
	ctx, cancel := context.WithCancel(context.Background())

	return &consumer{
		config:        config,
		kafkaReader:   reader,
		closeChan:     make(chan struct{}),
		readerContext: ctx,
		readerCancel:  cancel,
	}
}

// consumer provides APIs for consuming messages from kafka
type consumer struct {
	config        ConsumerConfig
	kafkaReader   kafkagoReader
	closeChan     chan struct{}
	waitGroup     sync.WaitGroup
	closeOnce     sync.Once
	readerContext context.Context
	readerCancel  context.CancelFunc
}

// Close stops consumer and release resources
func (c *consumer) Close() error {
	var err error

	c.closeOnce.Do(func() {
		close(c.closeChan)

		c.readerCancel()

		err = c.kafkaReader.Close()

		c.waitGroup.Wait()

		c.config.Instrumentation().ClosedConsumer()
	})

	return err
}

func (c *consumer) stopReading() {
	c.readerCancel()

	// must precede the call to Close() or we will livelock waiting for the waitgroup
	c.waitGroup.Done()

	_ = c.Close()
}

func getKafkaGoConfig(config ConsumerConfig) kafkago.ReaderConfig {
	return kafkago.ReaderConfig{
		Brokers:               config.Brokers(),
		GroupID:               config.GroupID(),
		Topic:                 config.Topic(),
		Dialer:                buildDialer(config),
		WatchPartitionChanges: true,
		MinBytes:              getMinBytes(config),
		MaxBytes:              getMaxBytes(config),
		MaxWait:               getMaxWait(config),
		CommitInterval:        getCommitInterval(config),
		RebalanceTimeout:      getRebalanceTimeout(config),
		StartOffset:           getStartOffset(config),
		Logger:                config.DebugLogger(),
		ErrorLogger:           config.Instrumentation(),
	}
}

func buildDialer(config ConsumerConfig) *kafkago.Dialer {
	dialer := &kafkago.Dialer{
		DualStack: true,
		Timeout:   10 * time.Second,
	}

	if config.Sasl() != nil {
		dialer.SASLMechanism = plain.Mechanism{
			Username: config.Sasl().Username,
			Password: config.Sasl().Password,
		}
	}

	if config.TLS() != nil {
		dialer.TLS = config.TLS()
	}

	return dialer
}

func getMinBytes(config ConsumerConfig) int {
	if config.MinBytes() > 0 {
		return config.MinBytes()
	}

	return defaultMinBytes
}

func getMaxBytes(config ConsumerConfig) int {
	if config.MaxBytes() > 0 {
		return config.MaxBytes()
	}

	return defaultMaxBytes
}

func getMaxWait(config ConsumerConfig) time.Duration {
	if int64(config.MaxWait()) > 0 {
		return config.MaxWait()
	}

	return defaultMaxWait
}

func getCommitInterval(config ConsumerConfig) time.Duration {
	if int64(config.CommitInterval()) > 0 {
		return config.CommitInterval()
	}

	return defaultCommitInterval
}

func getRebalanceTimeout(config ConsumerConfig) time.Duration {
	if int64(config.RebalanceTimeout()) > 0 {
		return config.RebalanceTimeout()
	}

	return defaultRebalanceTimeout
}

func getStartOffset(config ConsumerConfig) int64 {
	if config.ReadFromFirstOffset() {
		return kafkago.FirstOffset
	}

	return kafkago.LastOffset
}
