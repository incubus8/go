package kafka

import (
	"errors"
)

var (
	// ErrConsumerStartedReadBytes specifies the loop of reading bytes was already started
	ErrConsumerStartedReadBytes = errors.New("the loop of reading bytes was already started")
	// ErrConsumerStartedReadMessages specifies the loop of reading messages was already started
	ErrConsumerStartedReadMessages = errors.New("the loop of reading messages was already started")
	// ErrConsumerUnexpected specifies unexpected error
	ErrConsumerUnexpected = errors.New("consumer unexpected error")
	// ErrConfigNil specifies no provided ConsumerConfig
	ErrConfigNil = errors.New("nil Config")
	// ErrConfigEmptyBrokers specifies provided broker list is empty
	ErrConfigEmptyBrokers = errors.New("brokers is required")
	// ErrConfigEmptyTopic specifies provided topic is empty
	ErrConfigEmptyTopic = errors.New("topic is required")
	// ErrConfigEmptyGroupID specifies provided groupID is empty
	ErrConfigEmptyGroupID = errors.New("groupID is required")
)
