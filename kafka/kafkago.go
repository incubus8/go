package kafka

import (
	"context"

	kafkago "github.com/segmentio/kafka-go"
)

// kafkagoReader is for kafka-go's Reader mock
//
//go:generate mockery -name=kafkagoReader -case underscore -testonly -inpkg
type kafkagoReader interface {
	Close() error

	// ReadMessage reads and return the next message from the r. The method call
	// blocks until a message becomes available, or an error occurs. The program
	// may also specify a context to asynchronously cancel the blocking operation.
	//
	// The method returns io.EOF to indicate that the reader has been closed.
	//
	// If consumer groups are used, ReadMessage will automatically commit the
	// offset when called. Note that this could result in an offset being committed
	// before the message is fully processed.
	//
	// If more fine grained control of when offsets are committed is required, it
	// is recommended to use FetchMessage with CommitMessages instead.
	ReadMessage(ctx context.Context) (kafkago.Message, error)

	// FetchMessage reads and return the next message from the r. The method call
	// blocks until a message becomes available, or an error occurs. The program
	// may also specify a context to asynchronously cancel the blocking operation.
	//
	// The method returns io.EOF to indicate that the reader has been closed.
	//
	// FetchMessage does not commit offsets automatically when using consumer groups.
	// Use CommitMessages to commit the offset.
	FetchMessage(ctx context.Context) (kafkago.Message, error)

	// CommitMessages commits the list of messages passed as argument. The program
	// may pass a context to asynchronously cancel the commit operation when it was
	// configured to be blocking.
	CommitMessages(ctx context.Context, msgs ...kafkago.Message) error

	Stats() kafkago.ReaderStats
}

//go:generate mockery -name=kafkagoWriter -case underscore -testonly -inpkg
type kafkagoWriter interface {
	WriteMessages(ctx context.Context, msgs ...kafkago.Message) error
	Close() error
	Stats() kafkago.WriterStats
}

// SaslConfig defines SASL related settings
type SaslConfig struct {
	Username string
	Password string
}

// KLogger interface for Kafka events and/or errors
type KLogger kafkago.Logger
