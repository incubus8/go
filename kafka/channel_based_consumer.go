package kafka

import (
	"context"
	"errors"
	"io"
	"sync"
	"time"

	kafkago "github.com/segmentio/kafka-go"
)

func NewChannelBasedConsumer(config ConsumerConfig) (*ChannelBasedConsumer, error) {
	if err := validateConsumerConfig(config); err != nil {
		return nil, err
	}

	reader := kafkago.NewReader(getKafkaGoConfig(config))

	ackFunc := func() {}

	return newChannelBasedConsumer(config, reader, ackFunc)
}

func newChannelBasedConsumer(config ConsumerConfig, reader kafkagoReader, ackFunc func()) (*ChannelBasedConsumer, error) {
	fetchContext, fetchCtxCancel := context.WithCancel(context.Background())

	config.Instrumentation().InitializedConsumer()

	return &ChannelBasedConsumer{
		consumer:       newConsumer(config, reader),
		fetchContext:   fetchContext,
		fetchCtxCancel: fetchCtxCancel,
		ackFunc:        ackFunc,
	}, nil
}

type ChannelBasedConsumer struct {
	*consumer

	fetchContext   context.Context
	fetchCtxCancel context.CancelFunc

	messageCh chan *Message

	acknowledgeCh chan *Message
	// called after afk message processing (should only be used for testing)
	ackFunc func()

	initOnce sync.Once
}

// Start begins the flow of messages.
// WARNING: If messages are acknowledged in a different order from the order in which they were read, there exists a possibility that
// messages could be lost.
// If an out-of-order write occurs and then a rebalance or crash occurs then the messages that have been read but not acknowledged to
// be lost. (Yes, they still exist in Kafka but the read offsets will be such that they will not be read again without some external action;
// like changing the read offset start point)
// NOTE: This method starts two goroutines for reading and committing messages. It should be called synchronously as calling using
// `go myConsumer.Start(msg, ack)` creates the possibility for a data race between this method and consumer.Close()
func (s *ChannelBasedConsumer) Start(messageCh, acknowledgeCh chan *Message) {
	s.initOnce.Do(func() {
		s.waitGroup.Add(1)

		s.config.Instrumentation().StartConsumerReadLoop()

		s.messageCh = messageCh
		s.acknowledgeCh = acknowledgeCh

		go s.readMessages()

		go s.commitMessages()

		go func() {
			ticker := time.NewTicker(statsReportInterval)

			for range ticker.C {
				stats := s.kafkaReader.Stats()
				s.config.Instrumentation().ConsumerStats(&stats)
			}
		}()
	})
}

func (s *ChannelBasedConsumer) readMessages() {
	defer func() {
		close(s.messageCh)

		s.stopReading()
	}()

	for {
		msg, err := s.consumer.kafkaReader.FetchMessage(s.fetchContext)
		if err != nil {
			switch {
			case errors.Is(err, io.EOF) ||
				errors.Is(err, context.DeadlineExceeded) ||
				errors.Is(err, context.Canceled):
				// s.consumer is closed
				s.config.Instrumentation().StopConsumerReadLoop()
				return

			default:
				s.config.Instrumentation().ReadConsumerError(err)
				continue
			}
		}

		select {
		case s.messageCh <- &Message{original: &msg}:
			// happy path

		case <-s.closeChan:
			s.config.Instrumentation().StopConsumerReadLoop()
			return
		}
	}
}

func (s *ChannelBasedConsumer) commitMessages() {
	flushTicker := time.NewTicker(getCommitInterval(s.config))

	var messagesToCommit []kafkago.Message

	for {
		select {
		case msg := <-s.acknowledgeCh:
			messagesToCommit = append(messagesToCommit, *msg.original)

		case <-s.closeChan:
			s.config.Instrumentation().StopConsumerReadLoop()
			return

		case <-flushTicker.C:
			s.flush(messagesToCommit)

			messagesToCommit = nil
		}
	}
}

func (s *ChannelBasedConsumer) flush(messagesToCommit []kafkago.Message) {
	err := s.consumer.kafkaReader.CommitMessages(s.fetchContext, messagesToCommit...)

	s.ackFunc()

	if err != nil {
		s.config.Instrumentation().ConsumerCommitFailed(err)

		if err == io.ErrClosedPipe {
			// end the reading loop due to closed consumer
			return
		}
	}
}

func (s *ChannelBasedConsumer) Close() error {
	s.fetchCtxCancel()

	return s.consumer.Close()
}
