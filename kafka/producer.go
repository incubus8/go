package kafka

import (
	"context"
	"crypto/tls"
	"time"

	kafkago "github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/plain"
)

// AckType defines data type of producer's Acks
type AckType int

const (
	// AckAll, producer will wait for acknowledgements from all replicas
	AckAll AckType = -1
	// AckNo, producer will not to wait for acknowledgment from kafka at all
	AckNo AckType = 0
	// AckLeader,  producer will wait for acknowledgment from leader
	AckLeader AckType = 1

	defaultMaxRetry = 3
)

// ProducerConfig defines settings of producer config
type ProducerConfig interface {
	Instrumentation() Instrumentation

	// Brokers specifies the addresses of brokers
	Brokers() []string
	// Topic specifies kafka topic
	Topic() string
	// MaxRetry(optional) specifies the maximum number of retries on writing event to brokers.
	// If it is less than 1, the default value 3 will be used
	MaxRetry() int
	// CompressionCodec(optional) specifies which type of compression method will be used. It can be
	// `gzip`, `snappy`, `lz4` or `zstd`. Empty string means no compression.
	CompressionCodec() string
	// IsAsync(optional), default value is `false`, true means producer never blocks on writing messages
	IsAsync() bool
	// Acks specifies AckType. It can be either AckAll, AckNo or AckLeader.
	// AckLeader is good for most of cases.
	Acks() AckType
	// It(optional) specifies TSL settings, nil value means TLS is disabled. Required if using confluent cloud
	// clusters.
	TLS() *tls.Config
	// It specifies SASL related settings, nil value means producer will connect to brokers
	// without auth info. Required if using confluent cloud clusters
	Sasl() *SaslConfig
}

// NewProducer is used to create Producer instance
func NewProducer(config ProducerConfig) (*Producer, error) {
	if err := validateProducerConfig(config); err != nil {
		return nil, err
	}

	writer := &kafkago.Writer{
		Addr:         kafkago.TCP(config.Brokers()[0]),
		Topic:        config.Topic(),
		MaxAttempts:  writerConfigGetRetries(config),
		RequiredAcks: writerConfigGetAcks(config),
		Async:        config.IsAsync(),
		Compression:  writerConfigGetCompression(config),
		ErrorLogger:  config.Instrumentation(),
		Transport:    writerBuildTransport(config),
	}

	p := &Producer{
		config:   config,
		producer: writer,
	}

	go func() {
		ticker := time.NewTicker(statsReportInterval)

		for range ticker.C {
			stats := p.producer.Stats()
			p.config.Instrumentation().ProducerStats(&stats)
		}
	}()

	p.config.Instrumentation().InitializedProducer()

	return p, nil
}

func writerBuildTransport(config ProducerConfig) kafkago.RoundTripper {
	transport := &kafkago.Transport{}

	if config.TLS() != nil {
		transport.TLS = config.TLS()
	}

	if config.Sasl() != nil {
		transport.SASL = plain.Mechanism{
			Username: config.Sasl().Username,
			Password: config.Sasl().Password,
		}
	}

	return transport
}

// Producer provides APIs for sending messages to kafka
type Producer struct {
	producer kafkagoWriter
	config   ProducerConfig
}

// Write writes message to kafka. The balancer is Hash, The same key will be distributed
// to the same partition.
func (p *Producer) Write(ctx context.Context, key, value []byte) error {
	defer p.config.Instrumentation().ProducerDuration(time.Now())

	return p.producer.WriteMessages(ctx, kafkago.Message{Key: key, Value: value})
}

// WriteValue writes values to kafka, RoundRobin balancer will be used to distribute these messages
// among the partitions
func (p *Producer) WriteValue(ctx context.Context, value []byte) error {
	defer p.config.Instrumentation().ProducerDuration(time.Now())

	return p.producer.WriteMessages(ctx, kafkago.Message{Key: nil, Value: value})
}

// Close shutdowns the producer
func (p *Producer) Close() error {
	p.config.Instrumentation().ClosedConsumer()
	return p.producer.Close()
}

func writerConfigGetCompression(config ProducerConfig) kafkago.Compression {
	switch config.CompressionCodec() {
	case "gzip":
		return kafkago.Gzip

	case "snappy:":
		return kafkago.Snappy

	case "lz4":
		return kafkago.Lz4

	case "zstd":
		return kafkago.Zstd

	default:
		return 0
	}
}

func writerConfigGetRetries(config ProducerConfig) int {
	if config.MaxRetry() > 0 {
		return config.MaxRetry()
	}

	return defaultMaxRetry
}

func writerConfigGetAcks(config ProducerConfig) kafkago.RequiredAcks {
	switch config.Acks() {
	case AckAll:
		return kafkago.RequireAll

	case AckNo:
		return kafkago.RequireNone

	case AckLeader:
		return kafkago.RequireOne

	default:
		return kafkago.RequireAll
	}
}

// validateProducerConfig checks whether ProducerConfig is valid or not
func validateProducerConfig(config ProducerConfig) error {
	if config == nil {
		return ErrConfigNil
	}

	if len(config.Brokers()) == 0 {
		return ErrConfigEmptyBrokers
	}

	if config.Topic() == "" {
		return ErrConfigEmptyTopic
	}

	return nil
}
