package kafkastats

import (
	"errors"
	"testing"
	"time"

	kafkago "github.com/segmentio/kafka-go"
	"github.com/stretchr/testify/mock"
	"go.uber.org/zap"
)

func TestInstrumentation_ConsumerStats(t *testing.T) {
	topic := "test"
	stats := &kafkago.ReaderStats{
		Messages:   10,
		Rebalances: 2,
		Errors:     1,
	}

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}

	for _, key := range []string{
		statsKeyConsumerDials, statsKeyConsumerFetches, statsKeyConsumerMessages, statsKeyConsumerBytes,
		statsKeyConsumerRebalances, statsKeyConsumerTimeouts, statsKeyConsumerErrors,
	} {
		mockStats.On("Count", key, mock.Anything, topicTagName+topic)
	}

	for _, key := range []string{
		statsKeyConsumerDialTime, statsKeyConsumerReadTime, statsKeyConsumerFetchSize,
		statsKeyConsumerFetchBytes, statsKeyConsumerOffset, statsKeyConsumerLag,
	} {
		mockStats.On("Gauge", key, mock.Anything, topicTagName+topic)
	}

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.ConsumerStats(stats)
}

func TestInstrumentation_ConsumerCommitFailed(t *testing.T) {
	topic := "test"
	err := errors.New("foo")

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("Error", mock.Anything, zap.Error(err))
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}
	mockStats.On("Count1", statsKeyConsumerCommitFailed, topicTagName+topic)

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.ConsumerCommitFailed(err)
}

func TestInstrumentation_InitializedConsumer(t *testing.T) {
	topic := "test"

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("Info", mock.Anything)
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}
	mockStats.On("Count1", statsKeyConsumerEvent, topicTagName+topic, mock.Anything)

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.InitializedConsumer()
}

func TestInstrumentation_ClosedConsumer(t *testing.T) {
	topic := "test"

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("Info", mock.Anything)
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}
	mockStats.On("Count1", statsKeyConsumerEvent, topicTagName+topic, mock.Anything)

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.ClosedConsumer()
}

func TestInstrumentation_StartConsumerReadLoop(t *testing.T) {
	topic := "test"

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("Info", mock.Anything)
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}
	mockStats.On("Count1", statsKeyConsumerEvent, topicTagName+topic, mock.Anything)

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.StopConsumerReadLoop()
}

func TestInstrumentation_StopConsumerReadLoop(t *testing.T) {
	topic := "test"

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("Info", mock.Anything)
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}
	mockStats.On("Count1", statsKeyConsumerEvent, topicTagName+topic, mock.Anything)

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.StopConsumerReadLoop()
}

func TestInstrumentation_ReadConsumerError(t *testing.T) {
	topic := "test"
	err := errors.New("foo")

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("Error", mock.Anything, zap.Error(err))
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.ReadConsumerError(err)
}

func TestInstrumentation_InitializedProducer(t *testing.T) {
	topic := "test"

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("Info", mock.Anything)
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}
	mockStats.On("Count1", statsKeyProducerEvent, topicTagName+topic, mock.Anything)

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.InitializedProducer()
}

func TestInstrumentation_ProducerStats(t *testing.T) {
	topic := "test"
	stats := &kafkago.WriterStats{
		Messages: 10,
		Errors:   1,
	}

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}

	for _, key := range []string{
		statsKeyProducerWrites, statsKeyProducerMessages, statsKeyProducerBytes, statsKeyProducerErrors,
	} {
		mockStats.On("Count", key, mock.Anything, topicTagName+topic)
	}

	for _, key := range []string{
		statsKeyProducerBatchTime, statsKeyProducerWriteTime, statsKeyProducerWaitTime, statsKeyProducerRetries,
		statsKeyProducerBatchSize, statsKeyProducerBatchBytes,
	} {
		mockStats.On("Gauge", key, mock.Anything, topicTagName+topic)
	}

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.ProducerStats(stats)
}

func TestInstrumentation_ProducerDuration(t *testing.T) {
	topic := "test"

	logger, _ := zap.NewDevelopment()

	mockLogger := &mockLogger{}
	mockLogger.On("With", mock.Anything).Return(logger)

	mockStats := &mockStats{}
	mockStats.On("Duration", statsKeyProducerDuration, mock.Anything, topicTagName+topic)

	instrumentation := New(topic, mockLogger, mockStats)
	instrumentation.ProducerDuration(time.Now())
}
