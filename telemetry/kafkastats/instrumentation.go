package kafkastats

import (
	"fmt"
	"time"

	kafkago "github.com/segmentio/kafka-go"
	"go.uber.org/zap"
)

const (
	logTagTopic = "topic"

	statsKeyPrefix = "rhz"

	statsKeyKafkaError = statsKeyPrefix + "kafka.error"

	statsKeyConsumerPrefix     = statsKeyPrefix + "kafka.consumer."
	statsKeyConsumerDials      = statsKeyConsumerPrefix + "dials"
	statsKeyConsumerFetches    = statsKeyConsumerPrefix + "fetches"
	statsKeyConsumerMessages   = statsKeyConsumerPrefix + "messages"
	statsKeyConsumerBytes      = statsKeyConsumerPrefix + "bytes"
	statsKeyConsumerRebalances = statsKeyConsumerPrefix + "rebalances"
	statsKeyConsumerTimeouts   = statsKeyConsumerPrefix + "timeouts"
	statsKeyConsumerErrors     = statsKeyConsumerPrefix + "errors"
	statsKeyConsumerDialTime   = statsKeyConsumerPrefix + "dial_time.max"
	statsKeyConsumerReadTime   = statsKeyConsumerPrefix + "read_time.max"
	statsKeyConsumerFetchSize  = statsKeyConsumerPrefix + "fetch_size.max"
	statsKeyConsumerFetchBytes = statsKeyConsumerPrefix + "fetch_bytes.max"
	statsKeyConsumerOffset     = statsKeyConsumerPrefix + "offset"
	statsKeyConsumerLag        = statsKeyConsumerPrefix + "lag"

	statsKeyConsumerCommitFailed = statsKeyConsumerPrefix + "commit_failed"
	statsKeyConsumerEvent        = statsKeyConsumerPrefix + "event"

	statsKeyProducerPrefix = statsKeyPrefix + "kafka.producer."

	statsKeyProducerWrites   = statsKeyProducerPrefix + "writes"
	statsKeyProducerMessages = statsKeyProducerPrefix + "messages"
	statsKeyProducerBytes    = statsKeyProducerPrefix + "bytes"
	statsKeyProducerErrors   = statsKeyProducerPrefix + "errors"

	statsKeyProducerBatchTime  = statsKeyProducerPrefix + "batch_time.max"
	statsKeyProducerWriteTime  = statsKeyProducerPrefix + "write_time.max"
	statsKeyProducerWaitTime   = statsKeyProducerPrefix + "wait_time.max"
	statsKeyProducerRetries    = statsKeyProducerPrefix + "retries"
	statsKeyProducerBatchSize  = statsKeyProducerPrefix + "batch_size"
	statsKeyProducerBatchBytes = statsKeyProducerPrefix + "batch_bytes"

	statsKeyProducerDuration = statsKeyProducerPrefix + "duration"

	statsKeyProducerEvent = statsKeyProducerPrefix + "event"

	topicTagName = "topic:"
	tagType      = "type:"
)

// New initializes and returns a default implementation of kafka.Instrumentation
func New(topic string, logger logger, stats stats) *Instrumentation {
	i := &Instrumentation{
		topic:  topic,
		logger: logger,
		stats:  stats,
	}

	i.topicTagName = topicTagName + topic
	i.logger = i.logger.With(zap.String(logTagTopic, topic))

	return i
}

// Instrumentation is a default implementation of commons/go/kafka instrumentation.
type Instrumentation struct {
	topic string

	logger logger
	stats  stats

	topicTagName string
}

// ConsumerStats implements kafka.Instrumentation
func (i *Instrumentation) ConsumerStats(stats *kafkago.ReaderStats) {
	i.stats.Count(statsKeyConsumerDials, int(stats.Dials), i.topicTagName)
	i.stats.Count(statsKeyConsumerFetches, int(stats.Fetches), i.topicTagName)
	i.stats.Count(statsKeyConsumerMessages, int(stats.Messages), i.topicTagName)
	i.stats.Count(statsKeyConsumerBytes, int(stats.Bytes), i.topicTagName)
	i.stats.Count(statsKeyConsumerRebalances, int(stats.Rebalances), i.topicTagName)
	i.stats.Count(statsKeyConsumerTimeouts, int(stats.Timeouts), i.topicTagName)
	i.stats.Count(statsKeyConsumerErrors, int(stats.Errors), i.topicTagName)

	i.stats.Gauge(statsKeyConsumerDialTime, stats.DialTime.Max, i.topicTagName)
	i.stats.Gauge(statsKeyConsumerReadTime, stats.ReadTime.Max, i.topicTagName)
	i.stats.Gauge(statsKeyConsumerFetchSize, stats.FetchSize.Max, i.topicTagName)
	i.stats.Gauge(statsKeyConsumerFetchBytes, stats.FetchBytes.Max, i.topicTagName)

	i.stats.Gauge(statsKeyConsumerOffset, stats.Offset, i.topicTagName)
	i.stats.Gauge(statsKeyConsumerLag, stats.Lag, i.topicTagName)
}

// ConsumerCommitFailed implements kafka.Instrumentation
func (i *Instrumentation) ConsumerCommitFailed(err error) {
	i.stats.Count1(statsKeyConsumerCommitFailed, i.topicTagName)

	i.logger.Error("Consumer commit failed", zap.Error(err))
}

// InitializedConsumer implements kafka.Instrumentation
func (i *Instrumentation) InitializedConsumer() {
	i.stats.Count1(statsKeyConsumerEvent, i.topicTagName, tagType+":init")

	i.logger.Info("Consumer initialized")
}

// ClosedConsumer implements kafka.Instrumentation
func (i *Instrumentation) ClosedConsumer() {
	i.stats.Count1(statsKeyConsumerEvent, i.topicTagName, tagType+":close")

	i.logger.Info("Closed kafka consumer")
}

// StartConsumerReadLoop implements kafka.Instrumentation
func (i *Instrumentation) StartConsumerReadLoop() {
	i.stats.Count1(statsKeyConsumerEvent, i.topicTagName, tagType+":start_loop")

	i.logger.Info("Started kafka consumer read loop")
}

// StopConsumerReadLoop implements kafka.Instrumentation
func (i *Instrumentation) StopConsumerReadLoop() {
	i.stats.Count1(statsKeyConsumerEvent, i.topicTagName, tagType+":stop_loop")

	i.logger.Info("Stopped kafka consumer read loop")
}

// ReadConsumerError implements kafka.Instrumentation
func (i *Instrumentation) ReadConsumerError(err error) {
	i.logger.Error("Consumer read error", zap.Error(err))
}

// InitializedProducer implements kafka.Instrumentation
func (i *Instrumentation) InitializedProducer() {
	i.stats.Count1(statsKeyProducerEvent, i.topicTagName, tagType+":init")

	i.logger.Info("Producer initialized")
}

// ProducerStats implements kafka.Instrumentation
func (i *Instrumentation) ProducerStats(stats *kafkago.WriterStats) {
	i.stats.Count(statsKeyProducerWrites, int(stats.Writes), i.topicTagName)
	i.stats.Count(statsKeyProducerMessages, int(stats.Messages), i.topicTagName)
	i.stats.Count(statsKeyProducerBytes, int(stats.Bytes), i.topicTagName)
	i.stats.Count(statsKeyProducerErrors, int(stats.Errors), i.topicTagName)

	i.stats.Gauge(statsKeyProducerBatchTime, stats.BatchTime.Max, i.topicTagName)
	i.stats.Gauge(statsKeyProducerWriteTime, stats.WriteTime.Max, i.topicTagName)
	i.stats.Gauge(statsKeyProducerWaitTime, stats.WaitTime.Max, i.topicTagName)
	i.stats.Gauge(statsKeyProducerRetries, stats.Retries, i.topicTagName)
	i.stats.Gauge(statsKeyProducerBatchSize, stats.BatchSize.Max, i.topicTagName)
	i.stats.Gauge(statsKeyProducerBatchBytes, stats.BatchBytes.Max, i.topicTagName)
}

// ProducerDuration implements kafka.Instrumentation
func (i *Instrumentation) ProducerDuration(start time.Time) {
	i.stats.Duration(statsKeyProducerDuration, start, i.topicTagName)
}

// ReadConsumerError implements kafka.Instrumentation
func (i *Instrumentation) Printf(msg string, args ...interface{}) {
	i.stats.Count1(statsKeyKafkaError, i.topicTagName)

	i.logger.Error("Kafka Error", zap.Error(fmt.Errorf(msg, args...)))
}

//go:generate mockery --name=logger --case=underscore --testonly --inpackage
type logger interface {
	Warn(message string, fields ...zap.Field)
	Error(message string, fields ...zap.Field)
	Info(message string, fields ...zap.Field)
	With(fields ...zap.Field) *zap.Logger
}

//go:generate mockery --name=stats --case=underscore --testonly --inpackage
type stats interface {
	Duration(key string, start time.Time, tags ...string)
	Count1(key string, tags ...string)
	Count(key string, amount int, tags ...string)
	Gauge(key string, value interface{}, tags ...string)
}
