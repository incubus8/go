// Code generated by mockery v2.18.0. DO NOT EDIT.

package istats

import mock "github.com/stretchr/testify/mock"

// MockLogger is an autogenerated mock type for the Logger type
type MockLogger struct {
	mock.Mock
}

// ConfigError provides a mock function with given fields: message, args
func (_m *MockLogger) ConfigError(message string, args ...interface{}) {
	var _ca []interface{}
	_ca = append(_ca, message)
	_ca = append(_ca, args...)
	_m.Called(_ca...)
}

// StatsError provides a mock function with given fields: message, args
func (_m *MockLogger) StatsError(message string, args ...interface{}) {
	var _ca []interface{}
	_ca = append(_ca, message)
	_ca = append(_ca, args...)
	_m.Called(_ca...)
}

type mockConstructorTestingTNewMockLogger interface {
	mock.TestingT
	Cleanup(func())
}

// NewMockLogger creates a new instance of MockLogger. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMockLogger(t mockConstructorTestingTNewMockLogger) *MockLogger {
	mock := &MockLogger{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
