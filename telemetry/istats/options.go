package istats

import (
	"strings"
)

// Option allows for configuration of the Client
type Option func(client *Client)

// Address allows the user to define a custom location for the statsD agent
// Default: 0.0.0.0:8125
func Address(address string) Option {
	return func(client *Client) {
		if address == "" {
			client.logger.ConfigError("supplied address was empty")
			return
		}

		client.address = address
	}
}

// NOOP disables all metrics into a NOOP
func NOOP() Option {
	return func(client *Client) {
		client.noop = true
	}
}

// Log allows the user to inject a logger into the client
// Default: do not log
func Log(logger Logger) Option {
	return func(client *Client) {
		client.logger = logger
	}
}

// BaseSampleRate allows the user to set a default sample rate for all metrics not specifically configured via `SampleRates()`
// This is the rate used for all keys that are not specified in the "SampleRates" config
// Optional.  Default: 100 (aka 100%)
func BaseSampleRate(rate float64) Option {
	return func(client *Client) {
		if rate > 0.0 && rate < 100.0 {
			client.baseSampleRate = rate / 100.0
		} else {
			client.logger.ConfigError("invalid sample rate %f", rate)
		}
	}
}

// SampleRates allows the user to set sample rates
// Optional.  Default: 100 (aka 100%)
//
// Note: The map key should be in the format: "[key]"
func SampleRates(rates map[string]float64) Option {
	return func(client *Client) {
		savedRates := make(map[string]float64, len(rates))

		for thisKey, thisRate := range rates {
			if thisRate > 0.0 && thisRate < 100.0 {
				savedRates[thisKey] = thisRate / 100.0
				continue
			}

			client.logger.ConfigError("invalid sample rate for key %s - %f", thisKey, thisRate)
		}

		client.sampleRates = savedRates
	}
}

// Namespace allows the user to specify a custom namespace to prefix all metrics with.
// Optional. Default: "incubus8."
// Note: namespaces should end in a period "."; if one is not supplied it will be added automatically
func Namespace(namespace string) Option {
	return func(client *Client) {
		client.namespace = namespace
		if !strings.HasSuffix(namespace, ".") {
			client.namespace += "."
		}
	}
}

// Tags defines a set of tags that are added to all emitted metrics.
// Optional. Note: DD agents can also be configured to automatically add tags
func Tags(tags ...string) Option {
	return func(client *Client) {
		client.baseTags = tags
	}
}
