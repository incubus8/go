package istats

import (
	"fmt"
	"os"
	"time"
)

const (
	defaultBaseSampleRate = float64(1.0)
	defaultNamespace      = "incubus8."
	defaultAddress        = "0.0.0.0:8125"
)

// New initializes and configures the stats client
func New(options ...Option) *Client {
	client := &Client{
		logger:         &noopLogger{},
		baseSampleRate: defaultBaseSampleRate,
		sampleRates:    map[string]float64{},
		namespace:      defaultNamespace,
		address:        defaultAddress,
		baseTags:       []string{},
		forwarder:      &noopForwarder{},
	}

	for _, option := range options {
		option(client)
	}

	client.applyDefaultTags()

	if !client.noop {
		ddClient, err := newDDForwarder(client.address, client.namespace, client.baseTags)
		if err == nil {
			client.forwarder = ddClient
		} else {
			fmt.Fprintf(os.Stderr, "failed to init DataDog client with err %s", err)
		}
	}

	return client
}

// Client is the Stats client
// There is no need to stub or mock this client during testing unless you wish to validate that your code emitted a certain metric.
// In those cases, please use a custom local interface to mock this client (see examples for more details)
type Client struct {
	// underlying StatsD library that creates and emits the metrics
	forwarder forwarder

	// logger for client events and errors
	logger Logger

	// sample rate for all metrics that are not explicitly defined in the sampleRates map
	baseSampleRate float64

	// special sample rates
	sampleRates map[string]float64

	// namespace (prefix) for all emitted stats
	namespace string

	// address of the StatsD collector
	address string

	// Tags added to all emitted metrics
	baseTags []string

	// flag that turns the forwarder into a NOOP
	noop bool
}

// Duration tracks the time something took from start until now
func (c *Client) Duration(key string, start time.Time, tags ...string) {
	duration := time.Since(start)

	err := c.forwarder.Histogram(key, float64(duration.Nanoseconds()), tags, c.getRate(key))
	if err != nil {
		c.logger.StatsError("[key: %s] call to Histogram() failed with error %w", key, err)
	}
}

// Gauge allows you to track a value over time
// NOTE: value should be one of int, int32, int64, float32, float64.  All other types will record 0
func (c *Client) Gauge(key string, value interface{}, tags ...string) {
	trueValue := getTrueValue(value)

	err := c.forwarder.Gauge(key, trueValue, tags, c.getRate(key))
	if err != nil {
		c.logger.StatsError("[key: %s] call to Gauge() failed with error %w", key, err)
	}
}

// Histogram produces a statistical distribution of a set of values
// NOTE: value should be one of int, int32, int64, float32, float64.  All other types will record 0
func (c *Client) Histogram(key string, value interface{}, tags ...string) {
	trueValue := getTrueValue(value)

	err := c.forwarder.Histogram(key, trueValue, tags, c.getRate(key))
	if err != nil {
		c.logger.StatsError("[key: %s] call to Histogram() failed with error %w", key, err)
	}
}

// Count1 track the occurrence of something (this is equivalent to Count(key, 1, tags...)
func (c *Client) Count1(key string, tags ...string) {
	c.Count(key, 1, tags...)
}

// Count will increase or decrease the value of something over time
func (c *Client) Count(key string, amount int, tags ...string) {
	err := c.forwarder.Count(key, int64(amount), tags, c.getRate(key))
	if err != nil {
		c.logger.StatsError("[key: %s] call to Count() failed with error %w", key, err)
	}
}

// Set can be used to count the number of unique occurrences between flushes
// When a metric is sent with a specific value, this is counted as an occurrence.
func (c *Client) Set(key, value string, tags ...string) {
	err := c.forwarder.Set(key, value, tags, c.getRate(key))
	if err != nil {
		c.logger.StatsError("[key: %s] call to Set() failed with error %w", key, err)
	}
}

// Close the client, flush any pending metrics, and stop receiving any new metrics.
// This should be called just before existing your application to avoid losing any metrics.
func (c *Client) Close() error {
	return c.forwarder.Close()
}

func (c *Client) getRate(key string) float64 {
	customRate, exists := c.sampleRates[key]
	if !exists {
		return c.baseSampleRate
	}

	return customRate
}

func (c *Client) applyDefaultTags() {
	c.baseTags = append(
		c.baseTags,
		"app:"+os.Getenv("app"),
		"team:"+os.Getenv("team"),
		"image:"+os.Getenv("image"),
		"commit:"+os.Getenv("commit"),
		"build_time:"+os.Getenv("build_time"),
		"release:"+os.Getenv("release"),
	)
}

func getTrueValue(value interface{}) float64 {
	switch converted := value.(type) {
	case int32:
		return float64(converted)

	case int64:
		return float64(converted)

	case int:
		return float64(converted)

	case float32:
		return float64(converted)

	case float64:
		return converted

	default:
		return 0.0
	}
}
