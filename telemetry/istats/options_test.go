package istats

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestAddress(t *testing.T) {
	mockLogger := &MockLogger{}

	client := New(
		Log(mockLogger),
		Address("A:B"))

	assert.Equal(t, "A:B", client.address)
	assert.True(t, mockLogger.AssertExpectations(t))
}

func TestAddress_error(t *testing.T) {
	mockLogger := &MockLogger{}
	mockLogger.On("ConfigError", mock.Anything)

	client := New(
		Log(mockLogger),
		Address(""))

	assert.Equal(t, defaultAddress, client.address)
	assert.True(t, mockLogger.AssertExpectations(t))
}

func TestNOOP(t *testing.T) {
	mockLogger := &MockLogger{}

	client := New(
		Log(mockLogger),
		NOOP())

	assert.True(t, client.noop)
	assert.IsType(t, &noopForwarder{}, client.forwarder)
}

func TestBaseSampleRate(t *testing.T) {
	mockLogger := &MockLogger{}

	client := New(
		Log(mockLogger),
		BaseSampleRate(66.6))

	assert.InDelta(t, 0.666, client.baseSampleRate, 0.01)
}

func TestBaseSampleRate_error(t *testing.T) {
	mockLogger := &MockLogger{}
	mockLogger.On("ConfigError", mock.Anything, mock.Anything)

	client := New(
		Log(mockLogger),
		BaseSampleRate(200.0))

	assert.Equal(t, defaultBaseSampleRate, client.baseSampleRate)
	assert.True(t, mockLogger.AssertExpectations(t))
}

func TestBaseSampleRates(t *testing.T) {
	mockLogger := &MockLogger{}

	rates := map[string]float64{
		"foo": 12.34,
	}

	client := New(
		Log(mockLogger),
		SampleRates(rates))

	assert.Equal(t, 1, len(client.sampleRates))
	assert.InDelta(t, 0.1234, client.sampleRates["foo"], 0.01)
}

func TestBaseSampleRates_error(t *testing.T) {
	mockLogger := &MockLogger{}
	mockLogger.On("ConfigError", mock.Anything, mock.Anything, mock.Anything)

	rates := map[string]float64{
		"foo": 234.56,
	}

	client := New(
		Log(mockLogger),
		SampleRates(rates))

	assert.Equal(t, 0, len(client.sampleRates))
	assert.True(t, mockLogger.AssertExpectations(t))
}

func TestNamespace(t *testing.T) {
	mockLogger := &MockLogger{}

	client := New(
		Log(mockLogger),
		Namespace("foo"))

	assert.Equal(t, "foo.", client.namespace)
	assert.True(t, mockLogger.AssertExpectations(t))
}

func TestTags(t *testing.T) {
	// default tags
	os.Setenv("app", "A")
	os.Setenv("team", "B")
	os.Setenv("image", "C")
	os.Setenv("commit", "D")
	os.Setenv("build_time", "E")
	os.Setenv("release", "stable")

	defaultTags := []string{"app:A", "team:B", "image:C", "commit:D", "build_time:E", "release:stable"}

	mockLogger := &MockLogger{}

	testTags := []string{"foo:bar", "version:1.2.3"}

	client := New(
		Log(mockLogger),
		Tags(testTags...))

	assert.Equal(t, append(testTags, defaultTags...), client.baseTags)
	assert.True(t, mockLogger.AssertExpectations(t))
}
