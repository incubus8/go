package zapstats

import (
	"testing"

	"go.uber.org/zap"
)

func TestNew(t *testing.T) {
	zapLogger, _ := zap.NewDevelopment()

	myLogger := New(zapLogger)
	myLogger.StatsError("foo %s", "bar")
	myLogger.ConfigError("foo %s", "bar")
}
