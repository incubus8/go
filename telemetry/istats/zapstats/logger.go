package zapstats

import (
	"fmt"

	"go.uber.org/zap"
)

// New returns a zap based implementation of istats.Logger
func New(logger *zap.Logger) *Logger {
	return &Logger{
		logger: logger,
	}
}

// Logger provides an implementation of istats.Logger that uses Zap
type Logger struct {
	logger *zap.Logger
}

// ConfigError implements istats.Logger
func (l *Logger) ConfigError(message string, args ...interface{}) {
	l.logger.Error(fmt.Sprintf(message, args...))
}

// StatsError implements istats.Logger
func (l *Logger) StatsError(message string, args ...interface{}) {
	l.logger.Error(fmt.Sprintf(message, args...))
}
