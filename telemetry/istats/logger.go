package istats

// Logger for StatsD
//
//go:generate mockery --name=Logger --case underscore --testonly --inpackage
type Logger interface {
	// ConfigError reports errors during configuration
	ConfigError(message string, args ...interface{})

	// StatsError reports errors while attempting to emit the stats to the network
	StatsError(message string, args ...interface{})
}

type noopLogger struct{}

func (n *noopLogger) StatsError(message string, args ...interface{}) {
	// intentionally blank
}

func (n *noopLogger) ConfigError(message string, args ...interface{}) {
	// intentionally blank
}
