package istats

import (
	dogstatsd "github.com/DataDog/datadog-go/statsd"
)

// forwarder provides an abstraction of the underlying DataDog stats library (allowing for nooping)
//
//go:generate mockery --name=forwarder --case=underscore --testonly --inpackage
type forwarder interface {
	// Call the corresponding method on the DataDog StatsD client
	Gauge(key string, value float64, tags []string, rate float64) error

	// Call the corresponding method on the DataDog StatsD client
	Count(key string, value int64, tags []string, rate float64) error

	// Call the corresponding method on the DataDog StatsD client
	Histogram(key string, value float64, tags []string, rate float64) error

	// Call the corresponding method on the DataDog StatsD client
	Set(key, value string, tags []string, rate float64) error

	// Close the client, flush any pending metrics, and stop receiving any new metrics.
	Close() error
}

type noopForwarder struct{}

// Gauge implements forwarder
func (n *noopForwarder) Gauge(key string, value float64, tags []string, rate float64) error {
	// intentionally blank
	return nil
}

// Count implements forwarder
func (n *noopForwarder) Count(key string, value int64, tags []string, rate float64) error {
	// intentionally blank
	return nil
}

// Histogram implements forwarder
func (n *noopForwarder) Histogram(key string, value float64, tags []string, rate float64) error {
	// intentionally blank
	return nil
}

// Set implements forwarder
func (n *noopForwarder) Set(key, value string, tags []string, rate float64) error {
	// intentionally blank
	return nil
}

// Close implements forwarder
func (n *noopForwarder) Close() error {
	// intentionally blank
	return nil
}

func newDDForwarder(address, namespace string, baseTags []string) (*ddForwarder, error) {
	options := []dogstatsd.Option{
		dogstatsd.WithNamespace(namespace),
	}

	if len(baseTags) > 0 {
		options = append(options, dogstatsd.WithTags(baseTags))
	}

	client, err := dogstatsd.New(address, options...)
	if err != nil {
		return nil, err
	}

	return &ddForwarder{
		client: client,
	}, nil
}

type ddForwarder struct {
	client *dogstatsd.Client
}

// Gauge implements forwarder
func (d *ddForwarder) Gauge(key string, value float64, tags []string, rate float64) error {
	return d.client.Gauge(key, value, tags, rate)
}

// Count implements forwarder
func (d *ddForwarder) Count(key string, value int64, tags []string, rate float64) error {
	return d.client.Count(key, value, tags, rate)
}

// Histogram implements forwarder
func (d *ddForwarder) Histogram(key string, value float64, tags []string, rate float64) error {
	return d.client.Histogram(key, value, tags, rate)
}

// Set implements forwarder
func (d *ddForwarder) Set(key, value string, tags []string, rate float64) error {
	return d.client.Set(key, value, tags, rate)
}

// Close implements forwarder
func (d *ddForwarder) Close() error {
	return d.client.Close()
}
