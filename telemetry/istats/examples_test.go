package istats_test

import (
	"fmt"

	"go.uber.org/zap"

	"gitlab.com/incubus8/go/telemetry/istats"
	"gitlab.com/incubus8/go/telemetry/istats/zapstats"
)

// Please request/suggest usage examples in the #commons-telemetry channel on Slack

func Example_usingZap() {
	logger, _ := zap.NewDevelopment()

	statsClient := istats.New(istats.Log(zapstats.New(logger)))

	// debug (to keep the compiler happy)
	fmt.Printf("client: %v", statsClient)
}

func Example_simple() {
	// by default using `noopForwarder`
	statsClient := istats.New()

	// If you are new to StatsD, you can read https://sysdig.com/blog/monitoring-statsd-metrics/

	// For gauge
	statsClient.Gauge("incubus8.user.balance", 1000)

	// For custom count value
	statsClient.Count("incubus8.user.new", 1000)

	// For adding count to 1 value
	statsClient.Count1("incubus8.user.new")

	// For tracking request histogram
	statsClient.Histogram("incubus8.http_response", 200)

	// For tracking unique users by id
	statsClient.Set("incubus8.user_id", "111011")
}
