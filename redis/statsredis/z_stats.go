package statsredis

import (
	"time"
)

func New(stats Client) *StatsD {
	return &StatsD{
		stats: stats,
	}
}

type StatsD struct {
	stats Client
}

func (s *StatsD) Duration(key string, start time.Time, tags ...string) {
	s.stats.Duration(key, start, tags...)
}

func (s *StatsD) Gauge(key string, value interface{}, tags ...string) {
	s.stats.Gauge(key, value, tags...)
}

func (s *StatsD) Histogram(key string, value interface{}, tags ...string) {
	s.stats.Histogram(key, value, tags...)
}

func (s *StatsD) Count1(key string, tags ...string) {
	s.stats.Count1(key, tags...)
}

func (s *StatsD) Count(key string, amount int, tags ...string) {
	s.stats.Count(key, amount, tags...)
}

type Client interface {
	Duration(key string, start time.Time, tags ...string)
	Gauge(key string, value interface{}, tags ...string)
	Histogram(key string, value interface{}, tags ...string) 
	Count1(key string, tags ...string) 
	Count(key string, amount int, tags ...string)
}