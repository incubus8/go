package redis

import (
	"context"
	"crypto/sha1"
	"encoding/hex"
	"io"
	"strings"

	"github.com/gomodule/redigo/redis"
)

// Script encapsulates the source, hash and key count for a Lua script. See
// http://redis.io/commands/eval for information on scripts in Redis.
type Script struct {
	keyCount int
	src      string
	hash     string
}

// NewScript returns a new script object. If keyCount is greater than or equal
// to zero, then the count is automatically inserted in the EVAL command
// argument list. If keyCount is less than zero, then the application supplies
// the count as the first value in the keysAndArgs argument to the Do, Send and
// SendHash methods.
func NewScript(keyCount int, src string) *Script {
	h := sha1.New()
	_, _ = io.WriteString(h, src)

	return &Script{keyCount, src, hex.EncodeToString(h.Sum(nil))}
}

func (s *Script) args(spec string, keysAndArgs []interface{}) []interface{} {
	var args []interface{}
	if s.keyCount < 0 {
		args = make([]interface{}, 1+len(keysAndArgs))
		args[0] = spec
		copy(args[1:], keysAndArgs)
	} else {
		args = make([]interface{}, 2+len(keysAndArgs))
		args[0] = spec
		args[1] = s.keyCount
		copy(args[2:], keysAndArgs)
	}

	return args
}

// Do evaluates the script. Under the covers, Do optimistically evaluates the
// script using the EVALSHA command. If the command fails because the script is
// not loaded, then Do evaluates the script using the EVAL command (thus
// causing the script to load).
func (s *Script) doWithContext(ctx context.Context, c Conn, keysAndArgs ...interface{}) (interface{}, error) {
	v, err := c.Do(ctx, "EVALSHA", s.args(s.hash, keysAndArgs)...)
	if e, ok := err.(redis.Error); ok && strings.HasPrefix(string(e), "NOSCRIPT ") {
		v, err = c.Do(ctx, "EVAL", s.args(s.src, keysAndArgs)...)
	}

	return v, err
}

func (c *Client) DoWithScript(ctx context.Context, script *Script, keysAndArgs ...interface{}) (interface{}, error) {
	return script.doWithContext(ctx, c, keysAndArgs...)
}

//go:generate mockery --name=Conn --case=underscore --inpackage --testonly
type Conn interface {
	Do(ctx context.Context, cmdName string, args ...interface{}) (interface{}, error)
}
