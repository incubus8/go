package redis

import (
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
)

const (
	attemptsPrefix          = "hystrix.attempts"
	errorsPrefix            = "hystrix.errors"
	successesPrefix         = "hystrix.successes"
	failuresPrefix          = "hystrix.failures"
	rejectsPrefix           = "hystrix.rejects"
	shortCircuitsPrefix     = "hystrix.shortCircuits"
	timeoutsPrefix          = "hystrix.timeouts"
	fallbackSuccessesPrefix = "hystrix.fallbackSuccesses"
	fallbackFailuresPrefix  = "hystrix.fallbackFailures"
	canceledPrefix          = "hystrix.canceled"
	deadlinePrefix          = "hystrix.deadline"
	totalDurationPrefix     = "hystrix.totalDuration"
	runDurationPrefix       = "hystrix.runDuration"
	concurrencyInUsePrefix  = "hystrix.attempts"
)

func newHystrixStatsWrapper(name string, ddClient StatsD) *hystrixStatsWrapper {
	return &hystrixStatsWrapper{
		ddClient: ddClient,
		tag:      "circuit:" + name,
	}
}

type hystrixStatsWrapper struct {
	ddClient StatsD

	tag string
}

//nolint:gocritic
func (h *hystrixStatsWrapper) Update(result metricCollector.MetricResult) {
	h.ddClient.Count(attemptsPrefix, int(result.Attempts), h.tag)
	h.ddClient.Count(errorsPrefix, int(result.Errors), h.tag)
	h.ddClient.Count(successesPrefix, int(result.Successes), h.tag)
	h.ddClient.Count(failuresPrefix, int(result.Failures), h.tag)
	h.ddClient.Count(rejectsPrefix, int(result.Rejects), h.tag)
	h.ddClient.Count(shortCircuitsPrefix, int(result.ShortCircuits), h.tag)
	h.ddClient.Count(timeoutsPrefix, int(result.Timeouts), h.tag)
	h.ddClient.Count(fallbackSuccessesPrefix, int(result.FallbackSuccesses), h.tag)
	h.ddClient.Count(fallbackFailuresPrefix, int(result.FallbackFailures), h.tag)
	h.ddClient.Count(canceledPrefix, int(result.ContextCanceled), h.tag)
	h.ddClient.Count(deadlinePrefix, int(result.ContextDeadlineExceeded), h.tag)
	h.ddClient.Gauge(totalDurationPrefix, result.TotalDuration, h.tag)
	h.ddClient.Gauge(runDurationPrefix, result.RunDuration, h.tag)
	h.ddClient.Gauge(concurrencyInUsePrefix, int64(100*result.ConcurrencyInUse), h.tag)
}

func (h *hystrixStatsWrapper) Reset() {
	// do nothing
}
