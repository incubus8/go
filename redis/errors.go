package redis

import (
	"errors"
)

// error constants
var (
	ErrUnexpectedSubsResponse     = errors.New("unexpected: incorrect response type for subscribe action")
	ErrArgNotStringCmd            = errors.New("first argument cannot be converted to a command string")
	ErrOutputChanNotReady         = errors.New("unexpected internal error: did not receive reply from output channel")
	ErrConnClosedByCircuitBreaker = errors.New("connection closed by circuitbreaker error")

	ErrPipeline = errors.New("pipeline error")
)
