package redis

import (
	"time"

	"github.com/afex/hystrix-go/hystrix"

	"gitlab.com/incubus8/go/redis/internal/connman"
)

// ClientOption is a functional parameter used to configure the Client
// For some options, default value is applied to a config when no Option related to that config is supplied or invalid Option is supplied
type ClientOption func(client *Client)

// WithStatsD specifies the WithStatsD client
func WithStatsD(ddClient StatsD) ClientOption {
	return func(client *Client) {
		client.stats = ddClient
		client.cmOptions = append(client.cmOptions, connman.WithStatsD(ddClient))
	}
}

// WithHystrixStatsD enables instrumentation of the hystrix metrics
func WithHystrixStatsD(ddClient StatsD) ClientOption {
	return func(client *Client) {
		client.cbMetrics = ddClient
	}
}

// WithLogger specifies the logger
func WithLogger(logger Logger) ClientOption {
	return func(client *Client) {
		client.logger = logger
		client.cmOptions = append(client.cmOptions, connman.WithLogger(logger))
	}
}

// Name specifies client name
func Name(name string) ClientOption {
	return func(client *Client) {
		client.name = name
	}
}

func CircuitBreaker(config *hystrix.CommandConfig) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.CircuitBreaker(config))
	}
}

// Master specifies host port configuration write operation in a master slave
// redis setup, master is the redis master host (this address always points to
// master node, dns will resolve to new master in case of failover)
func Master(master string) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.Master(master))
	}
}

// ReadSlaves specifies host port configuration for read operation in a
// master slave redis setup, hostsForRead contains all host address including
// master(you cannot tell which is master from the addr used here because of
// failover)
func ReadSlaves(hostsForRead ...string) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.ReadSlaves(hostsForRead...))
	}
}

// DB specifies the redis DB to use.
// default: 0
func DB(db int) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.DB(db))
	}
}

// MaxActive sets the Maximum number of connections allocated by the redis connection pool at a given time.
// Defaults to defaultMaxActive
func MaxActive(maxActive int) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.MaxIdle(maxActive))
	}
}

// MaxIdle sets Maximum number of idle connections in the pool.
// Defaults to defaultMaxIdle
// clients which have large load spike may need higher MaxIdle,
// Too small MaxIdle might lead to inefficient use of the connection pool
// Too large MaxIdle might lead to high load of redis server or `ERR max number of clients reached`
func MaxIdle(maxIdle int) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.MaxIdle(maxIdle))
	}
}

// ConnectTimeout specifies the timeout for connecting to the Redis server.
// Defaults to defaultConnectTimeout
func ConnectTimeout(timeout time.Duration) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.ConnectTimeout(timeout))
	}
}

// ReadTimeout specifies the timeout for reading a single command reply.
// Defaults to defaultReadTimeout
func ReadTimeout(timeout time.Duration) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.ConnectTimeout(timeout))
	}
}

// WriteTimeout specifies the timeout for writing a single command.
// Defaults to defaultWriteTimeout
func WriteTimeout(timeout time.Duration) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.WriteTimeout(timeout))
	}
}

// IdleTimeout specifies the IdleTimeout of the connections. Close
// connections after remaining idle for this duration. If the value is zero,
// then idle connections are not closed. Applications should set the timeout
// to a value less than the server's timeout.
// Defaults to defaultIdleTimeout
func IdleTimeout(timeout time.Duration) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.IdleTimeout(timeout))
	}
}

// ReadOnlyFromSlaves specifies that the read operation is only performed on slaves.
// This could help reduce the load on master.
// By default client reads from both master and slaves.
//
// In the event that all slaves are down, all reads will be performed on the master (assuming it is alive) regardless
// of this setting.
func ReadOnlyFromSlaves() ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.ReadOnlyFromSlaves())
	}
}

// WithPassword specifies the password to use when connecting to
// the Redis server.
func WithPassword(password string) ClientOption {
	return func(client *Client) {
		client.cmOptions = append(client.cmOptions, connman.WithPassword(password))
	}
}
