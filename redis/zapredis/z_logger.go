package zapredis

import (
	"go.uber.org/zap"
)

func New(logger *zap.Logger) *Logger {
	return &Logger{
		logger: logger,
	}
}

type Logger struct {
	logger *zap.Logger
}

func (l *Logger) PingFailed(err error) {
	l.logger.Error("redis ping failed", zap.Error(err))
}

func (l *Logger) DialFailed(err error, config interface{}) {
	l.logger.Error("redis dial failed", zap.Error(err), zap.Any("config", config))
}

func (l *Logger) NoReadHosts() {
	l.logger.Warn("no hosts for reading supplied, using master")
}

func (l *Logger) CloseError(err error) {
	l.logger.Warn("failed to close redis connection", zap.Error(err))
}

func (l *Logger) PoolCloseError(errorType string, err error) {
	l.logger.Warn("pool failed to shutdown", zap.String("errorType", errorType), zap.Error(err))
}

func (l *Logger) QueryError(circuitName string, err error) {
	l.logger.Warn("query error", zap.String("circuit", circuitName), zap.Error(err))
}

func (l *Logger) CriticalError(circuitName string, err error) {
	l.logger.Warn("critical error", zap.String("circuit", circuitName), zap.Error(err))
}
