package redis

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClientImpl_Pipeline(t *testing.T) {
	scenarios := []struct {
		desc         string
		getMockConn  func() *mockConnection
		mockConMan   func(conn *mockConnection) *mockConnectionManager
		do           func(client *Client) ([]ReplyPair, error)
		expectResult []ReplyPair
		expectError  error
	}{
		{
			desc: "Pipeline",
			getMockConn: func() *mockConnection {
				conn := getMockConnection()
				conn.On("Name").Return("test")
				conn.On("Send", "SET", "key", "value").Return(nil)
				conn.On("Send", "GET", "key").Return(nil)
				conn.On("Flush").Return(nil)
				conn.On("Receive").Return("OK", nil).Once()
				conn.On("Receive").Return("value", nil).Once()
				return conn
			},
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) ([]ReplyPair, error) {
				args := [][]interface{}{
					{"SET", "key", "value"},
					{"GET", "key"},
				}

				reply, err := client.Pipeline(context.TODO(), args)

				return reply, err
			},
			expectResult: []ReplyPair{
				{Value: "OK", Err: nil},
				{Value: "value", Err: nil},
			},
		},
		{
			desc: "Pipeline flush error",
			getMockConn: func() *mockConnection {
				conn := getMockConnection()
				conn.On("Name").Return("test")
				conn.On("Send", "SET", "key", "value").Return(nil)
				conn.On("Send", "GET", "key").Return(nil)
				conn.On("Flush").Return(errors.New("flush failed"))

				return conn
			},
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) ([]ReplyPair, error) {
				args := [][]interface{}{
					{"SET", "key", "value"},
					{"GET", "key"},
				}

				reply, err := client.Pipeline(context.TODO(), args)

				return reply, err
			},
			expectError: ErrPipeline,
		},
		{
			desc: "Pipeline send error",
			getMockConn: func() *mockConnection {
				conn := getMockConnection()
				conn.On("Name").Return("test")
				conn.On("Send", "SET", "key", "value").Return(nil)
				conn.On("Send", "GET", "key").Return(errors.New("get failed"))

				return conn
			},
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) ([]ReplyPair, error) {
				args := [][]interface{}{
					{"SET", "key", "value"},
					{"GET", "key"},
				}

				reply, err := client.Pipeline(context.TODO(), args)

				return reply, err
			},
			expectError: ErrPipeline,
		},
		{
			desc: "Pipeline receive error",
			getMockConn: func() *mockConnection {
				conn := getMockConnection()
				conn.On("Name").Return("test")
				conn.On("Send", "SET", "key", "value").Return(nil)
				conn.On("Send", "GET", "key").Return(nil)
				conn.On("Flush").Return(nil)
				conn.On("Receive").Return(nil, errors.New("recv error"))

				return conn
			},
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) ([]ReplyPair, error) {
				args := [][]interface{}{
					{"SET", "key", "value"},
					{"GET", "key"},
				}

				reply, err := client.Pipeline(context.TODO(), args)

				return reply, err
			},
			expectResult: []ReplyPair{
				{Value: nil, Err: errors.New("recv error")},
				{Value: nil, Err: errors.New("recv error")},
			},
		},
		{
			desc: "Pipeline read only",
			getMockConn: func() *mockConnection {
				conn := getMockConnection()
				conn.On("Name").Return("test")
				conn.On("Send", "GET", "key1").Return(nil)
				conn.On("Send", "GET", "key2").Return(nil)
				conn.On("Flush").Return(nil)
				conn.On("Receive").Return("value1", nil).Once()
				conn.On("Receive").Return("value2", nil).Once()
				return conn
			},
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerRead(conn, nil)
			},
			do: func(client *Client) ([]ReplyPair, error) {
				args := [][]interface{}{
					{"GET", "key1"},
					{"GET", "key2"},
				}

				reply, err := client.PipelineReadOnly(context.TODO(), args)

				return reply, err
			},
			expectResult: []ReplyPair{
				{Value: "value1", Err: nil},
				{Value: "value2", Err: nil},
			},
		},
	}

	for _, scenario := range scenarios {
		s := scenario
		t.Run(s.desc, func(t *testing.T) {
			// setup expectations
			mockCon := s.getMockConn()
			mockConMan := s.mockConMan(mockCon)

			// build a client and make the call
			client := getTestClient(mockConMan)
			result, err := s.do(client)

			// validate the results
			assert.True(t, mockCon.AssertExpectations(t), s.desc)
			assert.True(t, mockConMan.AssertExpectations(t), s.desc)

			assert.Equal(t, s.expectResult, result, s.desc)
			assert.True(t, errors.Is(err, s.expectError), s.desc)
		})
	}
}
