package redis

import (
	"testing"
	"time"

	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/stretchr/testify/assert"
)

func TestHystrixStatsWrapper(t *testing.T) {
	recorder := &stubStatsD{}

	wrapper := newHystrixStatsWrapper("fu", recorder)

	wrapper.Update(metricCollector.MetricResult{})

	assert.Equal(t, 11, len(recorder.countCalls))
	assert.Equal(t, 3, len(recorder.gaugeCalls))
}

type stubStatsD struct {
	durationCalls  []string
	gaugeCalls     []string
	histogramCalls []string
	count1Calls    []string
	countCalls     []string
}

func (s *stubStatsD) Duration(key string, start time.Time, tags ...string) {
	s.durationCalls = append(s.durationCalls, key)
}

func (s *stubStatsD) Gauge(key string, value interface{}, tags ...string) {
	s.gaugeCalls = append(s.gaugeCalls, key)
}

func (s *stubStatsD) Histogram(key string, value interface{}, tags ...string) {
	s.histogramCalls = append(s.histogramCalls, key)
}

func (s *stubStatsD) Count1(key string, tags ...string) {
	s.count1Calls = append(s.count1Calls, key)
}

func (s *stubStatsD) Count(key string, amount int, tags ...string) {
	s.countCalls = append(s.countCalls, key)
}
