package redis

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"gitlab.com/incubus8/go/redis/zapredis"
)

func TestWithStatsD(t *testing.T) {
	cli := &Client{}

	assert.Nil(t, cli.stats)

	// build the option
	statsClient := newNoopStats()
	lambda := WithStatsD(statsClient)

	// apply it
	lambda(cli)

	assert.Equal(t, statsClient, cli.stats)
}

func TestWithHystrixStatsD(t *testing.T) {
	cli := &Client{}

	assert.Nil(t, cli.stats)

	// build the option
	statsClient := newNoopStats()
	lambda := WithHystrixStatsD(statsClient)

	// apply it
	lambda(cli)

	// nothing we can measure
}

func TestWithLogger(t *testing.T) {
	cli := &Client{}

	assert.Nil(t, cli.logger)

	// build the option
	loggingClient := zapredis.New(zap.NewNop())
	lambda := WithLogger(loggingClient)

	// apply it
	lambda(cli)

	assert.Equal(t, loggingClient, cli.logger)
}

func TestName(t *testing.T) {
	cli := &Client{}

	assert.Nil(t, cli.logger)

	// build the option
	lambda := Name("test")

	// apply it
	lambda(cli)

	assert.Equal(t, "test", cli.name)
}
