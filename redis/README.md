# Redis Common Library

This library built based on [Redigo](github.com/gomodule/redigo). It provides the following:

- Protect Redis operation connection with Hystrix Go
- Perform Pipelining
- Perform Timeout operation with context

## Example

You can find the example in `client_example_test.go`.
