package redis

// ReplyPair is the general struct for response of a single redis command
type ReplyPair struct {
	Value interface{}
	Err   error
}
