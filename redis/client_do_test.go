package redis

import (
	"context"
	"errors"
	"testing"

	redigo "github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/incubus8/go/redis/internal/connman"
)

func getMockConnection() (conn *mockConnection) {
	id := uuid.New()
	circuitName := "test" + id.String()
	mockCon := &mockConnection{}
	mockCon.On("CircuitName").Return(circuitName)
	mockCon.On("Close").Return(nil)

	return mockCon
}

func getTestClient(manager connectionManager) *Client {
	client, _ := NewClient()
	client.conMan = manager

	return client
}

func getMockConnectionManagerWrite(mockCon connman.Connection, conErr error) *mockConnectionManager {
	mockConMan := &mockConnectionManager{}
	mockConMan.On("GetForWrite", mock.Anything).Return(mockCon, conErr)

	return mockConMan
}

func getMockConnectionManagerRead(mockCon connman.Connection, conErr error) *mockConnectionManager {
	mockConMan := &mockConnectionManager{}
	mockConMan.On("GetForRead", mock.Anything).Return(mockCon, conErr)

	return mockConMan
}

func TestClientImpl_Do_happyPath(t *testing.T) {
	scenarios := []struct {
		desc       string
		mockConMan func(conn *mockConnection) *mockConnectionManager
		do         func(client *Client) (interface{}, error)
	}{
		{
			desc: "Do",
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) (interface{}, error) {
				return client.Do(context.Background(), "GET", "FU")
			},
		},
		{
			desc: "DoReadOnly",
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerRead(conn, nil)
			},
			do: func(client *Client) (interface{}, error) {
				return client.DoReadOnly(context.Background(), "GET", "FU")
			},
		},
	}

	for _, scenario := range scenarios {
		// setup expectations
		mockCon := getMockConnection()
		mockCon.On("Do", "GET", "FU").Return("BAR", nil).Once()
		mockCon.On("Name").Return("test").Once()

		mockConMan := scenario.mockConMan(mockCon)

		// build a client and make the call
		client := getTestClient(mockConMan)
		result, err := scenario.do(client)

		// validate the results
		assert.Nil(t, err, scenario.desc)
		assert.True(t, mockCon.AssertExpectations(t), scenario.desc)
		assert.True(t, mockConMan.AssertExpectations(t), scenario.desc)

		assert.Equal(t, "BAR", result, scenario.desc)
	}
}

func TestClientImpl_Do_noData(t *testing.T) {
	scenarios := []struct {
		desc       string
		mockConMan func(conn *mockConnection) *mockConnectionManager
		do         func(client *Client) (interface{}, error)
	}{
		{
			desc: "Do",
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) (interface{}, error) {
				return client.Do(context.Background(), "GET", "FU")
			},
		},
		{
			desc: "DoReadOnly",
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerRead(conn, nil)
			},
			do: func(client *Client) (interface{}, error) {
				return client.DoReadOnly(context.Background(), "GET", "FU")
			},
		},
	}

	for _, scenario := range scenarios {
		// setup expectations
		mockCon := getMockConnection()
		// NOTE: redigo returns redigo error
		mockCon.On("Do", "GET", "FU").Return(nil, redigo.ErrNil).Once()
		mockCon.On("Name").Return("test").Once()
		mockConMan := scenario.mockConMan(mockCon)

		// build a client and make the call
		client := getTestClient(mockConMan)
		result, err := scenario.do(client)
		// validate the results
		assert.Nil(t, result)
		assert.Equal(t, redigo.ErrNil, err)

		assert.True(t, mockCon.AssertExpectations(t))
		assert.True(t, mockConMan.AssertExpectations(t))
	}
}

func TestClientImpl_Do_randomError(t *testing.T) {
	scenarios := []struct {
		desc       string
		mockConMan func(conn *mockConnection) *mockConnectionManager
		do         func(client *Client) (interface{}, error)
	}{
		{
			desc: "Do",
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) (interface{}, error) {
				return client.Do(context.Background(), "GET", "FU")
			},
		},
		{
			desc: "DoReadOnly",
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerRead(conn, nil)
			},
			do: func(client *Client) (interface{}, error) {
				return client.DoReadOnly(context.Background(), "GET", "FU")
			},
		},
	}

	for _, scenario := range scenarios {
		// setup expectations
		mockCon := getMockConnection()
		mockCon.On("Do", "GET", "FU").Return(nil, errors.New("something bad happened")).Once()
		mockCon.On("Name").Return("test").Once()

		mockConMan := scenario.mockConMan(mockCon)

		// build a client and make the call
		client := getTestClient(mockConMan)
		result, err := scenario.do(client)
		// validate the results
		assert.Nil(t, result)
		assert.Error(t, err)

		assert.True(t, mockCon.AssertExpectations(t))
		assert.True(t, mockConMan.AssertExpectations(t))
	}
}

func TestClientImpl_Do_conManError(t *testing.T) {
	scenarios := []struct {
		desc              string
		mockConManWithErr func() *mockConnectionManager
		do                func(client *Client) (interface{}, error)
	}{
		{
			desc: "Do",
			mockConManWithErr: func() *mockConnectionManager {
				mockCon := getMockConnection()
				mockCon.On("Close").Return(nil).Once()
				return getMockConnectionManagerWrite(mockCon, errors.New("something bad"))
			},
			do: func(client *Client) (interface{}, error) {
				return client.Do(context.Background(), "GET", "FU")
			},
		},
		{
			desc: "DoReadOnly",
			mockConManWithErr: func() *mockConnectionManager {
				mockCon := getMockConnection()
				mockCon.On("Close").Return(nil).Once()
				return getMockConnectionManagerRead(mockCon, errors.New("something bad"))
			},
			do: func(client *Client) (interface{}, error) {
				return client.DoReadOnly(context.Background(), "GET", "FU")
			},
		},
	}

	for _, scenario := range scenarios {
		// setup expectations
		mockConMan := scenario.mockConManWithErr()

		// build a client and make the call
		client := getTestClient(mockConMan)
		result, err := scenario.do(client)

		// validate the results
		assert.Nil(t, result)
		assert.NotNil(t, err)

		assert.True(t, mockConMan.AssertExpectations(t))
	}
}

func TestClientImpl_DoWithScript(t *testing.T) {
	scenarios := []struct {
		desc       string
		mockConMan func(conn *mockConnection) *mockConnectionManager
		do         func(client *Client) (interface{}, error)
	}{
		{
			desc: "DoWithScript",
			mockConMan: func(conn *mockConnection) *mockConnectionManager {
				return getMockConnectionManagerWrite(conn, nil)
			},
			do: func(client *Client) (interface{}, error) {
				lua := `return redis.call("SET", "hello", "world")`
				script := NewScript(1, lua)

				return client.DoWithScript(context.Background(), script, nil, nil)
			},
		},
	}

	for _, scenario := range scenarios {
		// setup expectations
		mockCon := getMockConnection()
		mockCon.On("Do", "EVALSHA", "0742e48b11dcc17623aa08b197f09caf65d97e15", 1, nil, nil).Return("OK", nil)
		mockCon.On("Name").Return("script test")

		mockConMan := scenario.mockConMan(mockCon)

		// build a client and make the call
		client := getTestClient(mockConMan)
		result, err := scenario.do(client)

		// validate the results
		assert.Nil(t, err, scenario.desc)
		assert.True(t, mockCon.AssertExpectations(t), scenario.desc)
		assert.True(t, mockConMan.AssertExpectations(t), scenario.desc)

		assert.Equal(t, "OK", result, scenario.desc)
	}
}
