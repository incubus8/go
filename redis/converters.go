package redis

import (
	redigo "github.com/gomodule/redigo/redis"
)

// IsErrNoData returns true if the error indicates that no data was returned
func IsErrNoData(err error) bool {
	return err == redigo.ErrNil
}

// Int is a helper that converts a command reply to an integer.
// It will also convert the error to ErrNoData if appropriate.
func Int(reply interface{}, err error) (int, error) {
	return redigo.Int(reply, err)
}

// Int64 is a helper that converts a command reply to 64 bit integer.
// It will also convert the error to ErrNoData if appropriate.
func Int64(reply interface{}, err error) (int64, error) {
	return redigo.Int64(reply, err)
}

// Uint64 is a helper that converts a command reply to 64 bit integer.
// It will also convert the error to ErrNoData if appropriate.
func Uint64(reply interface{}, err error) (uint64, error) {
	return redigo.Uint64(reply, err)
}

// Float64 is a helper that converts a command reply to 64 bit float.
// It will also convert the error to ErrNoData if appropriate.
func Float64(reply interface{}, err error) (float64, error) {
	return redigo.Float64(reply, err)
}

// String is a helper that converts a command reply to a string.
// It will also convert the error to ErrNoData if appropriate.
func String(reply interface{}, err error) (string, error) {
	return redigo.String(reply, err)
}

// Bytes is a helper that converts a command reply to a slice of bytes.
// It will also convert the error to ErrNoData if appropriate.
func Bytes(reply interface{}, err error) ([]byte, error) {
	return redigo.Bytes(reply, err)
}

// Bool is a helper that converts a command reply to a boolean.
// It will also convert the error to ErrNoData if appropriate.
func Bool(reply interface{}, err error) (bool, error) {
	return redigo.Bool(reply, err)
}

// Values is a helper that converts an array command reply to a []interface{}.
// It will also convert the error to ErrNoData if appropriate.
func Values(reply interface{}, err error) ([]interface{}, error) {
	return redigo.Values(reply, err)
}

// Float64s is a helper that converts an array command reply to a []float64.
// It will also convert the error to ErrNoData if appropriate.
func Float64s(reply interface{}, err error) ([]float64, error) {
	return redigo.Float64s(reply, err)
}

// Strings is a helper that converts an array command reply to a []string.
// It will also convert the error to ErrNoData if appropriate.
func Strings(reply interface{}, err error) ([]string, error) {
	return redigo.Strings(reply, err)
}

// ByteSlices is a helper that converts an array command reply to a [][]byte.
// It will also convert the error to ErrNoData if appropriate.
func ByteSlices(reply interface{}, err error) ([][]byte, error) {
	return redigo.ByteSlices(reply, err)
}

// Int64s is a helper that converts an array command reply to a []int64.
// It will also convert the error to ErrNoData if appropriate.
func Int64s(reply interface{}, err error) ([]int64, error) {
	return redigo.Int64s(reply, err)
}

// Ints is a helper that converts an array command reply to a []in.
// It will also convert the error to ErrNoData if appropriate.
func Ints(reply interface{}, err error) ([]int, error) {
	return redigo.Ints(reply, err)
}

// StringMap is a helper that converts an array of strings (alternating key, value) into a map[string]string.
// It will also convert the error to ErrNoData if appropriate.
func StringMap(result interface{}, err error) (map[string]string, error) {
	return redigo.StringMap(result, err)
}

// IntMap is a helper that converts an array of strings (alternating key, value) into a map[string]int.
// It will also convert the error to ErrNoData if appropriate.
func IntMap(result interface{}, err error) (map[string]int, error) {
	return redigo.IntMap(result, err)
}

// Int64Map is a helper that converts an array of strings (alternating key, value) into a map[string]int64.
// It will also convert the error to ErrNoData if appropriate.
func Int64Map(result interface{}, err error) (map[string]int64, error) {
	return redigo.Int64Map(result, err)
}

// Positions is a helper that converts an array of positions (lat, long) into a [][2]float64.
// It will also convert the error to ErrNoData if appropriate.
func Positions(result interface{}, err error) ([]*[2]float64, error) {
	return redigo.Positions(result, err)
}
