package redis

import (
	"context"
	"testing"

	"github.com/gomodule/redigo/redis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestScript_DoWithContext(t *testing.T) {
	ctx := context.Background()

	lua := `redis.call("SET", "hello", "world")`
	script := NewScript(1, lua)

	conn := NewMockConn(t)
	conn.On("Do", mock.Anything, "EVALSHA", "3111494cc9f281cab6c75c6d273cb6537486e3fe", 1, "id").Return(nil, redis.Error("NOSCRIPT "))
	conn.On("Do", mock.Anything, "EVAL", lua, 1, "id").Return(nil, nil)

	res, err := script.doWithContext(ctx, conn, "id")
	assert.NoError(t, err)
	assert.Equal(t, nil, res)
}
