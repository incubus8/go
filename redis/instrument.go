package redis

import (
	"time"
)

type StatsD interface {
	// Time tracking metrics
	Duration(key string, start time.Time, tags ...string)

	// Measure a value over time
	Gauge(key string, value interface{}, tags ...string)

	// Track the statistical distribution of a set of values
	Histogram(key string, value interface{}, tags ...string)

	// Track the occurrence of something (this is equivalent to Count(key, 1, tags...)
	Count1(key string, tags ...string)

	// Increase or Decrease the value of something over time
	Count(key string, amount int, tags ...string)
}

// newNoopStats returns a new No op statsD client.
// This is expected to useful in testing and to adding "standard" instrumentation to shared packages.
// As such this client is safe to be used in init() functions in libraries (skipping a tonne of nil checks)
func newNoopStats() StatsD {
	return &clientNoop{}
}

// clientNoop implements the Client interface and provides the no-op implementation
type clientNoop struct { // intentionally left blank
}

// Duration implements Client interface
func (client *clientNoop) Duration(key string, start time.Time, tags ...string) {
	// intentionally left blank
}

// Gauge implements Client interface
func (client *clientNoop) Gauge(key string, value interface{}, tags ...string) {
	// intentionally left blank
}

// Histogram implements Client interface
func (client *clientNoop) Histogram(key string, value interface{}, tags ...string) {
	// intentionally left blank
}

// Count1 implements Client interface
func (client *clientNoop) Count1(key string, tags ...string) {
	// intentionally left blank
}

// Count implements Client interface
func (client *clientNoop) Count(key string, value int, tags ...string) {
	// intentionally left blank
}
