package connman

import (
	"context"
	"net"
	"sync"
	"sync/atomic"

	redigo "github.com/gomodule/redigo/redis"
)

const (
	logTag = "conMan"

	connTypeRead  = "read"
	connTypeWrite = "write"
)

// ConnectionManager is responsible for:
// - The creation and maintenance of connections and connection pools
// - The selection of connections
// - The monitoring of hosts and dealing with host related concerns like fail-over
type ConnectionManager struct {
	name         string
	cmOptions    *ConManOptions
	masterPool   *singlePool
	poolsForRead []*singlePool

	// pools[masterIdx] is master, should be skipped when getting pool for read
	masterIdx int64

	// current index of pool to use for read
	roundRobinIdx int
	rrIdxLock     sync.RWMutex

	logger Logger
	stats  StatsD

	shutDownChan chan struct{}
}

func New(name string, options ...Option) *ConnectionManager {
	conMan := &ConnectionManager{
		name:         name,
		masterIdx:    -1, // Initialize to impossible index to avoid clashing with actual index in hostsForRead
		shutDownChan: make(chan struct{}),
		cmOptions:    newConManOptions(),
	}

	for _, opt := range options {
		opt(conMan.cmOptions)
	}

	conMan.initLogger()
	conMan.initStats()

	conMan.buildMasterPool()
	conMan.buildReadPools()

	return conMan
}

func (c *ConnectionManager) GetForRead(_ context.Context) (Connection, error) {
	return c.getConn(connTypeRead), nil
}

func (c *ConnectionManager) GetForWrite(_ context.Context) (Connection, error) {
	return c.getConn(connTypeWrite), nil
}

func (c *ConnectionManager) ShutDown(ctx context.Context) {
	done := make(chan struct{})

	go func() {
		defer close(done)
		close(c.shutDownChan)

		err := c.masterPool.Close()
		if err != nil {
			c.logger.PoolCloseError("master", err)
		}

		for _, pool := range c.poolsForRead {
			poolCloseErr := pool.Close()
			if poolCloseErr != nil {
				c.logger.PoolCloseError("slave", err)
			}
		}
	}()

	// wait for complete or timeout
	select {
	case <-ctx.Done():
		c.logger.PoolCloseError("shutdown", ctx.Err())
		return

	case <-done:
		return
	}
}

func (c *ConnectionManager) OnReadOnlyDetected(ctx context.Context) {
	// TODO: find out real master
}

func (c *ConnectionManager) getConn(connectionType string) Connection {
	var pool *singlePool
	var circuit string

	if connectionType == connTypeRead {
		pool = c.getPoolForRead()
		circuit = pool.readCircuitName
	} else {
		pool = c.masterPool
		circuit = pool.writeCircuitName
	}

	hostAndPort := pool.hostAndPort
	conn := pool.connPool.Get()

	return c.newWrappedConnection(hostAndPort, circuit, conn)
}

func (c *ConnectionManager) selectPool() (int, *singlePool) {
	c.rrIdxLock.Lock()
	defer c.rrIdxLock.Unlock()

	index := c.roundRobinIdx
	selectedPool := c.poolsForRead[index]
	c.roundRobinIdx = (c.roundRobinIdx + 1) % len(c.poolsForRead)

	return index, selectedPool
}

func (c *ConnectionManager) getPoolForRead() *singlePool {
	index, selectedPool := c.selectPool()
	if c.cmOptions.readOnlyFromSlaves && c.isMaster(index) {
		// select the next one
		// if there's only one, we will return the master node although readOnlyFromSlaves is set
		_, selectedPool = c.selectPool()
	}

	return selectedPool
}

func (c *ConnectionManager) isMaster(idx int) bool {
	return atomic.LoadInt64(&c.masterIdx) == int64(idx)
}

func (c *ConnectionManager) newWrappedConnection(hostAndPort, circuit string, redisConn redigo.Conn) Connection {
	return &wrappedConnection{
		hostAndPort: hostAndPort,
		circuitName: circuit,
		redisConn:   redisConn,
		logger:      c.logger,
		stats:       c.stats,
	}
}

func (c *ConnectionManager) initLogger() {
	c.logger = c.cmOptions.logger
	if c.logger == nil {
		c.logger = &noopLogger{}
	}
}

func (c *ConnectionManager) initStats() {
	c.stats = c.cmOptions.stats
	if c.stats == nil {
		c.stats = &statsNoop{}
	}
}

func (c *ConnectionManager) buildMasterPool() {
	pool := newSinglePool(c.name, c.cmOptions.getHostAndPort(), c.cmOptions)
	c.masterPool = pool
}

// use master for read if read host list is empty
func (c *ConnectionManager) buildReadPools() {
	if len(c.cmOptions.hostsForRead) == 0 {
		c.logger.NoReadHosts()
		master := net.JoinHostPort(c.cmOptions.Config.Host, c.cmOptions.Config.Port)
		c.cmOptions.hostsForRead = []string{master}
	}

	for _, hostAndPort := range c.cmOptions.hostsForRead {
		pool := newSinglePool(c.name, hostAndPort, c.cmOptions)
		c.poolsForRead = append(c.poolsForRead, pool)
	}
}

// used by tests only
//
//go:generate mockery --name=connection --inpackage --testonly --case=underscore
type connection interface { //nolint:unused,deadcode
	redigo.Conn

	// CircuitName will return the circuit breaker circuit name for this host
	CircuitName() string

	// Name will return Host and Port String associated with each connection
	Name() string
}
