package connman

// Logger is the logger for this package
type Logger interface {
	PoolCloseError(errorType string, err error)
	CloseError(err error)
	NoReadHosts()
	DialFailed(err error, config interface{})
	PingFailed(err error)
}

type noopLogger struct{}

func (n *noopLogger) PingFailed(err error) {}

func (n *noopLogger) DialFailed(err error, config interface{}) {}

func (n *noopLogger) NoReadHosts() {}

func (n *noopLogger) CloseError(err error) {}

func (n *noopLogger) PoolCloseError(_ string, _ error) {}
