package connman

import (
	"testing"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/stretchr/testify/assert"
)

func TestAllOptions(t *testing.T) {
	scenarios := []struct {
		desc      string
		option    Option
		getActual func(conf *ConManOptions) interface{}
		expected  interface{}
	}{
		{
			desc:   "DB",
			option: DB(1),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.DB
			},
			expected: 1,
		},
		{
			desc:   "MaxActive",
			option: MaxActive(100),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.MaxActiveConnections
			},
			expected: 100,
		},
		{
			desc:   "MaxIdle",
			option: MaxIdle(60),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.MaxIdleConnections
			},
			expected: 60,
		},
		{
			desc:   "ConnectTimeout",
			option: ConnectTimeout(2 * time.Second),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.ConnectTimeout
			},
			expected: 2 * time.Second,
		},
		{
			desc:   "ReadTimeout",
			option: ReadTimeout(2 * time.Second),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.ReadTimeout
			},
			expected: 2 * time.Second,
		},
		{
			desc:   "WriteTimeout",
			option: WriteTimeout(2 * time.Second),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.WriteTimeout
			},
			expected: 2 * time.Second,
		},
		{
			desc:   "IdleTimeout",
			option: IdleTimeout(2 * time.Second),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.IdleTimeout
			},
			expected: 2 * time.Second,
		},
		{
			desc:   "WithLogger",
			option: WithLogger(&noopLogger{}),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.logger
			},
			expected: &noopLogger{},
		},
		{
			desc:   "Master",
			option: Master("some_host:9999"),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config
			},
			expected: &poolConfig{
				Host: "some_host",
				Port: "9999",
			},
		},
		{
			desc:   "ReadSlaves",
			option: ReadSlaves("host-1:9999", "host-2:9999", "host-3:9999"),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.hostsForRead
			},
			expected: []string{"host-1:9999", "host-2:9999", "host-3:9999"},
		},
		{
			desc:   "ReadOnlyFromSlaves",
			option: ReadOnlyFromSlaves(),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.readOnlyFromSlaves
			},
			expected: true,
		},
		{
			desc:   "CircuitBreaker",
			option: CircuitBreaker(&hystrix.CommandConfig{MaxConcurrentRequests: 666}),
			getActual: func(conf *ConManOptions) interface{} {
				return conf.Config.cbConfig
			},
			expected: &hystrix.CommandConfig{MaxConcurrentRequests: 666},
		},
	}

	for _, scenario := range scenarios {
		conManOpts := newConManOptions()
		scenario.option(conManOpts)
		assert.Equal(t, scenario.expected, scenario.getActual(conManOpts), scenario.desc)
	}
}

func TestDefaultOptions(t *testing.T) {
	conManOpts := newConManOptions()

	assert.Equal(t, defaultMaxIdle, conManOpts.getMaxIdle())
	assert.Equal(t, defaultMaxActive, conManOpts.getMaxActive())
	assert.Equal(t, defaultIdleTimeout, conManOpts.getIdleTimeout())
	assert.Equal(t, defaultConnectTimeout, conManOpts.getConnectTimeout())
	assert.Equal(t, defaultReadTimeout, conManOpts.getReadTimeout())
	assert.Equal(t, defaultWriteTimeout, conManOpts.getWriteTimeout())
}
