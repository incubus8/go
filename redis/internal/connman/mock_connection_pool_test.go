// Code generated by mockery v2.18.0. DO NOT EDIT.

package connman

import (
	redis "github.com/gomodule/redigo/redis"
	mock "github.com/stretchr/testify/mock"
)

// mockConnectionPool is an autogenerated mock type for the connectionPool type
type mockConnectionPool struct {
	mock.Mock
}

// ActiveCount provides a mock function with given fields:
func (_m *mockConnectionPool) ActiveCount() int {
	ret := _m.Called()

	var r0 int
	if rf, ok := ret.Get(0).(func() int); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int)
	}

	return r0
}

// Close provides a mock function with given fields:
func (_m *mockConnectionPool) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Get provides a mock function with given fields:
func (_m *mockConnectionPool) Get() redis.Conn {
	ret := _m.Called()

	var r0 redis.Conn
	if rf, ok := ret.Get(0).(func() redis.Conn); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(redis.Conn)
		}
	}

	return r0
}

// IdleCount provides a mock function with given fields:
func (_m *mockConnectionPool) IdleCount() int {
	ret := _m.Called()

	var r0 int
	if rf, ok := ret.Get(0).(func() int); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int)
	}

	return r0
}

type mockConstructorTestingTnewMockConnectionPool interface {
	mock.TestingT
	Cleanup(func())
}

// newMockConnectionPool creates a new instance of mockConnectionPool. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func newMockConnectionPool(t mockConstructorTestingTnewMockConnectionPool) *mockConnectionPool {
	mock := &mockConnectionPool{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
