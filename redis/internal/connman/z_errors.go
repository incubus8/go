package connman

// ReadOnlyErr represents error that redis server responds `-READONLY` error
// when client attempted to do write operation on a slave node
type ReadOnlyErr string

func (e ReadOnlyErr) Error() string {
	return string(e)
}
