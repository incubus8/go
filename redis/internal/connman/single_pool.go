package connman

import (
	"strings"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	redigo "github.com/gomodule/redigo/redis"
)

const (
	defaultPort = "6379"
)

// represents a single redigo.Pool entry and metadata
type singlePool struct {
	connPool         connectionPool
	hostAndPort      string
	readCircuitName  string
	writeCircuitName string
	closeChan        chan struct{}
}

// PoolConfig is used to configure a pool of redis connections
type poolConfig struct {
	// Host settings
	Host string
	Port string

	// Redis DB settings
	DB int

	// Connection Pool Settings
	MaxActiveConnections int
	MaxIdleConnections   int
	IdleTimeout          time.Duration

	// Connection settings
	ConnectTimeout time.Duration
	ReadTimeout    time.Duration
	WriteTimeout   time.Duration

	cbConfig *hystrix.CommandConfig
}

// connectionPool contains interface for redigo.Pool
// This is to facilitate testing
//
//go:generate mockery --name=connectionPool --inpackage --testonly --case=underscore
type connectionPool interface {
	// Get gets a connection. The application must close the returned connection.
	// This method always returns a valid connection so that applications can defer
	// error handling to the first use of the connection. If there is an error
	// getting an underlying connection, then the connection Err, Do, Send, Flush
	// and Receive methods return that error.
	Get() redigo.Conn

	// ActiveCount returns the number of connections in the pool. The count
	// includes idle connections and connections in use.
	ActiveCount() int

	// IdleCount returns the number of idle connections in the pool.
	IdleCount() int

	// Close releases the resources used by the pool.
	Close() error
}

// create a pool to a single host and port
func newSinglePool(clientName, hostAndPort string, conManOptions *ConManOptions) *singlePool {
	sp := &singlePool{
		connPool:         buildRedigoPool(conManOptions),
		hostAndPort:      hostAndPort,
		readCircuitName:  makeCircuitName(clientName, hostAndPort, connTypeRead),
		writeCircuitName: makeCircuitName(clientName, hostAndPort, connTypeWrite),
		closeChan:        make(chan struct{}),
	}

	// configure CB
	if conManOptions.Config.cbConfig != nil {
		cbSettings := map[string]hystrix.CommandConfig{
			sp.readCircuitName:  *conManOptions.Config.cbConfig,
			sp.writeCircuitName: *conManOptions.Config.cbConfig,
		}
		hystrix.Configure(cbSettings)
	}

	go sp.monitorPool(conManOptions.stats)

	return sp
}

func makeCircuitName(clientName, hostAnyPort, connectionType string) string {
	return strings.Join([]string{"oredis", clientName, hostAnyPort, connectionType}, "-")
}

func (sp *singlePool) Get() redigo.Conn {
	return sp.connPool.Get()
}

func (sp *singlePool) Close() error {
	close(sp.closeChan)
	return sp.connPool.Close()
}

func (sp *singlePool) monitorPool(stats StatsD) {
	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			stats.Gauge("singlePool.active", float64(sp.connPool.ActiveCount()), "host:"+sp.hostAndPort)
			stats.Gauge("singlePool.idle", float64(sp.connPool.IdleCount()), "host:"+sp.hostAndPort)

		case <-sp.closeChan:
			return
		}
	}
}

func buildRedigoPool(connOpts *ConManOptions) *redigo.Pool {
	maxIdle := connOpts.getMaxIdle()
	maxActive := connOpts.getMaxActive()

	idleTimeout := connOpts.getIdleTimeout()
	connectTimeout := connOpts.getConnectTimeout()
	readTimeout := connOpts.getReadTimeout()
	writeTimeout := connOpts.getWriteTimeout()
	hostAndPort := connOpts.getHostAndPort()
	logger := connOpts.logger

	return &redigo.Pool{
		MaxIdle:     maxIdle,
		MaxActive:   maxActive,
		Wait:        false,
		IdleTimeout: idleTimeout,
		Dial: func() (redigo.Conn, error) {
			conn, err := redigo.Dial("tcp", hostAndPort, redigo.DialConnectTimeout(connectTimeout),
				redigo.DialReadTimeout(readTimeout), redigo.DialWriteTimeout(writeTimeout),
				redigo.DialDatabase(connOpts.Config.DB), redigo.DialPassword(connOpts.password))
			if err != nil {
				logger.DialFailed(err, connOpts.Config)
			}

			return conn, err
		},
		TestOnBorrow: func(c redigo.Conn, timeReturned time.Time) error {
			if time.Since(timeReturned) < 1*time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			if err != nil {
				logger.PingFailed(err)
			}
			return err
		},
	}
}
