package connman

import (
	"time"
)

//go:generate mockery --name=StatsD --inpackage --testonly --case=underscore
type StatsD interface {
	// Time tracking metrics
	Duration(key string, start time.Time, tags ...string)

	// Measure a value over time
	Gauge(key string, value interface{}, tags ...string)

	// Track the statistical distribution of a set of values
	Histogram(key string, value interface{}, tags ...string)

	// Track the occurrence of something (this is equivalent to Count(key, 1, tags...)
	Count1(key string, tags ...string)

	// Increase or Decrease the value of something over time
	Count(key string, amount int, tags ...string)
}

type statsNoop struct{}

func (noop *statsNoop) Duration(_ string, _ time.Time, _ ...string) {}

func (noop *statsNoop) Gauge(_ string, _ interface{}, _ ...string) {}

func (noop *statsNoop) Histogram(_ string, _ interface{}, _ ...string) {}

func (noop *statsNoop) Count1(_ string, _ ...string) {}

func (noop *statsNoop) Count(_ string, _ int, _ ...string) {}
