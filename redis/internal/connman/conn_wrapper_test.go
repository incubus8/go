package connman

import (
	"errors"
	"testing"

	redigo "github.com/gomodule/redigo/redis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func newWrappedConnection(conn *MockConnection, mocker func(*mockStatsd)) *wrappedConnection {
	mStats := &mockStatsd{}
	mocker(mStats)

	return &wrappedConnection{
		hostAndPort: ":6666",
		circuitName: "oredis:host:read",

		stats:  mStats,
		logger: &noopLogger{},

		redisConn: conn,
	}
}

func TestWrappedConnectionClose(t *testing.T) {
	closeErr := errors.New("some error")
	tests := []struct {
		desc   string
		mock   func(*mockStatsd)
		retErr error
	}{
		{
			desc: "happy path",
			mock: func(mstats *mockStatsd) {
				mstats.On("Count1", logTag, "connection", "return", mock.Anything).Once()
			},
			retErr: nil,
		},
		{
			desc: "close error",
			mock: func(mstats *mockStatsd) {
				mstats.On("Count1", logTag, "connection", "return", mock.Anything).Once()
				mstats.On("Count1", logTag, metricError, tagErrConnClose, mock.Anything).Once()
			},
			retErr: closeErr,
		},
	}

	for _, test := range tests {
		redisConn := &MockConnection{}
		redisConn.On("Close").Return(test.retErr)

		wc := newWrappedConnection(redisConn, test.mock)

		err := wc.Close()
		assert.Equal(t, test.retErr, err, test.desc)

		mock.AssertExpectationsForObjects(t, wc.stats)
	}
}

func TestWrappedConnectionDo(t *testing.T) {
	tests := []struct {
		desc      string
		mock      func(*mockStatsd)
		retResult string
		retErr    error
	}{
		{
			desc: "happy path",
			mock: func(mstats *mockStatsd) {
			},
			retResult: "PONG",
			retErr:    nil,
		},
		{
			desc: "return error",
			mock: func(mstats *mockStatsd) {
				mstats.On("Count1", logTag, metricError, "func:do", mock.Anything).Once()
			},
			retResult: "",
			retErr:    errors.New("some error"),
		},
	}

	for _, test := range tests {
		redisConn := &MockConnection{}
		redisConn.On("Do", "PING", mock.Anything).Return(test.retResult, test.retErr)

		wc := newWrappedConnection(redisConn, test.mock)
		res, err := wc.Do("PING")
		assert.Equal(t, test.retResult, res, test.desc)
		assert.Equal(t, test.retErr, err, test.desc)

		mock.AssertExpectationsForObjects(t, wc.stats)
	}
}

func TestWrappedConnectionSend(t *testing.T) {
	tests := []struct {
		desc   string
		mock   func(*mockStatsd)
		retErr error
	}{
		{
			desc: "happy path",
			mock: func(mstats *mockStatsd) {
			},
			retErr: nil,
		},
		{
			desc: "return error",
			mock: func(mstats *mockStatsd) {
				mstats.On("Count1", logTag, metricError, "func:send", mock.Anything).Once()
			},
			retErr: errors.New("some error"),
		},
	}

	for _, test := range tests {
		redisConn := &MockConnection{}
		redisConn.On("Send", "PING", mock.Anything).Return(test.retErr)

		wc := newWrappedConnection(redisConn, test.mock)
		err := wc.Send("PING")
		assert.Equal(t, test.retErr, err, test.desc)

		mock.AssertExpectationsForObjects(t, wc.stats)
	}
}

func TestWrappedConnectionFlush(t *testing.T) {
	tests := []struct {
		desc   string
		mock   func(*mockStatsd)
		retErr error
	}{
		{
			desc: "happy path",
			mock: func(mstats *mockStatsd) {
			},
			retErr: nil,
		},
		{
			desc: "return error",
			mock: func(mstats *mockStatsd) {
				mstats.On("Count1", logTag, metricError, "func:flush", mock.Anything).Once()
			},
			retErr: errors.New("some error"),
		},
	}

	for _, test := range tests {
		redisConn := &MockConnection{}
		redisConn.On("Flush").Return(test.retErr)

		wc := newWrappedConnection(redisConn, test.mock)
		err := wc.Flush()
		assert.Equal(t, test.retErr, err, test.desc)

		mock.AssertExpectationsForObjects(t, wc.stats)
	}
}

func TestWrappedConnectionReceive(t *testing.T) {
	tests := []struct {
		desc      string
		mock      func(*mockStatsd)
		retResult string
		retErr    error
		expectErr error
	}{
		{
			desc: "happy path",
			mock: func(mstats *mockStatsd) {
			},
			retResult: "PONG",
			retErr:    nil,
			expectErr: nil,
		},
		{
			desc: "return error",
			mock: func(mstats *mockStatsd) {
				mstats.On("Count1", logTag, metricError, "func:recv", mock.Anything).Once()
			},
			retResult: "",
			retErr:    errors.New("some error"),
			expectErr: errors.New("some error"),
		},
		{
			desc: "return readonly error",
			mock: func(mstats *mockStatsd) {
				mstats.On("Count1", logTag, metricError, "func:recv", mock.Anything).Once()
			},
			retResult: "",
			retErr:    redigo.Error("READONLY error"),
			expectErr: ReadOnlyErr("READONLY error"),
		},
	}

	for _, test := range tests {
		redisConn := &MockConnection{}
		redisConn.On("Receive").Return(test.retResult, test.retErr)

		wc := newWrappedConnection(redisConn, test.mock)
		res, err := wc.Receive()
		assert.Equal(t, test.retResult, res, test.desc)
		assert.Equal(t, test.expectErr, err, test.desc)

		mock.AssertExpectationsForObjects(t, wc.stats)
	}
}

func TestWrappedConnectionCircuitName(t *testing.T) {
	wc := newWrappedConnection(nil, func(mstats *mockStatsd) {})
	assert.Equal(t, "oredis:host:read", wc.CircuitName())
	assert.Equal(t, ":6666", wc.Name())
}
