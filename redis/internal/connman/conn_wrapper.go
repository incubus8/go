package connman

import (
	"strings"

	redigo "github.com/gomodule/redigo/redis"
)

const (
	metricError = "error"

	tagErrConnClose    = "connClose"
	tagRedisHostPrefix = "host:"
)

// WrappedConnection implements internal.Connection
// it's a thin wrapper for redigo.Conn with logger and stats
type wrappedConnection struct {
	hostAndPort string
	circuitName string

	redisConn redigo.Conn

	logger Logger
	stats  StatsD
}

// Close implements internal.Connection
func (cw *wrappedConnection) Close() error {
	cw.stats.Count1(logTag, "connection", "return", tagRedisHostPrefix+cw.hostAndPort)

	err := cw.redisConn.Close()
	if err != nil {
		cw.stats.Count1(logTag, metricError, tagErrConnClose, tagRedisHostPrefix+cw.hostAndPort)
		cw.logger.CloseError(err)
	}

	return err
}

// Do implements internal.Connection
func (cw *wrappedConnection) Do(command string, args ...interface{}) (interface{}, error) {
	reply, err := cw.redisConn.Do(command, args...)
	if err != nil {
		cw.stats.Count1(logTag, metricError, "func:do", tagRedisHostPrefix+cw.hostAndPort)
	}

	return reply, err
}

// Send implements internal.Connection
func (cw *wrappedConnection) Send(command string, args ...interface{}) error {
	err := cw.redisConn.Send(command, args...)
	if err != nil {
		cw.stats.Count1(logTag, metricError, "func:send", tagRedisHostPrefix+cw.hostAndPort)
	}

	return err
}

// Flush implements internal.Connection
func (cw *wrappedConnection) Flush() error {
	err := cw.redisConn.Flush()
	if err != nil {
		cw.stats.Count1(logTag, metricError, "func:flush", tagRedisHostPrefix+cw.hostAndPort)
	}

	return err
}

// Receive implements internal.Connection
func (cw *wrappedConnection) Receive() (interface{}, error) {
	reply, err := cw.redisConn.Receive()
	if err != nil {
		cw.stats.Count1(logTag, metricError, "func:recv", tagRedisHostPrefix+cw.hostAndPort)

		if e, ok := err.(redigo.Error); ok {
			if strings.HasPrefix(e.Error(), "READONLY ") {
				return reply, ReadOnlyErr(e)
			}
		}
	}

	return reply, err
}

func (cw *wrappedConnection) Err() error {
	return cw.redisConn.Err()
}

// CircuitName implements internal.Connection
func (cw *wrappedConnection) CircuitName() string {
	return cw.circuitName
}

// Name implements internal.Connection
func (cw *wrappedConnection) Name() string {
	return cw.hostAndPort
}
