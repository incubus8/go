package connman

import (
	redigo "github.com/gomodule/redigo/redis"
)

// Connection represents a connection to a redis server
//
// Note: This intentionally does not implement the redigo.Conn interface to enforce decoupling
//
//go:generate mockery --name=Connection --case underscore --testonly --inpackage
type Connection interface {
	redigo.Conn

	// CircuitName will return the circuit breaker circuit name for this host
	CircuitName() string

	// Name will return HostandPort String associated with each connection
	Name() string
}
