package connman

import (
	"net"
	"time"

	"github.com/afex/hystrix-go/hystrix"
)

const (
	// default connections (0 == unlimited)
	defaultMaxActive = 0

	// defaults to 50
	defaultMaxIdle = 50

	defaultConnectTimeout = 3 * time.Second
	defaultReadTimeout    = 3 * time.Second
	defaultWriteTimeout   = 3 * time.Second
	defaultIdleTimeout    = 1 * time.Minute
)

// options common to all ConnectionManagers
type ConManOptions struct {
	Config             *poolConfig
	readOnlyFromSlaves bool

	// a group of host and port which contains one master and multiple slaves
	hostsForRead []string

	logger Logger
	stats  StatsD

	password string
}

func newConManOptions() *ConManOptions {
	return &ConManOptions{
		Config: &poolConfig{},
		stats:  &statsNoop{},
		logger: &noopLogger{},
	}
}

// Option is function configuration options for the Connection Manager.
// For some options, default value is applied to a Config when no Option related to that Config is supplied or invalid Option is supplied
type Option func(conManOpt *ConManOptions)

func CircuitBreaker(config *hystrix.CommandConfig) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.cbConfig = config
	}
}

func Master(master string) Option {
	return func(conManOpt *ConManOptions) {
		host, port := splitHostPort(master)
		conManOpt.Config.Host = host
		conManOpt.Config.Port = port
	}
}

func ReadSlaves(hostsForRead ...string) Option {
	return func(conManOpt *ConManOptions) {
		if len(hostsForRead) == 0 {
			return
		}

		conManOpt.hostsForRead = hostsForRead
	}
}

func DB(db int) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.DB = db
	}
}

func MaxActive(maxActive int) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.MaxActiveConnections = maxActive
	}
}

func MaxIdle(maxIdle int) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.MaxIdleConnections = maxIdle
	}
}

func ConnectTimeout(timeout time.Duration) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.ConnectTimeout = timeout
	}
}

func ReadTimeout(timeout time.Duration) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.ReadTimeout = timeout
	}
}

func WriteTimeout(timeout time.Duration) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.WriteTimeout = timeout
	}
}

func IdleTimeout(timeout time.Duration) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.Config.IdleTimeout = timeout
	}
}

func WithLogger(logger Logger) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.logger = logger
	}
}

func WithStatsD(s StatsD) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.stats = s
	}
}

func ReadOnlyFromSlaves() Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.readOnlyFromSlaves = true
	}
}

func WithPassword(password string) Option {
	return func(conManOpt *ConManOptions) {
		conManOpt.password = password
	}
}

func (conManOpt *ConManOptions) getMaxIdle() int {
	maxIdle := conManOpt.Config.MaxIdleConnections
	if maxIdle <= 0 {
		maxIdle = defaultMaxIdle
	}

	return maxIdle
}

func (conManOpt *ConManOptions) getMaxActive() int {
	maxActive := conManOpt.Config.MaxActiveConnections
	if maxActive <= 0 {
		maxActive = defaultMaxActive
	}

	return maxActive
}

func (conManOpt *ConManOptions) getIdleTimeout() time.Duration {
	timeout := conManOpt.Config.IdleTimeout
	if timeout <= 0 {
		timeout = defaultIdleTimeout
	}

	return timeout
}

func (conManOpt *ConManOptions) getConnectTimeout() time.Duration {
	timeout := conManOpt.Config.ConnectTimeout
	if timeout <= 0 {
		timeout = defaultConnectTimeout
	}

	return timeout
}

func (conManOpt *ConManOptions) getReadTimeout() time.Duration {
	timeout := conManOpt.Config.ReadTimeout
	if timeout <= 0 {
		timeout = defaultReadTimeout
	}

	return timeout
}

func (conManOpt *ConManOptions) getWriteTimeout() time.Duration {
	timeout := conManOpt.Config.WriteTimeout
	if timeout <= 0 {
		timeout = defaultWriteTimeout
	}

	return timeout
}

func (conManOpt *ConManOptions) getHostAndPort() string {
	host := conManOpt.Config.Host
	if host == "" {
		host = "localhost"
	}

	port := conManOpt.Config.Port
	if port == "" {
		port = defaultPort
	}

	return net.JoinHostPort(host, port)
}

func splitHostPort(hostAndPort string) (host, port string) {
	host, port, err := net.SplitHostPort(hostAndPort)
	if err != nil {
		return hostAndPort, defaultPort
	}

	return host, port
}
