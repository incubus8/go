package connman

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewConnManager(t *testing.T) {
	tests := []struct {
		desc                          string
		master                        string
		slaves                        []string
		expectMaster                  string
		expectHostsForRead            []string
		expectedReadCircuitName       string
		expectedWriteCircuitName      string
		expectedFirstSlaveCircuitName string
	}{
		{
			desc:                          "one master, multiple slaves",
			master:                        "10.0.0.1:6379",
			slaves:                        []string{"66.0.0.2:6379", "66.0.0.3:6379"},
			expectMaster:                  "10.0.0.1:6379",
			expectHostsForRead:            []string{"66.0.0.2:6379", "66.0.0.3:6379"},
			expectedReadCircuitName:       "oredis-fu-10.0.0.1:6379-read",
			expectedWriteCircuitName:      "oredis-fu-10.0.0.1:6379-write",
			expectedFirstSlaveCircuitName: "oredis-fu-66.0.0.2:6379-read",
		},
		{
			desc:                          "one master, empty read hosts",
			master:                        "10.0.0.1:6379",
			slaves:                        nil,
			expectMaster:                  "10.0.0.1:6379",
			expectHostsForRead:            []string{"10.0.0.1:6379"},
			expectedReadCircuitName:       "oredis-fu-10.0.0.1:6379-read",
			expectedWriteCircuitName:      "oredis-fu-10.0.0.1:6379-write",
			expectedFirstSlaveCircuitName: "oredis-fu-10.0.0.1:6379-read",
		},
	}

	for _, test := range tests {
		ctx := context.Background()
		connMan := New("fu", Master(test.master), ReadSlaves(test.slaves...))

		assert.Equal(t, test.expectMaster, connMan.masterPool.hostAndPort, test.desc)

		var poolsForRead []string
		for _, pool := range connMan.poolsForRead {
			poolsForRead = append(poolsForRead, pool.hostAndPort)
		}

		assert.Equal(t, test.expectHostsForRead, poolsForRead, test.desc)

		assert.Equal(t, test.expectedReadCircuitName, connMan.masterPool.readCircuitName, test.desc)
		assert.Equal(t, test.expectedWriteCircuitName, connMan.masterPool.writeCircuitName, test.desc)

		require.True(t, len(connMan.poolsForRead) > 0, test.desc)
		assert.Equal(t, test.expectedFirstSlaveCircuitName, connMan.poolsForRead[0].readCircuitName, test.desc)

		connMan.ShutDown(ctx)
	}
}

func TestConnectionManager_getPoolForRead(t *testing.T) {
	ctx := context.Background()

	connMan := New("fu", Master("10.0.0.1:6379"), ReadSlaves("10.0.0.2:6379", "10.0.0.3:6379"))
	defer connMan.ShutDown(ctx)

	tests := []struct {
		desc       string
		expectHost string
	}{
		{
			desc:       "start",
			expectHost: "10.0.0.2:6379",
		},
		{
			desc:       "idx = 0",
			expectHost: "10.0.0.3:6379",
		},
		{
			desc:       "idx = 1",
			expectHost: "10.0.0.2:6379",
		},
	}

	for _, test := range tests {
		pool := connMan.getPoolForRead()

		assert.Equal(t, test.expectHost, pool.hostAndPort, test.desc)
	}
}

func TestConnectionManager_GetForWrite(t *testing.T) {
	ctx := context.Background()

	connMan := New("fu", Master("10.0.0.1:6379"), ReadSlaves("10.0.0.2:6379", "10.0.0.3:6379"))
	defer connMan.ShutDown(ctx)

	// mock the master pool
	mockConnPool := &mockConnectionPool{}
	mockConnPool.On("Close").Return(nil).Once()
	connMan.masterPool.connPool = mockConnPool

	tests := []struct {
		desc       string
		expectHost string
	}{
		{
			desc:       "start",
			expectHost: "10.0.0.1:6379",
		},
	}

	for _, test := range tests {
		mockRedigoConn := &MockConnection{}
		mockRedigoConn.On("Close").Return(nil).Once()
		mockConnPool.On("Get").Return(mockRedigoConn).Once()

		conn, err := connMan.GetForWrite(ctx)

		assert.Equal(t, nil, err, test.desc)

		wc, _ := conn.(*wrappedConnection)
		assert.Equal(t, test.expectHost, wc.hostAndPort, test.desc)

		_ = conn.Close()
	}
}
