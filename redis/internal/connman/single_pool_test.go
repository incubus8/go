package connman

import (
	"testing"

	"github.com/afex/hystrix-go/hystrix"
	redigo "github.com/gomodule/redigo/redis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_newSinglePool(t *testing.T) {
	// inputs
	hostAndPort := "10.0.0.1:6379"

	cbConfig := &hystrix.CommandConfig{
		MaxConcurrentRequests: 1,
		ErrorPercentThreshold: 2,
	}

	options := newConManOptions()
	CircuitBreaker(cbConfig)(options)

	readCircuitName := makeCircuitName("fu", hostAndPort, connTypeRead)
	writeCircuitName := makeCircuitName("fu", hostAndPort, connTypeWrite)

	// build pool
	connPool := newSinglePool("fu", hostAndPort, options)
	assert.NotNil(t, connPool)

	defer connPool.Close()

	// validation
	rpool, _ := connPool.connPool.(*redigo.Pool)

	assert.Equal(t, rpool.IdleTimeout, defaultIdleTimeout)
	assert.Equal(t, rpool.MaxActive, defaultMaxActive)
	assert.Equal(t, rpool.MaxIdle, defaultMaxIdle)
	assert.Equal(t, "10.0.0.1:6379", connPool.hostAndPort)

	// validate CB settings
	resultCbConfig := hystrix.GetCircuitSettings()

	readCircuitConfig, exists := resultCbConfig[readCircuitName]
	require.True(t, exists)

	assert.Equal(t, 1, readCircuitConfig.MaxConcurrentRequests)
	assert.Equal(t, 2, readCircuitConfig.ErrorPercentThreshold)

	writeCircuitConfig, exists := resultCbConfig[writeCircuitName]
	require.True(t, exists)

	assert.Equal(t, 1, writeCircuitConfig.MaxConcurrentRequests)
	assert.Equal(t, 2, writeCircuitConfig.ErrorPercentThreshold)
}
