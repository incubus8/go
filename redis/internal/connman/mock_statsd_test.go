// Code generated by mockery v1.0.0. DO NOT EDIT.

package connman

import (
	"time"

	"github.com/stretchr/testify/mock"
)

// mockStatsd is an autogenerated mock type for the statsd type
type mockStatsd struct {
	mock.Mock
}

// Count provides a mock function with given fields: key, amount, tags
func (_m *mockStatsd) Count(key string, amount int, tags ...string) {
	_va := make([]interface{}, len(tags))
	for _i := range tags {
		_va[_i] = tags[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, key, amount)
	_ca = append(_ca, _va...)
	_m.Called(_ca...)
}

// Count1 provides a mock function with given fields: key, tags
func (_m *mockStatsd) Count1(key string, tags ...string) {
	_va := make([]interface{}, len(tags))
	for _i := range tags {
		_va[_i] = tags[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, key)
	_ca = append(_ca, _va...)
	_m.Called(_ca...)
}

// Duration provides a mock function with given fields: key, start, tags
func (_m *mockStatsd) Duration(key string, start time.Time, tags ...string) {
	_va := make([]interface{}, len(tags))
	for _i := range tags {
		_va[_i] = tags[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, key, start)
	_ca = append(_ca, _va...)
	_m.Called(_ca...)
}

// Gauge provides a mock function with given fields: key, value, tags
func (_m *mockStatsd) Gauge(key string, value interface{}, tags ...string) {
	_va := make([]interface{}, len(tags))
	for _i := range tags {
		_va[_i] = tags[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, key, value)
	_ca = append(_ca, _va...)
	_m.Called(_ca...)
}

// Histogram provides a mock function with given fields: key, value, tags
func (_m *mockStatsd) Histogram(key string, value interface{}, tags ...string) {
	_va := make([]interface{}, len(tags))
	for _i := range tags {
		_va[_i] = tags[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, key, value)
	_ca = append(_ca, _va...)
	_m.Called(_ca...)
}
