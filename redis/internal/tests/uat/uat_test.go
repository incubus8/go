package uat

// func Test_Redis(t *testing.T) {
// 	skiptest.IfNotSet(t, skiptest.UAT)
// 	skiptest.IfSet(t, "CI") // disable until CI can handle MySQL tests

// 	zapLogger, _ := zap.NewDevelopment()

// 	client, err := redis.NewClient(
// 		redis.Master(getAddress()),
// 		redis.MaxIdle(50),
// 		redis.IdleTimeout(30*time.Second),
// 		redis.ReadTimeout(3*time.Second),
// 		redis.WriteTimeout(3*time.Second),
// 		redis.CircuitBreaker(&hystrix.CommandConfig{
// 			Timeout:               3000, // in ms
// 			MaxConcurrentRequests: 50,
// 			ErrorPercentThreshold: 80,
// 		}),
// 		redis.WithLogger(zapredis.New(zapLogger)),
// 		redis.Name("example-client"),
// 		redis.WithPassword(""))

// 	require.NoError(t, err)

// 	reply, err := client.Do(context.Background(), "SET", "key1", "val2")
// 	require.NoError(t, err)

// 	val, err := redigo.String(reply, err)
// 	require.NoError(t, err)
// 	t.Logf("SET reply: %s\n", val)

// 	reply, err = client.Do(context.Background(), "GET", "key1")
// 	require.NoError(t, err)

// 	val, err = redigo.String(reply, err)
// 	require.NoError(t, err)

// 	t.Logf("GET reply: %s\n", val)
// }

// func Test_Redis_conn_race(t *testing.T) {
// 	skiptest.IfNotSet(t, skiptest.UAT)
// 	skiptest.IfSet(t, "CI") // disable until CI can handle MySQL tests

// 	zapLogger, _ := zap.NewDevelopment()

// 	client, err := redis.NewClient(
// 		redis.Master(getAddress()),
// 		redis.MaxIdle(10),
// 		redis.IdleTimeout(30*time.Second),
// 		redis.ReadTimeout(3*time.Second),
// 		redis.WriteTimeout(3*time.Second),
// 		redis.CircuitBreaker(&hystrix.CommandConfig{
// 			Timeout:               3000, // in ms
// 			MaxConcurrentRequests: 50,
// 			ErrorPercentThreshold: 80,
// 		}),
// 		redis.WithLogger(zapredis.New(zapLogger)),
// 		redis.Name("example-client"),
// 		redis.WithPassword(""))
// 	assert.Nil(t, err)

// 	for i := 0; i < 10; i++ {
// 		assert.NotPanics(t, func() {
// 			ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
// 			_, _ = client.Do(ctx, "GET", "key1")
// 			cancel()
// 		})
// 	}
// }

// func getAddress() string {
// 	names, _ := net.LookupHost("mysql")
// 	if len(names) > 0 {
// 		// use docker
// 		return "redis:6379"
// 	}

// 	// use localhost
// 	return "0.0.0.0:6379"
// }
