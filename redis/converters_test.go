package redis

import (
	"testing"

	redigo "github.com/gomodule/redigo/redis"
	"github.com/stretchr/testify/assert"
)

// Given these functions are pure wrappers the tests are just enough to satisfy coverage.

func TestIsErrNoData(t *testing.T) {
	assert.True(t, IsErrNoData(redigo.ErrNil))
}

func TestBool(t *testing.T) {
	_, err := Bool(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestByteSlices(t *testing.T) {
	_, err := ByteSlices(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestBytes(t *testing.T) {
	_, err := Bytes(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestFloat64(t *testing.T) {
	_, err := Float64(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestFloat64s(t *testing.T) {
	_, err := Float64s(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestInt(t *testing.T) {
	_, err := Int(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestInt64(t *testing.T) {
	_, err := Int64(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestInt64Map(t *testing.T) {
	_, err := Int64Map(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestInt64s(t *testing.T) {
	_, err := Int64s(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestIntMap(t *testing.T) {
	_, err := IntMap(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestInts(t *testing.T) {
	_, err := Ints(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestPositions(t *testing.T) {
	_, err := Positions(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestString(t *testing.T) {
	_, err := String(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestStringMap(t *testing.T) {
	_, err := StringMap(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestStrings(t *testing.T) {
	_, err := Strings(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestUint64(t *testing.T) {
	_, err := Uint64(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}

func TestValues(t *testing.T) {
	_, err := Values(nil, redigo.ErrNil)
	assert.True(t, IsErrNoData(err))
}
