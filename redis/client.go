package redis

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	redigo "github.com/gomodule/redigo/redis"

	"gitlab.com/incubus8/go/redis/internal/connman"
)

const (
	pkgName = "oredis"

	// statsd metrics
	metricError   = pkgName + ".error"
	metricElapsed = pkgName + ".elapsed"

	// statsd tags
	tagErrRedisActionFailed = "error:redis.action.failed"
	tagClientPrefix         = "client:"
	tagCmdPrefix            = "cmd:"
	tagWherePrefix          = "where:"
	tagFunctionDo           = "func:do"
	tagFunctionPipeline     = "func:pipeline"
	tagReadOnlyTrue         = "readonly:true"
	tagReadOnlyFalse        = "readonly:false"

	cmdPipeline = "PIPELINE"
)

var (
	doReadOnlyTags       = []string{tagFunctionDo, tagReadOnlyTrue}
	doTags               = []string{tagFunctionDo, tagReadOnlyFalse}
	pipelineReadOnlyTags = []string{tagFunctionPipeline, tagReadOnlyTrue}
	pipelineTags         = []string{tagFunctionDo, tagReadOnlyFalse}
)

type redisActionFunc func(conn connman.Connection) (interface{}, error)

type getConnFunc func(ctx context.Context) (connman.Connection, error)

//go:generate mockery --name=connectionManager --case underscore --testonly --inpackage
type connectionManager interface {
	GetForRead(ctx context.Context) (connman.Connection, error)
	GetForWrite(ctx context.Context) (connman.Connection, error)
	ShutDown(ctx context.Context)
	OnReadOnlyDetected(ctx context.Context)
}

type Client struct {
	cmOptions []connman.Option
	conMan    connectionManager

	stats  StatsD
	logger Logger

	name string

	cbMetrics StatsD
}

func NewClient(options ...ClientOption) (*Client, error) {
	client := newClient()

	for _, opt := range options {
		opt(client)
	}

	if client.name == "" {
		client.name = "name_missing"
	}

	if client.cbMetrics != nil {
		metricCollector.Registry.Register(func(_ string) metricCollector.MetricCollector {
			return newHystrixStatsWrapper(client.name, client.cbMetrics)
		})
	}

	client.conMan = connman.New(client.name, client.cmOptions...)

	return client, nil
}

func newClient() *Client {
	return &Client{
		stats:  newNoopStats(),
		logger: &noopLogger{},
	}
}

// Do implements Client interface
func (c *Client) Do(ctx context.Context, cmdName string, args ...interface{}) (interface{}, error) {
	actionFunc := func(conn connman.Connection) (interface{}, error) {
		return conn.Do(cmdName, args...)
	}

	tags := append(doTags, tagClientPrefix+c.name, tagCmdPrefix+strings.ToUpper(cmdName)) //nolint:gocritic

	return c.write(ctx, actionFunc, tags...)
}

// DoReadOnly implements Client interface
func (c *Client) DoReadOnly(ctx context.Context, cmdName string, args ...interface{}) (interface{}, error) {
	actionFunc := func(conn connman.Connection) (interface{}, error) {
		return conn.Do(cmdName, args...)
	}

	tags := append(doReadOnlyTags, tagClientPrefix+c.name, tagCmdPrefix+strings.ToUpper(cmdName)) //nolint:gocritic

	return c.read(ctx, actionFunc, tags...)
}

func (c *Client) pipelineActionFunc(args [][]interface{}) redisActionFunc {
	return func(conn connman.Connection) (interface{}, error) {
		for _, arg := range args {
			command, ok := arg[0].(string)

			if !ok {
				return nil, fmt.Errorf("%w - cmd is not string", ErrPipeline)
			}

			err := conn.Send(command, arg[1:]...)
			if err != nil {
				return nil, fmt.Errorf("%w - %s", ErrPipeline, err)
			}
		}

		// flush
		err := conn.Flush()
		if err != nil {
			return nil, fmt.Errorf("%w - %s", ErrPipeline, err)
		}

		// receive
		results := make([]ReplyPair, len(args))

		for i := range args {
			results[i].Value, results[i].Err = conn.Receive()
		}

		return results, nil
	}
}

// Pipeline implements Client interface
func (c *Client) Pipeline(ctx context.Context, args [][]interface{}) ([]ReplyPair, error) {
	tags := append(pipelineTags, tagClientPrefix+c.name, tagCmdPrefix+cmdPipeline) //nolint:gocritic

	result, err := c.write(ctx, c.pipelineActionFunc(args), tags...)

	resultPair, _ := result.([]ReplyPair)

	return resultPair, err
}

// PipelineReadOnly implements Client interface
func (c *Client) PipelineReadOnly(ctx context.Context, args [][]interface{}) ([]ReplyPair, error) {
	tags := append(pipelineReadOnlyTags, tagClientPrefix+c.name, tagCmdPrefix+cmdPipeline) //nolint:gocritic

	result, err := c.read(ctx, c.pipelineActionFunc(args), tags...)

	resultPair, _ := result.([]ReplyPair)

	return resultPair, err
}

// IsErrNoData implements redisdata.Client and returns true when the error supplied indicates no data was returned
// Deprecated:Use function IsErrNoData() instead
func (c *Client) IsErrNoData(err error) bool {
	return IsErrNoData(err)
}

func (c *Client) ShutDown(_ context.Context) {
	// intentional NOOP
}

func (c *Client) read(ctx context.Context, actionFunc redisActionFunc, tags ...string) (interface{}, error) {
	closeConnOnDone := true
	return c.doRedisAction(ctx, actionFunc, c.conMan.GetForRead, closeConnOnDone, tags...)
}

func (c *Client) write(ctx context.Context, actionFunc redisActionFunc, tags ...string) (interface{}, error) {
	closeConnOnDone := true
	return c.doRedisAction(ctx, actionFunc, c.conMan.GetForWrite, closeConnOnDone, tags...)
}

type doResponse struct {
	reply interface{}
	err   error
}

func (c *Client) doRedisAction(ctx context.Context, actionFunc redisActionFunc, getConn getConnFunc, closeConn bool, tags ...string) (
	interface{}, error,
) {
	closedCh := make(chan struct{}, 1)

	conn, err := getConn(ctx)
	if err != nil {
		return nil, err
	}

	tagsArray := append(tags, "redis:"+conn.Name()) //nolint:gocritic
	defer c.stats.Duration(metricElapsed, time.Now(), tagsArray...)

	circuitName := conn.CircuitName()
	outputChan := make(chan doResponse, 1)

	cbErr := hystrix.DoC(ctx, circuitName, c.buildAction(actionFunc, closeConn, conn, circuitName, outputChan, closedCh), nil)
	if cbErr != nil {
		c.stats.Count1(metricError, tagErrRedisActionFailed, tagWherePrefix+circuitName)
		c.logger.QueryError(circuitName, cbErr)

		return nil, c.handleCBError(ctx, conn, closeConn, closedCh, cbErr)
	}

	select {
	case output := <-outputChan:
		return output.reply, output.err

	default:
		return nil, ErrOutputChanNotReady
	}
}

func (c *Client) buildAction(actionFunc redisActionFunc, closeConn bool, conn connman.Connection, circuitName string,
	outputChan chan doResponse, closedCh chan struct{},
) func(context.Context) error {
	return func(ctx context.Context) error {
		select {
		case <-closedCh:
			return ErrConnClosedByCircuitBreaker

		case closedCh <- struct{}{}:
		}

		reply, err := actionFunc(conn)
		if c.isCritical(err) {
			// Always close connection if it is a critical error
			conn.Close()

			c.logger.CriticalError(circuitName, err)

			return err
		}

		// Close connection if it should be auto closed
		// This has to be closed here otherwise it can cause race in tests
		if closeConn {
			conn.Close()
		}

		outputChan <- doResponse{
			reply: reply,
			err:   err,
		}

		return nil
	}
}

func (c *Client) handleCBError(ctx context.Context, conn io.Closer, closeConn bool, closedCh chan struct{}, cbErr error) error {
	if c.isReadOnlyErr(cbErr) {
		c.conMan.OnReadOnlyDetected(ctx)
		return cbErr
	}

	if closeConn {
		select {
		case <-closedCh:
			// cb routine has started and will close the conn
		case closedCh <- struct{}{}:
			conn.Close()
		}
	}

	return cbErr
}

func (c *Client) isReadOnlyErr(in error) bool {
	if in == nil {
		return false
	}

	if _, ok := in.(connman.ReadOnlyErr); ok {
		return true
	}

	return false
}

// check errors for types that should not cause the circuit breaker to open
func (c *Client) isCritical(in error) bool {
	if in == nil {
		return false
	}

	if in == redigo.ErrNil {
		// user error caused by data not found
		return false
	}

	switch in.(type) {
	case redigo.Error:
		// These are errors defined in redis protocol (where first byte of the reply is "-")
		// Examples of this error type are "unknown command", "no script" and "read only".
		// Therefore as the CB is tracking the connection health, these are not relevant.
		return false

	default:
		return true
	}
}

// this is used in tests only (but mockery annotation needs a PRD comment)
//
//go:generate mockery --name=connection --inpackage --testonly --case=underscore
type connection interface { //nolint:unused,deadcode
	redigo.Conn

	// CircuitName will return the circuit breaker circuit name for this host
	CircuitName() string

	// Name will return HostandPort String associated with each connection
	Name() string
}
