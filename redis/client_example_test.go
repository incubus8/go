package redis_test

import (
	"context"
	"fmt"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	redigo "github.com/gomodule/redigo/redis"
	"go.uber.org/zap"

	"gitlab.com/incubus8/go/redis"
	"gitlab.com/incubus8/go/redis/zapredis"
)

func Example_usage() {
	zapLogger, _ := zap.NewDevelopment()

	client, err := redis.NewClient(
		redis.Master("10.0.0.1:6379"),
		redis.ReadSlaves("10.0.0.2:6379", "10.0.0.3:6379"),
		redis.MaxIdle(50),
		redis.IdleTimeout(30*time.Second),
		redis.ReadTimeout(3*time.Second),
		redis.WriteTimeout(3*time.Second),
		redis.CircuitBreaker(&hystrix.CommandConfig{
			Timeout:               1000, // in ms
			MaxConcurrentRequests: 50,
			ErrorPercentThreshold: 80,
		}),
		redis.WithLogger(zapredis.New(zapLogger)),
		redis.Name("example-client"),
		redis.WithPassword("password"),
	)
	if err != nil {
		return
	}

	{
		val, _ := redigo.String(client.Do(context.Background(), "SET", "key1", "val1"))
		fmt.Printf("%s\n", val)
	}

	{
		val, _ := redigo.String(client.DoReadOnly(context.Background(), "GET", "key1"))
		fmt.Printf("%s\n", val)
	}

	{
		replies, _ := client.Pipeline(context.Background(), [][]interface{}{
			{"SET", "key1", "val1"},
			{"GET", "key1"},
		})
		for _, reply := range replies {
			fmt.Printf("%s\n", reply.Value)
		}
	}

	{
		replies, _ := client.PipelineReadOnly(context.Background(), [][]interface{}{
			{"GET", "key1"},
			{"GET", "key1"},
		})
		for _, reply := range replies {
			fmt.Printf("%s\n", reply.Value)
		}
	}

	{
		lua := `return redis.call("SET", "hello", "world")`
		script := redis.NewScript(1, lua)

		reply, err := client.DoWithScript(context.Background(), script, nil, nil)
		if err != nil {
			panic(err)
		}

		fmt.Printf("%s\n", reply)
	}

	// uncomment to run example
	/*
		// Output:
		//
		// OK
		// val1
		// OK
		// val1
		// val1
		// val1
		// OK
	*/
}
