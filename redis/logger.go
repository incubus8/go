package redis

import (
	"gitlab.com/incubus8/go/redis/internal/connman"
)

// Logger is the logger for this package
type Logger interface {
	connman.Logger

	CriticalError(circuitName string, err error)
	QueryError(circuitName string, err error)
}

type noopLogger struct{}

func (n *noopLogger) PingFailed(err error) {}

func (n *noopLogger) DialFailed(err error, config interface{}) {}

func (n *noopLogger) NoReadHosts() {}

func (n *noopLogger) CloseError(err error) {}

func (n *noopLogger) PoolCloseError(_ string, _ error) {}

func (n *noopLogger) CriticalError(circuitName string, err error) {}

func (n *noopLogger) QueryError(circuitName string, err error) {}
